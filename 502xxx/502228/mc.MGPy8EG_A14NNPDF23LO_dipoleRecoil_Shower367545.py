evgenConfig.description = "MG LHEF+Pythia8 production JO example with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak"]
evgenConfig.contact = ['despoina.sampsonidou@cern.ch']
evgenConfig.generators = ["Pythia8" , "MadGraph", "EvtGen" ]
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=on"]

include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

