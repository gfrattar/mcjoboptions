evgenConfig.nEventsPerJob = 10000

# ZZ inclusive or ZZjj_EW
isZZjj = False
# polarization states: TT, TL, LL
polStat = "TL"
# number of jets
njets = -1

include("MadGraphControl_Pythia8EvtGen_ZZ2jets_pol.py")
