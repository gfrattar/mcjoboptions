#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZ->llll production with AZNLO CTEQ6L1 tune and mllmin4'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '4lepton' ]
evgenConfig.contact     = [ 'oldrich.kepka@cern.ch' ]
evgenConfig.nEventsPerJob = 200
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune        = "H7.1-Default"

#-------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361603

include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = 'z z > l+ l- l\'+ l\'-'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.PDF = [10800,13000, 303600, 25100] #CT10nnlo, CT14nnlo, NNPDF3.1nnlo, MMHT2014nlo68cl
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 1500
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings              
# --------------------------------------------------------------    
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10") # not possible to use CT18
## Please note from: https://herwig.hepforge.org/tutorials/faq/pdf.html#set-pdf-of-the-lhereader
# For POWHEG matching, there is basically no cross talk between the hard subprocess PDFs and the shower PDFs,
# so choosing a LO shower PDF different from whatever (NLO) PDF has been used with Powheg should not be a problem.

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 0
filtSeq.ChargedTracksWeightFilter.NchMax = 6
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 4, 6, 10, 15, 25 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.00025, 0.00025, 0.002, 0.0045, 0.008, 0.011, 0.013 ]

