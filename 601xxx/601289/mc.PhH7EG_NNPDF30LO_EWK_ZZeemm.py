#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEGV2+Herwig7 EWK ZZ(eemm)+2j production with 7.2 tune.'
evgenConfig.keywords = ["SM", "VBS", "ZZ", "2jet"]
evgenConfig.contact = ["bili@cern.ch"]
evgenConfig.process     = "VBS ZZ(eemm)"
evgenConfig.nEventsPerJob = 10000
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune = "H7.2-Default"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
#include("Herwig7_i_Herwig72_LHEF.py")
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen
#include("Herwig7_i_Herwig72_LHEF.py")
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------


