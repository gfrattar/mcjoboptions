#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg with t~ -> Wd~ and t -> Wd filter'
evgenConfig.keywords    =['SM', 'top', 'ttbar', 'CKM']
evgenConfig.contact     = [ 'benedikt.gocke@cern.ch']
evgenConfig.nEventsPerJob = 5000

include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode      = "t t~ > undecayed"
    # PowhegConfig.MadSpin_enabled = False

DoSingleWeight = False
if DoSingleWeight:
    PowhegConfig.mu_F         = 1.0
    PowhegConfig.mu_R         = 1.0
    PowhegConfig.PDF          = 260400
else:
    PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
    PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
    PowhegConfig.PDF          = 260400

PowhegConfig.nEvents     *= 1.1 # compensate filter efficiency

# Set top quark Branching ratios
PowhegConfig.BR_t_to_Wd = 1.0

PowhegConfig.MadSpin_model = "loop_sm-ckm"
PowhegConfig.MadSpin_decays= ["decay t > w+ d, w+ > l+ vl","decay t~ > w- d~, w- > l- vl~"]
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]"
PowhegConfig.MadSpin_nFlavours = 5

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print('Did not see option!')
    else: opts.nprocs = 0
    print (opts)

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]
