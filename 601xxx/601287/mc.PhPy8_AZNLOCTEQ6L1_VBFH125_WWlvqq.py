process="VBF"

include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 -24',
                             '24:onMode = off', # decay of W
                             '24:mMin = 2.0',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16' ]

#--------------------------------------------------------------
# Filter Higgs decays to WW to lv qq
#--------------------------------------------------------------
include("GeneratorFilters/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 24
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,12,13,14,15,16]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6]

#--------------------------------------------------------------
# Lepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeptonFilter")

LeptonFilter = filtSeq.LeptonFilter
LeptonFilter.Ptcut = 20.*GeV
LeptonFilter.Etacut = 5.0

filtSeq.Expression = "(XtoVVDecayFilterExtended) and (LeptonFilter)"

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "VBF H->WW->lvqq"
evgenConfig.description = "POWHEG+PYTHIA8, VBF H(125)->WW->lvqq"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","VBF","WW", "mH125" ]
evgenConfig.contact     = [ 'benedict.tobias.winter@cern.ch' ]
evgenConfig.generators += [ "Pythia8", "Powheg"]
evgenConfig.inputFilesPerJob = 10 #Specify the number of LHEs files needed 
evgenConfig.nEventsPerJob = 10000

