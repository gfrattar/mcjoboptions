#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "ggH H->ZZ->4l"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, H+jet production with MiNLO and A14 tune, HZZ4l (no tau) mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZZ", "mH125" ]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.inputFilesPerJob = 6
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
	genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']

else:
	genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off', #decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13' ]
