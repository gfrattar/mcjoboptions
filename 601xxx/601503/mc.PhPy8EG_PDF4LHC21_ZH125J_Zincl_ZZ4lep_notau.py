#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process = "qq->ZH, H->4l, Z->all"
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet production: Z->all, H->ZZ->4l (no tau)"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.inputFilesPerJob = 55
evgenConfig.nEventsPerJob    = 5000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
  genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
  genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13']
