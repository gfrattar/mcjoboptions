##############################################################
# Job options fragment for gg->X->4mu  
# Equivalent of MC15.300050.Py8BEG_A14NNPDF23LO_ggX18_4mu_3pt2.py
##############################################################
evgenConfig.description = "PYTHIA8 gg->X->4mu with A14 NNPDF23LO"
evgenConfig.keywords = ["heavyFlavour","Upsilon","4muon"]
evgenConfig.contact = [ 'Tiesheng.Dai@cern.ch' ]
evgenConfig.nEventsPerJob = 10000
 
include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8B.Commands += ['Bottomonium:all = off',
                             'Higgs:useBSM = on',
                             'HiggsBSM:gg2H2 = on',
                             'HiggsH2:coup2d = 1.0',
                             'HiggsH2:coup2u = 1.0',
                             'HiggsH2:coup2Z = 0.0',
                             'HiggsH2:coup2W = 0.0',
                             'HiggsA3:coup2H2Z = 0.0',
                             'HiggsH2:coup2A3A3 = 0.0',
                             'HiggsH2:coup2H1H1 = 0.0',
                             '35:mMin = 0',
                             '35:mMax = 25',
                             '35:m0   = 18.0',
                             '35:mWidth = 0.00',
                             '35:addChannel 1 1.00 100 13 -13 13 -13',
                             '35:onMode = off',
                             '35:onIfMatch 13 -13 13 -13' ## 4mu
                           ] 
#
### 3 lepton filter
#
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("MultiMuonFilter")
filtSeq.MultiMuonFilter.NMuons = 3
filtSeq.MultiMuonFilter.Ptcut  = 2000.0
filtSeq.MultiMuonFilter.Etacut = 2.8
