#--------------------------------------------------------------
#JHUgen+Pythia8 pp/gg ->  eta_b -> 2 J/Psi ->4mu production with JP=0-
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1
evgenConfig.description = "HelacOnia+Pythia8 pp/gg-> SPS J/Psi + Upsilon ->4mu/2mu2e production"
evgenConfig.keywords = ["JPsi","Upsilon","4muon"]
evgenConfig.contact = ["hpang@cern.ch"]
evgenConfig.process = "pp/gg-> TQ -> J/Psi + Upsilon ->4mu/2mu2e production"
evgenConfig.generators += ['Lhef']
#runArgs.inputGeneratorFile = runName+'*.tar.gz'

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")
include("Pythia8_i/Pythia8_LHEF.py")

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   muonfilter2 = MultiMuonFilter("muonfilter2")
   muonfilter3 = MultiMuonFilter("muonfilter3")
   filtSeq += muonfilter1
   filtSeq += muonfilter2
   filtSeq += muonfilter3
   twomuonfilter1 = MultiMuonFilter("twomuonfilter1")
   twomuonfilter2 = MultiMuonFilter("twomuonfilter2")
   filtSeq += twomuonfilter1
   filtSeq += twomuonfilter2

filtSeq.muonfilter1.Ptcut = 1500.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 4 #minimum

filtSeq.muonfilter2.Ptcut = 2500.0 #MeV
filtSeq.muonfilter2.Etacut = 2.7
filtSeq.muonfilter2.NMuons = 2 #minimum

filtSeq.muonfilter3.Ptcut = 3500.0 #MeV
filtSeq.muonfilter3.Etacut = 2.7
filtSeq.muonfilter3.NMuons = 1 #minimum

##########
filtSeq.twomuonfilter1.Ptcut = 1500.0 #MeV
filtSeq.twomuonfilter1.Etacut = 2.7
filtSeq.twomuonfilter1.NMuons = 2 #minimum

filtSeq.twomuonfilter2.Ptcut = 2500.0 #MeV
filtSeq.twomuonfilter2.Etacut = 2.7
filtSeq.twomuonfilter2.NMuons = 1 #minimum

if not hasattr(filtSeq, "MultiElectronFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
   twoelectronfilter1 = MultiElectronFilter("twoelectronfilter1")
   filtSeq += twoelectronfilter1

filtSeq.twoelectronfilter1.Ptcut = 3500.0 #MeV
filtSeq.twoelectronfilter1.Etacut = 2.7
filtSeq.twoelectronfilter1.NElectrons = 2 #minimum

filtSeq.Expression = "(muonfilter1 and muonfilter2 and muonfilter3) or (twomuonfilter1 and twomuonfilter2 and twoelectronfilter1)"

