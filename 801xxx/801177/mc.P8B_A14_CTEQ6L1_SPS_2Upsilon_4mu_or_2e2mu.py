evgenConfig.description = "Pythia8B 2Upsilon -> 4 muon or 2e2mu in single parton scattering"
evgenConfig.process = "2Upsilon -> 4mu or 2e2mu"
evgenConfig.keywords = ["Muon","SM","heavyFlavour"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["Xin.Chen@cern.ch,Yue.Xu@cern.ch"]
evgenConfig.nEventsPerJob = 10000

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include("Pythia8B_i/Pythia8B_Photospp.py")

genSeq.Pythia8B.Commands += [
        "Bottomonium:gg2doublebbbar(3S1)[3S1(1)] = on,on,on,on,on,on",
        "Bottomonium:qqbar2doublebbbar(3S1)[3S1(1)] = on,on,on,on,on,on",
        "PartonLevel:MPI = on",
        "PartonLevel:ISR = on",
        "PartonLevel:FSR = on",
        "HadronLevel:all = off",
        "HadronLevel:Hadronize = on",
        "HadronLevel:Decay = on"]

genSeq.Pythia8B.SuppressSmallPT = True
genSeq.Pythia8B.pT0timesMPI = 0.42
genSeq.Pythia8B.numberAlphaS = 3.
genSeq.Pythia8B.useSameAlphaSasMPI = False

genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMinDiverge = 0.5']
genSeq.Pythia8B.SelectBQuarks = False
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = False
genSeq.Pythia8B.VetoDoubleCEvents = False
genSeq.Pythia8B.NHadronizationLoops = 1

genSeq.Pythia8B.Commands += [
    "200553:onMode = off", # Upsilon(3S)
    "200553:onIfMatch = 11 11", # ee
    "200553:onIfMatch = 13 13", # mumu
    "200553:onIfMatch = 15 15", # tautau (dummy)
    "200553:onIfMatch = 553 111 111", # Upsilon(1S) pi0 pi0
    "200553:onIfMatch = 553 211 211", # Upsilon(1S) pi- pi+
    "200553:onIfMatch = 100553 111 111", # Upsilon(2S) pi0 pi0
    "200553:onIfMatch = 100553 211 211", # Upsilon(2S) pi- pi+
    "200553:onIfMatch = 100553 22 22", # Upsilon(2S) gam gam
    "200553:0:bRatio = 0.336", # ee
    "200553:1:bRatio = 0.336", # mumu
    "200553:2:bRatio = 0.165", # tautau (dummy)
    "200553:3:bRatio = 0.022", # Upsilon(1S) pi0 pi0
    "200553:4:bRatio = 0.044", # Upsilon(1S) pi- pi+
    "200553:5:bRatio = 0.019", # Upsilon(2S) pi0 pi0
    "200553:6:bRatio = 0.028", # Upsilon(2S) pi- pi+
    "200553:7:bRatio = 0.050", # Upsilon(2S) gam gam

    "100553:onMode = off", # Upsilon(2S)
    "100553:onIfMatch = 11 11", # ee
    "100553:onIfMatch = 13 13", # mumu
    "100553:onIfMatch = 555 22", # chi_2b gam (chi_2b -> Upsilon(1S) gam)
    "100553:onIfMatch = 20553 22", # chi_1b gam (chi_1b -> Upsilon(1S) gam)
    "100553:onIfMatch = 553 111 111", # Upsilon(1S) pi0 pi0
    "100553:onIfMatch = 553 211 211", # Upsilon(1S) pi- pi+
    "100553:0:bRatio = 0.294", # ee
    "100553:1:bRatio = 0.298", # mumu
    "100553:3:bRatio = 0.067", # chi_2b gam (chi_2b -> Upsilon(1S) gam)
    "100553:5:bRatio = 0.066", # chi_1b gam (chi_1b -> Upsilon(1S) gam)
    "100553:6:bRatio = 0.090", # Upsilon(1S) pi0 pi0
    "100553:7:bRatio = 0.185", # Upsilon(1S) pi- pi+

    "553:onMode = off", # Upsilon(1S)
    "553:onIfMatch = 11 11", # ee
    "553:onIfMatch = 13 13", # mumu
    "553:onIfMatch = 15 15", # tautau (dummy)
    "553:2:bRatio = 0.367", # ee
    "553:3:bRatio = 0.382", # mumu
    "553:4:bRatio = 0.251", # tautau (dummy)
]

# ### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   fourmuonfilter1 = MultiMuonFilter("fourmuonfilter1")
   fourmuonfilter2 = MultiMuonFilter("fourmuonfilter2")
   fourmuonfilter3 = MultiMuonFilter("fourmuonfilter3")
   filtSeq += fourmuonfilter1
   filtSeq += fourmuonfilter2
   filtSeq += fourmuonfilter3

filtSeq.fourmuonfilter1.Ptcut = 1500.0 #MeV
filtSeq.fourmuonfilter1.Etacut = 2.7
filtSeq.fourmuonfilter1.NMuons = 4 #minimum

filtSeq.fourmuonfilter2.Ptcut = 2500.0 #MeV
filtSeq.fourmuonfilter2.Etacut = 2.7
filtSeq.fourmuonfilter2.NMuons = 3 #minimum

filtSeq.fourmuonfilter3.Ptcut = 3500.0 #MeV
filtSeq.fourmuonfilter3.Etacut = 2.7
filtSeq.fourmuonfilter3.NMuons = 2 #minimum

if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   twomuonfilter1 = MultiMuonFilter("twomuonfilter1")
   twomuonfilter2 = MultiMuonFilter("twomuonfilter2")
   filtSeq += twomuonfilter1
   filtSeq += twomuonfilter2

filtSeq.twomuonfilter1.Ptcut = 1500.0 #MeV
filtSeq.twomuonfilter1.Etacut = 2.7
filtSeq.twomuonfilter1.NMuons = 2 #minimum

filtSeq.twomuonfilter2.Ptcut = 2500.0 #MeV
filtSeq.twomuonfilter2.Etacut = 2.7
filtSeq.twomuonfilter2.NMuons = 1 #minimum

if not hasattr(filtSeq, "MultiElectronFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
   twoelectronfilter1 = MultiElectronFilter("twoelectronfilter1")
   filtSeq += twoelectronfilter1

filtSeq.twoelectronfilter1.Ptcut = 3500.0 #MeV
filtSeq.twoelectronfilter1.Etacut = 2.7
filtSeq.twoelectronfilter1.NElectrons = 2 #minimum

filtSeq.Expression = "(fourmuonfilter1 and fourmuonfilter2 and fourmuonfilter3) or (twomuonfilter1 and twomuonfilter2 and twoelectronfilter1)"
