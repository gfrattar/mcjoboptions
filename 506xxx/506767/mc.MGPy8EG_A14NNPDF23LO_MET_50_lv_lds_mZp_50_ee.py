model="LightVector" 
fs = "ee" 
mDM1 = 5. 
mDM2 = 80. 
mZp = 50. 
mHD = 125. 
filteff = 2.028398e-01 

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MadGraphControl_MGPy8EG_mono_zp_lep.py")