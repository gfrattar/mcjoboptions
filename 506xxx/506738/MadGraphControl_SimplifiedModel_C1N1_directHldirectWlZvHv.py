include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
 
#Define C1 and N1 masses and mass splitting
from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

splitConfig = JOName.split("_")
print splitConfig
masses['1000024'] = float(splitConfig[4]) # C1 mass
masses['1000022'] = float(splitConfig[5]) # N1 mass

# N1: 1000022 ;  N2: 1000023 ; C1: 1000024 ; C2: 1000037

# Will be C1N1
gentype = str(splitConfig[2])
# Will be All RPV decays of both C1 and N1 
decaytype = str(splitConfig[3]) 

process = '''
import model RPVMSSM_UFO                             
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > x1+ n1 / susystrong @1
add process p p > x1- n1 / susystrong @1
add process p p > x1+ n1 j / susystrong @2
add process p p > x1- n1 j / susystrong @2
add process p p > x1+ n1 j j / susystrong @3
add process p p > x1- n1 j j / susystrong @3
'''

# Set up the decays 
decays['1000024'] = """DECAY   1000024     1.70414503E-02   # chargino1+ decays  
#          BR        NDA      ID1           ID2      ID3
3.3333333334E-01     2    25    -11    # BR(C1 -> H e+)                            
3.3333333333E-01    2    25    -13    # BR(C1 -> H mu+)
3.3333333333E-01    2    25    -15    # BR(C1 -> H tau+)
#
"""
decays['1000022'] = """DECAY   1000022     1.70414503E-02   # neutralino1 decays
     5.5555556E-02    2       15        24    # BR(~chi_10 -> tau-     W+)
     5.5555556E-02    2      -15       -24    # BR(~chi_10 -> tau+     W-)
     5.5555556E-02    2       11        24    # BR(~chi_10 -> e-       W+)
     5.5555556E-02    2      -11       -24    # BR(~chi_10 -> e+       W-)
     5.5555556E-02    2       13        24    # BR(~chi_10 -> mu-      W+)
     5.5555556E-02    2      -13       -24    # BR(~chi_10 -> mu+      W-)
     5.5555556E-02    2       23        12    # BR(~chi_10 -> Z nu_e)
     5.5555556E-02    2       23       -12    # BR(~chi_10 -> Z nu_ebar)
     5.5555556E-02    2       23        14    # BR(~chi_10 -> Z nu_mu)
     5.5555556E-02    2       23       -14    # BR(~chi_10 -> Z nu_mubar)
     5.5555556E-02    2       23        16    # BR(~chi_10 -> Z nu_tau)
     5.5555556E-02    2       23       -16    # BR(~chi_10 -> Z nu_taubar)
     5.5555556E-02    2       25        12    # BR(~chi_10 -> H nu_e)
     5.5555556E-02    2       25       -12    # BR(~chi_10 -> H nu_ebar)
     5.5555556E-02    2       25        14    # BR(~chi_10 -> H nu_mu)
     5.5555556E-02    2       25       -14    # BR(~chi_10 -> H nu_mubar)
     5.5555556E-02    2       25        16    # BR(~chi_10 -> H nu_tau)
     5.5555556E-02    2       25       -16    # BR(~chi_10 -> H nu_taubar)
#
"""
# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
masses['25'] = 125.00

njets = 2
evgenConfig.contact  = [ "elodie.deborah.resseguie@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','SUSY','neutralino','RPV']
evgenConfig.description = '~chi1+/- ~chi10 production,  decay via Hl and Zv,Wl,Hv (all RPV) respectively in simplified model, mN1 = %s GeV'%(masses['1000022'])


from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
filtSeq += DirectPhotonFilter()

filtSeq.DirectPhotonFilter.NPhotons = 2

filtSeq.Expression = "DirectPhotonFilter"

evt_multiplier = 300.

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    evgenLog.info('Performing the Pythia guess CKKW-L merging feature')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
