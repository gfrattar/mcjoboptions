selected_operators = ['ctGRe','ctWRe','cQj11','cQj18','cQj31','cQj38','ctj1','ctj8','ctGIm','ctWIm']

process_definition = 'set group_subprocesses False\ndefine uc = u c\ndefine uc~ = u~ c~\ndefine ds = d s\ndefine ds~ = d~ s~\n' # define W hadronic decay
process_definition+= 'generate p p > t t~ l+ vl / h z a b QCD=2 QED=4 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > uc~ ds NP=0) @0 NPprop=0 SMHLOOP=0\n' # dileptonic ++
process_definition+= 'add process p p > t t~ l- vl~ / h z a b QCD=2 QED=4 NP=1, (t > w+ b NP=0, w+ > uc ds~ NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @1 NPprop=0 SMHLOOP=0\n' # dileptonic --
process_definition+= 'add process p p > t t~ l+ vl / h z a b QCD=2 QED=4 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @2 NPprop=0 SMHLOOP=0\n' # trileptonic +
process_definition+= 'add process p p > t t~ l- vl~ / h z a b QCD=2 QED=4 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @3 NPprop=0 SMHLOOP=0\n' # trileptonic -
process_definition+= 'add process p p > t t~ l+ vl j / h z a b QCD=3 QED=4 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > uc~ ds NP=0) @4 NPprop=0 SMHLOOP=0\n' # dileptonic ++ extra jet
process_definition+= 'add process p p > t t~ l- vl~ j / h z a b QCD=3 QED=4 NP=1, (t > w+ b NP=0, w+ > uc ds~ NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @5 NPprop=0 SMHLOOP=0\n' # dileptonic -- extra jet
process_definition+= 'add process p p > t t~ l+ vl j / h z a b QCD=3 QED=4 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @6 NPprop=0 SMHLOOP=0\n' # trileptonic + extra jet
process_definition+= 'add process p p > t t~ l- vl~ j / h z a b QCD=3 QED=4 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @7 NPprop=0 SMHLOOP=0\n' # trileptonic - extra jet

fixed_scale = 425.0 # ~ 2*m(top)+m(W)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+W, top model, multilepton (2LSS/3L), reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=10000

include("Common_SMEFTsim_topmW_ttlv_reweighted.py")
