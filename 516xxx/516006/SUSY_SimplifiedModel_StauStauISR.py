# Job options for event generation with direct stau production.
# To be used for MC16 in r21.6.
# 
# Originally created for
#   https://its.cern.ch/jira/browse/ATLMCPROD-8490
# Based on previous version in r19:
#   https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/blob/master/common/MadGraph/MadGraphControl_SimplifiedModel_StauStau_Full.py
#
# (85)
# 

### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

### helpers
def StringToFloat(s):
    if "p" in s:
        return float(s.replace("p", "."))
    return float(s)

### parse job options name 
# N.B. 60 chars limit on PhysicsShort name...
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("Physics short name: " + str(phys_short))
additional_options = phys_short.split("_")[4:]
evgenLog.info("DirectStau: Additional options: " + str(additional_options))

if not 'ISR' in phys_short :
  raise RuntimeError("This control file is only for StauStauISR production")

# Extract job settings/masses etc.
mslep      = StringToFloat(phys_short.split('_')[2]) 
mn1        = StringToFloat(phys_short.split('_')[3])
masses['1000011'] = mslep
masses['1000013'] = mslep
masses['1000015'] = mslep
masses['2000011'] = mslep
masses['2000013'] = mslep
masses['2000015'] = mslep
masses['1000022'] = mn1
m_dM=mslep-mn1

# Stau-stau + 2 partons, do stau decay in MadGraph (this is different from MC15 setup), no mixed mode
process = '''
generate    p p > ta1- ta1+ j , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @1 
add process p p > ta2- ta2+ j , (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @2 
add process p p > ta1- ta1+ j j , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @3
add process p p > ta2- ta2+ j j, (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @4
'''
useguess = True

evgenLog.info('DirectStau: generation of stau-pair production; grid point decoded into m(stau) = %f GeV, m(N1) = %f GeV' % (mslep, mn1))

evgenConfig.contact = [ "mann@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'stau']
evgenConfig.description = 'Direct stau-pair production in simplified model, m_stauLR = %s GeV, m_N1 = %s GeV' % (mslep, mn1)

# Filter and event multiplier 
evt_multiplier = 2

if m_dM >= 10 :
    evt_multiplier *= 3
elif m_dM >= 5 :
    evt_multiplier *= 4
elif m_dM >= 3 :
    evt_multiplier *= 5
else:
    raise RuntimeError("Couldn't properly set evt_multiplier (mass splitting below 2 GeV not currently allowed), exiting")

#//////////////////////////////////////////////////////////////////////////////
# MadGraph5 Options
#--------------------------------------------------------------
run_settings['ptj1min']=50 # boosted jet 

if 'MET' in runArgs.jobConfig[0]:
  from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
  filtSeq += MissingEtFilter("MissingEtFilter")
  filtSeq.Expression = 'MissingEtFilter'
if 'MET50' in runArgs.jobConfig[0]:
  evt_multiplier *= 5
  evgenLog.info('MET50 filter is applied')
  #include ( 'MC15JobOptions/MissingEtFilter.py' )
  include ( 'GeneratorFilters/MissingEtFilter.py' )
  filtSeq.MissingEtFilter.METCut = 50*GeV
if 'MET75' in runArgs.jobConfig[0]:
    evt_multiplier *= 8
    evgenLog.info('MET75 filter is applied')
    #include ( 'MC15JobOptions/MissingEtFilter.py' )
    include ( 'GeneratorFilters/MissingEtFilter.py' )
    filtSeq.MissingEtFilter.METCut = 75*GeV


if '2L2' in additional_options:

    # Filter that was used in MC15
    evgenLog.info('DirectStau: 2lepton2 filter is applied')

    include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
    filtSeq.MultiElecMuTauFilter.NLeptons  = 2
    filtSeq.MultiElecMuTauFilter.MinPt = 2000.         # pt-cut on the lepton
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 1    # include hadronic taus

    filtSeq.Expression = "MultiElecMuTauFilter"

    # set higher evt_multiplier when using filter
    evt_multiplier *= 2

if '2L1T' in additional_options:
  evgenLog.info('2L1T filter is applied')
  from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
  filtSeq += MultiElecMuTauFilter("DileptonFilter")
  filtSeq += MultiElecMuTauFilter("TauFilter")
  
  MultiElecMuTauFilter1 = filtSeq.DileptonFilter
  MultiElecMuTauFilter1.NLeptons  = 2
  MultiElecMuTauFilter1.MinPt = 2000.
  MultiElecMuTauFilter1.MaxEta = 2.8
  MultiElecMuTauFilter1.MinVisPtHadTau = 13000. # pt-cut on the visible hadronic tau
  MultiElecMuTauFilter1.IncludeHadTaus = 1      # include hadronic taus
  
  MultiElecMuTauFilter2 = filtSeq.TauFilter
  MultiElecMuTauFilter2.NLeptons  = 1
  MultiElecMuTauFilter2.MinPt = 1e10
  MultiElecMuTauFilter2.MaxEta = 2.8
  MultiElecMuTauFilter2.MinVisPtHadTau = 13000. # pt-cut on the visible hadronic tau
  MultiElecMuTauFilter2.IncludeHadTaus = 1      # include hadronic taus
  filtSeq.Expression = "(DileptonFilter and TauFilter)"
  #evt_multiplier *= 2

if '2TFilt' in additional_options:
  
    # new filter for MC16
    evgenLog.info('DirectStau: 2 Tau filter is applied.')
                  
    if not hasattr(filtSeq, "TauFilter" ):
        from GeneratorFilters.GeneratorFiltersConf import TauFilter
        filtSeq += TauFilter("TauFilter" )
 
    # pt and eta filter thresholds
    filtSeq.TauFilter.EtaMaxe   = 2.6 # 2.47
    filtSeq.TauFilter.EtaMaxmu  = 2.8 # 2.7
    filtSeq.TauFilter.EtaMaxhad = 2.6 # 2.5

    filtSeq.TauFilter.Ptcute    = 4.e3 # 18 GeV - 3*1 GeV
    filtSeq.TauFilter.Ptcutmu   = 3.e3 # 15 GeV - 4*0.4 GeV
    filtSeq.TauFilter.Ptcuthad  = 15.e3 # (13 GeV)

    filtSeq.TauFilter.UseNewOptions = False

    filtSeq.TauFilter.Ntaus = 2
    filtSeq.Expression = "TauFilter"    

    # set higher evt_multiplier when using filter
    evt_multiplier *= 2

if 'FwdTFilt' in additional_options:
  
    # new filter for MC16
    evgenLog.info('DirectStau: Forward Tau filter is applied.')
                  
    if not hasattr(filtSeq, "TauFilter" ):
        from GeneratorFilters.GeneratorFiltersConf import TauFilter
        filtSeq += TauFilter("TauFilter" )
 
    # pt and eta filter thresholds
    filtSeq.TauFilter.EtaMaxe   = 2.6 # 2.47
    filtSeq.TauFilter.EtaMaxmu  = 2.8 # 2.7
    filtSeq.TauFilter.EtaMaxhad = 4.6 # 4.5 include forward taus for HL-LHC study

    filtSeq.TauFilter.Ptcute    = 15.e3 # 18 GeV - 3*1 GeV
    filtSeq.TauFilter.Ptcutmu   = 13.e3 # 15 GeV - 4*0.4 GeV
    filtSeq.TauFilter.Ptcuthad  = 15.e3 # (20 GeV)

    filtSeq.TauFilter.UseNewOptions = False

    filtSeq.TauFilter.Ntaus = 2
    filtSeq.Expression = "TauFilter"    
        
    # set higher evt_multiplier when using filter
    evt_multiplier *= 4

else:
  
    evgenLog.info('DirectStau: No filter is applied')

# need more at low stau masses
if mslep < 130: 
    evt_multiplier *= 4

# Configure our decays
decays['1000015'] = """DECAY   1000015     auto   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     0.00000000E+00    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     0.00000000E+00    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     0.00000000E+00    2     1000016       -37   # BR(~tau_1 -> ~nu_tauL H-)
     0.00000000E+00    2     1000016       -24   # BR(~tau_1 -> ~nu_tauL W-)
"""
decays['2000015'] = """DECAY   2000015     auto   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     0.00000000E+00    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     0.00000000E+00    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
     0.00000000E+00    2     1000016       -37   # BR(~tau_2 -> ~nu_tauL H-)
     0.00000000E+00    2     1000016       -24   # BR(~tau_2 -> ~nu_tauL W-)
     0.00000000E+00    2     1000015        25   # BR(~tau_2 -> ~tau_1 h)
     0.00000000E+00    2     1000015        35   # BR(~tau_2 -> ~tau_1 H)
     0.00000000E+00    2     1000015        36   # BR(~tau_2 -> ~tau_1 A)
     0.00000000E+00    2     1000015        23   # BR(~tau_2 -> ~tau_1 Z)
"""

# Configure stau mixing
param_blocks['selmix'] = {}
if 'mmix' in additional_options:
    # https://arxiv.org/pdf/0801.0045.pdf
    # use maximally mixed staus
    evgenLog.info("DirectStau: Maxmix scenario selected.")
    param_blocks['selmix'][ '3   3' ] = '0.70710678118' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.70710678118' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '-0.70710678118' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '0.70710678118' # # RRl6x6

else:
    # https://arxiv.org/pdf/0801.0045.pdf
    # No mixing stau1=stauL stau2=stauR
    param_blocks['selmix'][ '3   3' ] = '1.0' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.0' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '0.0' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '1.0' # # RRl6x6

if 's11' in additional_options:
  
    # only do stau1 modes
    evgenLog.info("DirectStau: Only do stau1-stau1 production modes.")
    process = '''
    generate    p p > ta1- ta1+ j, (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @1 
    '''
    useguess = False
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{ta1-,1000015}{ta1+,-1000015}"]

if 's22' in additional_options:
  
    # only do stau2 modes
    evgenLog.info("DirectStau: Only do stau2-stau2 production modes.")
    process = '''
    generate    p p > ta2- ta2+ j, (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @1 
    '''
    useguess = False
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{ta2-,2000015}{ta2+,-2000015}"]

if 's12' in additional_options:

    # include mixed production modes
    evgenLog.info("DirectStau: Include mixed production modes.")
    process += '''
    add process p p > ta1- ta2+ j, (ta1- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @3
    add process p p > ta2- ta1+ j, (ta2- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @4
    '''

Dm = filter(lambda x: x.startswith("Dm"), additional_options)
if Dm:
  
    # introduce mass splitting
    deltaM = int(Dm[0][2:])
    masses['2000011'] = mslep + deltaM
    masses['2000013'] = mslep + deltaM
    masses['2000015'] = mslep + deltaM
    evgenLog.info('DirectStau: Mass of stau2 (etc.) set to %d GeV' % (masses['2000015']))

# post include
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# AGENE-1511: new CKKW-L "guess" merging features
if useguess:
    evgenLog.info('Using Merging:Process = guess')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 
else:
    evgenLog.info('Using standard merging syntax: ' + str(genSeq.Pythia8.Commands))
