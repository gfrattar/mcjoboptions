AthGeneration,   21.6.0,  , Duplication in event numbering
AthGeneration,   21.6.1,  , Duplication in event numbering
AthGeneration,   21.6.2,  , Duplication in event numbering
AthGeneration,   21.6.3,  , Duplication in event numbering
AthGeneration,   21.6.4,  , Duplication in event numbering
AthGeneration,   21.6.5,  , Duplication in event numbering
AthGeneration,   21.6.6,  , Duplication in event numbering
AthGeneration,   21.6.7,  , Duplication in event numbering
AthGeneration,   21.6.8,  , Duplication in event numbering
AthGeneration,   21.6.9,  , Duplication in event numbering
AthGeneration,   21.6.10,  , Duplication in event numbering
AthGeneration,   21.6.11,  , Duplication in event numbering
AthGeneration,   21.6.12,  , Duplication in event numbering
AthGeneration,   21.6.13,  , Duplication in event numbering
AthGeneration,   21.6.14,  , Duplication in event numbering
AthGeneration,   21.6.15,  , Duplication in event numbering
AthGeneration,   21.6.16, Herwig7 , problematic fix for matchbox+herwig7 muons that travel large distances
AthGeneration,   21.6.18, Herwig7.*EvtGen, AGENE-1838
AthGeneration,   21.6.19, Herwig7.*EvtGen, AGENE-1838
AthGeneration,   21.6.20, Herwig7.*EvtGen, AGENE-1838
AthGeneration,   21.6.21, Herwig7.*EvtGen, AGENE-1838
AthGeneration,   21.6.22, Herwig7.*EvtGen, AGENE-1838
AthGeneration,   21.6.23, Herwig7.*EvtGen, AGENE-1838
AthGeneration,   21.6.25,     , transform crashes when running with inputGeneratorFile parameter
AthGeneration,   21.6.27, Sherpa, buggy version
AthGeneration,   21.6.28, Sherpa, buggy version
AthGeneration,   21.6.16, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.17, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.18, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.19, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.20, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.21, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.22, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.23, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.24, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.25, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.26, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.27, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.28, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.29, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.30, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.31, Pythia8.*EvtGen, hangup for highly filtered samples
AthGeneration,   21.6.16, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.17, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.18, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.19, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.20, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.21, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.22, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.23, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.24, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.25, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.26, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.27, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.28, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.29, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.30, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.31, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.32, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.33, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.34, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.35, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.36, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.37, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.38, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.39, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.40, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.41, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.42, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.43, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.44, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.45, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.46, MadGraph, lhapdf-config problem when multiple BOOST versions available
AthGeneration,   21.6.51, Pythia8, range error - wrong version included, fixed in the next release
AthGeneration,   21.6.52, Pythia8, range error - wrong version included, fixed in the next release
AthGeneration,   21.6.68, , crash when changing event number (in EvgenProdTools/CountHepMC new vs. const_cast in GenEvent object) 
AthGeneration,   21.6.61, MadGraph, MG2.92 validation failed
AthGeneration,   21.6.62, MadGraph, MG2.92 validation failed
AthGeneration,   21.6.64, MadGraph, MG2.92 validation failed
AthGeneration,   21.6.65, MadGraph, MG2.92 validation failed
AthGeneration,   21.6.67, MadGraph, MG2.92 validation failed
AthGeneration,   21.6.69, MadGraph, problematic integration strategy
AthGeneration,   21.6.70, MadGraph, problematic integration strategy
AthGeneration,   21.6.71, MadGraph, problematic integration strategy
AthGeneration,   21.6.72, MadGraph, problematic integration strategy
AthGeneration,   21.6.73, MadGraph, problematic integration strategy
AthGeneration,   21.6.74, MadGraph, problematic integration strategy
AthGeneration,   21.6.81,  , DEBUG option switched on as default
AthGeneration,   21.6.88, Sherpa, interface not prepared for new version (saving and restoring of RNG)
AthGeneration,   21.6.93, Pythia8, special fix for Wtt production (Pythia8.306.fxfx)
AthGeneration,   21.6.100, Sherpa, Sherpa2.2.13 - contains bug in physics 
AthGeneration,   22.6.25, Sherpa, Sherpa2.2.13 - contains bug in physics 
AthGeneration,   22.6.26, Sherpa, Sherpa2.2.13 - contains bug in physics 
AthGeneration,   22.6.27, Sherpa, Sherpa2.2.13 - contains bug in physics 
AthGeneration,   23.6.0, Sherpa, Sherpa2.2.13 - contains bug in physics 
