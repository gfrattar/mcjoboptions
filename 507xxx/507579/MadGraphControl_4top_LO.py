#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords += ['4top', 'LO']
evgenConfig.contact = ["nedaa-alexandra.asbah@cern.ch"]
evgenConfig.generators += ["Pythia8"]
evgenConfig.description = 'Standard-Model 4tops production at LO with MadGraph5 and Pythia8, dedicated for upgrade studies, using an older scale of HT/2'

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import fileinput

# General settings

gridpack_mode=False

#PDF
pdflabel="nn23lo1"

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------

if process=="tttt":
    mgproc="""generate p p > t t~ t t~"""
    name="4topSM"
    topdecay = "decay t > w+ b, w+ > all all \ndecay t~ > w- b~, w- > all all \n"
else: 
    raise RuntimeError("process not found")

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
"""+mgproc+"""
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '2.0',
           'nevents'      : nevents,
           'pdlabel'      : "'"+pdflabel+"'" }

process_dir = new_process()

modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

print_cards()
generate(process_dir=process_dir,runArgs=runArgs)

#generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir)

outputDS = arrange_output(process_dir=process_dir,
                   runArgs=runArgs)

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------

check_reset_proc_number(opts)
runArgs.inputGeneratorFile=outputDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
