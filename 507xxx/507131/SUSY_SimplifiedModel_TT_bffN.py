# General control python for event generation of stop1 pairs with decays into 4body
# New adaptation for Release 21 processing made by Francesco G. Gravili
#
# Original Release 19 code:
# https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_SimplifiedModel_TT_bffN.py
#
# CERN 2021 for the benefit of ATLAS Collaboration

include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Parsing Job Option simplified keys
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("Physics short name: " + str(phys_short))

decay = phys_short.split('_')[2]
mt1 = phys_short.split('_')[3]
mn1 = phys_short.split('_')[4]
decay_tool = phys_short.split('_')[5]
filter_choice = phys_short.split('_')[6]
evgenLog.info("Now checking parsing: decoded mass point (mt1,mn1) = (" + str(mt1) + ", " + str(mn1) + ")" +
              " - Decay " + str(decay) +
              " - Tool used for decay " + str(decay_tool) +
              " - Custom Filter " + str(filter_choice) )

# Now declaring masses of SUSY particles involved
masses['1000006'] = float(mt1)
masses['1000022'] = float(mn1)

# The general process is unchanged
process = '''
generate    p p > t1 t1~     $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j   $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @3
'''
njets = 2

# Now introducing decay table for the 4body, from https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/param_card.SM.TT.bffN.dat
decays['1000006']="""DECAY   1000006     1.34259598E-01              # Total Width of the decay
     1.078E-01         4     1000022         5       -11        12   # BR(stop1 --- chi10 b e nu)
     1.078E-01         4     1000022         5       -13        14   # BR(stop1 --- chi10 b mu nu)
     1.078E-01         4     1000022         5       -15        16   # BR(stop1 --- chi10 b tau nu)
     3.208525E-01      4     1000022         5         2        -1   # BR(stop1 --- chi10 b u dbar)
     1.74404E-02       4     1000022         5         4        -1   # BR(stop1 --- chi10 b c dbar)
     1.74639E-02       4     1000022         5         2        -3   # BR(stop1 --- chi10 b u sbar)
     3.201099E-01      4     1000022         5         4        -3   # BR(stop1 --- chi10 b c sbar)
     5.3E-06           4     1000022         5         2        -5   # BR(stop1 --- chi10 b u bbar)
     5.993E-04         4     1000022         5         4        -5   # BR(stop1 --- chi10 b c bbar)"""

# Renaming features from "extras" to "run_settings"
#run_settings["use_syst"]='F'     # Bug fix for syst calc issue in AGENE-1542 -> Now removed because the framework is different, making the code crash!
run_settings["event_norm"]='sum'
# Starting from MadGraph 2.7.3 4-flav merging enabled by default:
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure#7_6_Four_flavour_or_five_flavour
# For Rel21 Snowmass efforts, let's use a 5F scheme, to be coherent with Generation PDF used and Th. x-secs, done with a 5F scheme as well
flavourScheme = 5

# Some general information
evgenConfig.contact  = [ 'francesco.giuseppe.gravili@cern.ch', 'luigi.longo@cern.ch' ]
evgenConfig.keywords += ['simplifiedModel', 'stop']
evgenConfig.description = 'stop direct pair production, st->b+ff+LSP in simplified model, m_stop = %s GeV, m_N1 = %s GeV'%(masses['1000006'],masses['1000022'])

if 'MadSpin' in decay_tool:
  evgenLog.info('Running w/ MadSpin option')
  madspin_card = 'madspin_card_TT_bffN.dat'

  mscard = open(madspin_card,'w')

  ### Only considering Leptonic BRs by default! Commenting everything else: the JO name must be <50, so something (LepBRonly part) must be taken out
  decay_chains = [ "t1 > n1 ll+ vl b" , "t1~ > n1 ll- vl~ b~" ]

  ### Original 4-lines code snippet below
  #decay_chains = [ "t1 > n1 fu fd~ b" , "t1~ > n1 fu~ fd b~" ]
  #if '2L15' in runArgs.jobConfig[0].split('_')[-1] or 'LepBRonly' in runArgs.jobConfig[0].split('_'):
  #    evgenLog.info('Setting only leptonic decay chains')
  #    decay_chains = [ "t1 > n1 ll+ vl b" , "t1~ > n1 ll- vl~ b~" ]

  mscard.write("""
  #************************************************************
  #*                        MadSpin                           *
  #*                                                          *
  #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
  #*                                                          *
  #*    Part of the MadGraph5_aMC@NLO Framework:              *
  #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
  #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
  #*                                                          *
  #************************************************************
  #Some options (uncomment to apply)
  set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
  #
  set seed %i
  set spinmode none
  # specify the decay for the final state particles
  define ll- = e- mu- ta-
  define ll+ = e+ mu+ ta+
  decay %s
  decay %s
  # running the actual code
  launch"""%(runArgs.randomSeed,decay_chains[0],decay_chains[1]))

  mscard.close()

# Filters rewritten in Rel21 conventions, but only m100 tested
if filter_choice == "m1002L3":
  evgenLog.info('met100 and 2Leps 3GeV filter is applied')

  if not hasattr(filtSeq, "MultiElecMuTauFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")

  if not hasattr(filtSeq, "MissingEtFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
    filtSeq += MissingEtFilter("MissingEtFilter")

  filtSeq.MultiElecMuTauFilter.MinPt  = 3000.
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
  filtSeq.MultiElecMuTauFilter.NLeptons = 2
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
  filtSeq.MissingEtFilter.METCut = 100000.

  filtSeq.Expression = "(MultiElecMuTauFilter and MissingEtFilter)"
  evt_multiplier = 50
  if masses['1000006']-masses['1000022']<20: evt_multiplier=200
  elif masses['1000006']-masses['1000022']<30: evt_multiplier=100

elif 'm1001L20' in filter_choice:
  evgenLog.info('1Lepton or MET100 filter is applied')

  if not hasattr(filtSeq, "MultiElecMuTauFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")

  if not hasattr(filtSeq, "MissingEtFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
    filtSeq += MissingEtFilter("MissingEtFilter")

  filtSeq.MultiElecMuTauFilter.MinPt  = 20000.
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
  filtSeq.MultiElecMuTauFilter.NLeptons = 1
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
  filtSeq.MissingEtFilter.METCut = 100000.

  filtSeq.Expression = "(MultiElecMuTauFilter or MissingEtFilter)"
  evt_multiplier = 5

elif '2L3' in filter_choice:
  evgenLog.info('2lepton3 filter is applied')

  if not hasattr(filtSeq, "MultiElecMuTauFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")

  filtSeq.MultiElecMuTauFilter.MinPt  = 3000.
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
  filtSeq.MultiElecMuTauFilter.NLeptons = 2
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0

  filtSeq.Expression = "MultiElecMuTauFilter"
  evt_multiplier = 20

elif '2L15' in filter_choice:
  evgenLog.info('2lepton15 filter is applied')

  if not hasattr(filtSeq, "MultiElecMuTauFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")

  filtSeq.MultiElecMuTauFilter.MinPt  = 15000.
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
  filtSeq.MultiElecMuTauFilter.NLeptons = 2
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0

  filtSeq.Expression = "MultiElecMuTauFilter"
  evt_multiplier = 50
  if masses['1000006']-masses['1000022']<40: evt_multiplier=150
  elif masses['1000006']-masses['1000022']<70: evt_multiplier=75

elif filter_choice == "m100":
  evgenLog.info('ETMiss 100 GeV filter is applied')

  if not hasattr(filtSeq, "MissingEtFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
    filtSeq += MissingEtFilter("MissingEtFilter")

  filtSeq.MissingEtFilter.METCut = 100000.

  filtSeq.Expression = "MissingEtFilter"
  evt_multiplier = 13

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
  genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]
