#! /usr/bin/env python

import os,sys,time,subprocess,shutil,glob
from AthenaCommon import Logging
mglog = Logging.logging.getLogger('ConfigureVVjj')

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

gridpack_mode=False

mode=0

#----------------------------------------------------------------------------
# Parse the production and decay modes
#----------------------------------------------------------------------------
## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')
print(' *** get_physics_short:', shortname)

ShowerMode = 'Pythia'
if 'H7' in shortname[0] : ShowerMode = 'Herwig'

VVChannel    = shortname[3]
DecayChannel = shortname[4]
ProdMode     = shortname[5]
BOpt         = ''
if len(shortname) == 7 :
    BOpt = shortname[6]

ProcessChainString=""
ProcessChannelBase="p p > j j VV QCD_QED_Scheme, DECAY \n"

### diboson channel
if VVChannel == 'WWjj':
    ProcessChannelBase = ProcessChannelBase.replace('VV', 'w w')
if VVChannel == 'WZjj':
    ProcessChannelBase = ProcessChannelBase.replace('VV', 'w z')
if VVChannel == 'ZZjj':
    ProcessChannelBase = ProcessChannelBase.replace('VV', 'z z')

### decay channel
if DecayChannel == 'vvqq':
    if VVChannel == 'ZZjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'z > vl vl~, z > j j')
    if VVChannel == 'WZjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'z > vl vl~, w > j j')
if DecayChannel == 'lvqq':
    if VVChannel == 'WWjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('w w', 'w+ w-').replace('DECAY', 'w+ > l+ vl, w- > j j')
        ProcessChainString += "add process "
        ProcessChainString += ProcessChannelBase.replace('w w', 'w- w+').replace('DECAY', 'w- > l- vl~, w+ > j j')
        ProcessChainString += "add process "
        ProcessChainString += ProcessChannelBase.replace('w w', 'w+ w+').replace('DECAY', 'w+ > l+ vl, w+ > j j')
        ProcessChainString += "add process "
        ProcessChainString += ProcessChannelBase.replace('w w', 'w- w-').replace('DECAY', 'w- > l- vl~, w- > j j')
    if VVChannel == 'WZjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('w z', 'w+ z').replace('DECAY', 'w+ > l+ vl, z > j j')
        ProcessChainString += "add process "
        ProcessChainString += ProcessChannelBase.replace('w z', 'w- z').replace('DECAY', 'w- > l- vl~, z > j j')
if DecayChannel == 'llqq':
    if VVChannel == 'ZZjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'z > e+ e-, z > j j')
        ProcessChainString += "add process "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'z > mu+ mu-, z > j j')
    if VVChannel == 'WZjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'z > e+ e-, w > j j')
        ProcessChainString += "add process "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'z > mu+ mu-, w > j j')
if DecayChannel == 'qqqq':
    if VVChannel == 'WWjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'w > j j, w > j j')
    if VVChannel == 'WZjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'w > j j, z > j j')
    if VVChannel == 'ZZjj':
        ProcessChainString += "generate "
        ProcessChainString += ProcessChannelBase.replace('DECAY', 'z > j j, z > j j')

### QCD/QED scheme
if ProdMode == 'EW6' :
    ProcessChainString = ProcessChainString.replace('QCD_QED_Scheme', 'QCD=0 QED=4')
if ProdMode == 'QCD' :
    ProcessChainString = ProcessChainString.replace('QCD_QED_Scheme', 'QCD=2 QED=2')
if ProdMode == 'Inc' :
    ppp = ProcessChainString
    ProcessChainString = ppp.replace('QCD_QED_Scheme', 'QCD=0 QED=4') + ppp.replace('QCD_QED_Scheme', 'QCD=2 QED=2').replace('generate', 'add process')

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
process_str="""
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define w = w+ w-
"""

process_str+=ProcessChainString

process_str+="""
output -f
"""

print(process_str)

fcard = open('proc_card_mg5.dat','w')
fcard.write(process_str)
fcard.close()

if not is_gen_from_gridpack():
  process_dir = new_process(process_str)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

params = {}
modify_param_card(process_dir=process_dir, params=params)

#----------------------------------------------------------------------------
# Run Number
#----------------------------------------------------------------------------
#if not hasattr(runArgs,'runNumber'):
#    raise RunTimeError("No run number found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
### safe factor applied to nevents, to account for the filter efficiency
safefactor = 1.2
if   BOpt == 'BVeto'   : safefactor *= 6.
elif BOpt == 'BFilter' : safefactor *= 2.

# ad-hoc for EWK6 ZZ llqq
if ProdMode == 'EW6' and VVChannel == 'ZZjj' and DecayChannel == 'llqq':
    safefactor *= 3.

nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob
nevents = int(nevents)

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
    'asrwgtflavor':"5",
    'lhe_version':"3.0",
    'ptj':"15",
    'ptb':"15",
    'pta':"0",
    'ptl':"4",
    'misset':"10",
    'etaj':"5",
    'etab':"5",
    'etal':"2.8",
    'drjj':"0",
    'drll':"0",
    'draa':"0",
    'draj':"0",
    'drjl':"0",
    'dral':"0",
    'mmjj':"10",
    'mmbb':"10",
    'mmll':"40",
    'maxjetflavor':"5" ,
    'cut_decays'  :'T',
    'auto_ptj_mjj': 'F',
    'nevents'     : nevents,
}

# update run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
generate(process_dir=process_dir, grid_pack=gridpack_mode, runArgs=runArgs)


#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

### Shower
if ShowerMode == 'Pythia' :
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("Pythia8_i/Pythia8_MadGraph.py")
    
    # Dipole shower
    genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

elif ShowerMode == 'Herwig' :
    # initialize Herwig7 generator configuration for showering of LHE files
    include("Herwig7_i/Herwig72_LHEF.py")

    # configure Herwig7
    Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_mg5amc_commands(lhe_filename="tmp_LHE_events.events", me_pdf_order="NLO")

    # add EvtGen
    include("Herwig7_i/Herwig71_EvtGen.py")

    # run Herwig7
    Herwig7Config.run()

### B filter/veto
if BOpt != '':
    include("GeneratorFilters/BHadronFilter.py")
    filtSeq += HeavyFlavorBHadronFilter
    if BOpt == 'BFilter' :
        filtSeq.Expression = "(HeavyFlavorBHadronFilter)"
    elif BOpt == 'BVeto' :
        filtSeq.Expression = "(not HeavyFlavorBHadronFilter)"

###
if ShowerMode == 'Pythia' :
    evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
elif ShowerMode == 'Herwig' :
    evgenConfig.generators = ["MadGraph", "Herwig7", "EvtGen"]
    evgenConfig.tune = "H7.1-Default"

evgenConfig.description = 'MadGraph VV plus two EWK jets'
evgenConfig.keywords+=['SM','ZZ','2jet','VBS']

evgenConfig.contact = ['Lailin Xu <lailin.xu@cern.ch>', 'Antonio Giannini <antonio.giannini@cern.ch>', 'Karolos Potamianos <karolos.potamianos@cern.ch>']
