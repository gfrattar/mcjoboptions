include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

jobConfigParts = JOName.split("_")

mx1=float(jobConfigParts[4])

masses['1000024'] = mx1 # C1 mass


chargino_decay = {'1000024':'''DECAY   1000024    1.70414503E-02   # chargino1+ decays
#          BR        NDA      ID1       ID2      ID3  
        0.0000000000E+00    2    23    -11    # BR(C1 -> Z e+)
        0.0000000000E+00    2    23    -13    # BR(C1 -> Z mu+)
        0.0000000000E+00    2    23    -15    # BR(C1 -> Z tau+)
        3.3300000000E-01    2    25    -11    # BR(C1 -> H e+)
        3.3300000000E-01    2    25    -13    # BR(C1 -> H mu+)
        3.3400000000E-01    2    25    -15    # BR(C1 -> H tau+)
        0.0000000000E+00    2    24    -12    # BR(C1 -> W+ nu_ebar)
        0.0000000000E+00    2    24    -14    # BR(C1 -> W+ nu_mubar)
        0.0000000000E+00    2    24    -16    # BR(C1 -> W+ nu_taubar)
#
  '''}
decays.update(chargino_decay)

# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
masses['5'] = 0.00
masses['25'] = 125.00
higgs_decay = {'25':'''DECAY   25     4.06911399E-03   # higgs decays
#          BR         NDA          ID1       ID2       ID3       ID4
        0.00000000E+00    2          15       -15   # BR(H1 -> tau- tau+)
        1.00000000E+00    2           5        -5   # BR(H1 -> b bbar)
        0.00000000E+00    2          24       -24   # BR(H1 -> W+ W-)
        0.00000000E+00    2          23        23   # BR(H1 -> Z Z)
        0.00000000E+00    2          23        22   # BR(H1 -> Z gamma)  
        0.00000000E+00    2          22        22   # BR(H1 -> gamma gamma)   
        0.00000000E+00    2          13       -13   # BR(H1 -> mu mu)         
        0.00000000E+00    2           4        -4   # BR(H1 -> c cbar)       
        0.00000000E+00    2           3        -3   # BR(H1 -> s sbar)     
        0.00000000E+00    2          21        21   # BR(H1 -> g g)
#
  '''}
decays.update(higgs_decay)

gentype = str(jobConfigParts[2])
decaytype = str(jobConfigParts[3])
flavourScheme = 5

print("gentype", gentype)
print("decaytype", decaytype)
print("decays", decays)
print("masses", masses)
print("flavourScheme", flavourScheme)

process = '''
   import model RPVMSSM_UFO
   define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~\
   dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
   define p = g u c d s b u~ c~ d~ s~ b~
   define j = g u c d s b u~ c~ d~ s~ b~
   generate p p > x1+ x1- QED=2 RPV=0 / susystrong @1
add process p p > x1+ x1- j QED=2 RPV=0 / susystrong @2
add process p p > x1+ x1- j j QED=2 RPV=0 / susystrong @3
'''

njets = 2
evgenLog.info('Registered generation of ~chi1+/~chi1- production, decay via Hl; grid point '+str(runArgs.jobConfig[0].split('/')[-1]))

evgenConfig.contact  = [ "michael.donald.hank@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','SUSY','chargino','RPV']
evgenConfig.description = '~chi1+/~chi1- production, decay via Hl in simplified model, m_C1 = %s GeV'%(masses['1000024'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{x1-,-1000024}",
                              "1000024:spinType = 1"]
