import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os 

nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob 
nevents*=6.

## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short() 


runName = "run_01" # hardcoded because madSpin expects run_01 in path
process="""import model TopFCNC-onlyh
   generate p p > t h $$ t~ [QCD]
   add process p p > t~ h $$ t [QCD]
   output -f"""

beamEnergy=-999 
if hasattr(runArgs,'ecmEnergy'):
     beamEnergy = runArgs.ecmEnergy / 2. 
else:
     raise RuntimeError("No center of mass energy found.")

settings_run = {   'parton_shower': 'PYTHIA8',
                   'fixed_ren_scale':'True',
                   'fixed_fac_scale':'True',
                   'mur_ref_fixed':'297.5',
                   'muf_ref_fixed':'297.5',
                   'jetalgo': '-1.0',
                   'jetradius': '0.4',
                   'ptj': '10.0',
                   'etaj': '-1.0',
                   'etal': '-1.0',
                   'mll_sf': '30.0',
                   'ptgmin': '20.0',
                   'nevents':int(nevents), }


RCtphi = 0.
ICtphi = 0.
RCuphi = 0.
ICuphi = 0.
RCtcphi = 0.
ICtcphi = 0.
RCctphi = 0.
ICctphi = 0.

if "_tphiQH" in shortname:
    RCtphi = 1.
    RCtcphi = 1e-5
elif "_uphiQH" in shortname:
    RCuphi = 1.
    RCctphi = 1e-5
elif "_tcphiQH" in shortname:
    RCtcphi = 1.
    RCtphi = 1e-5
elif "_ctphiQH" in shortname:
    RCctphi = 1.
    RCuphi = 1e-5
else:
    raise RuntimeError("Vertex not found :< ")

wstrm = ', w- > l- vl~'
wstrp = ', w+ > l+ vl'
if "QHZZ_Zall" in shortname:
    wstrm = ', w- > All All'
    wstrp = ', w+ > All All'

tstrm = 'decay t~ > w- b~'
tstrp = 'decay t > w+ b'


params = {
    'RCtphi'  : RCtphi ,
    'RCuphi'  : RCuphi ,
    'RCtcphi' : RCtcphi,
    'RCctphi' : RCctphi,
    'ICtphi' : ICtphi,
    'ICuphi' : ICuphi,
    'ICtcphi' : ICtcphi,
    'ICctphi' : ICctphi
    }

process_dir = new_process(process) 
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings_run) 
from MadGraphControl.MadGraphParamHelpers import set_top_params
set_top_params(process_dir)
modify_param_card(process_dir=process_dir,params={'dim6':params})


###### hack some cards to get rid of the default higgs decay
myFile = process_dir + '/Cards/param_card.dat'
with open(myFile, "r") as f:
    lines = f.readlines()
with open(myFile, "w") as f:
    writeBlock = False
    for line in lines:
        if "DECAY  " in line and writeBlock:
            writeBlock = False
        if not writeBlock:
            if "DECAY  25" in line:
                writeBlock = True
            else:
                f.write(line)


## For MadSpin
madspin_card_loc = process_dir + '/Cards/madspin_card.dat'
madspin_card_rep = madspin_card_loc
madspin_in       = 'import Events/'+runName+'/events.lhe'
madspin_rep      = 'set ms_dir MadSpin'
madspin_seed     = runArgs.randomSeed
if hasattr(runArgs, 'inputGenConfFile'):
    madspin_card_rep = gridpack_dir+'Cards/'+madspin_card_loc
    madspin_in       = 'import '+gridpack_dir+'Events/'+runName+'/events.lhe'
    madspin_rep      = 'set ms_dir '+gridpack_dir+'MadSpin'
    madspin_seed     = 10000000+int(runArgs.randomSeed)

mscard = open(madspin_card_rep,'w')
mscard.write("""set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event   (default: 400)
set seed %i
%s
%s
define l+ = l+ ta+
define l- = l- ta-
define All = l+ l- vl vl~ j
\n
"""%(madspin_seed,madspin_in,madspin_rep))  
mscard.write("""%s%s\n"""%(tstrm,wstrm))
mscard.write("""%s%s\nlaunch"""%(tstrp,wstrp))
mscard.close()

print_cards()

generate(process_dir=process_dir,runArgs=runArgs) 
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.contact          = ['maren.stratmann@cern.ch'] 
evgenConfig.generators       = ['MadGraph','Pythia8','EvtGen'] 
evgenConfig.description = 'Single-top t-channel production with four-fermion EFT operator' 

evgenConfig.nEventsPerJob = 10000


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")


if "QHWW_Wall" in shortname:
    genSeq.Pythia8.Commands += ["25:onMode = 0",
        "25:onIfMatch = 24 24"]
elif "QHZZ_Zall" in shortname:
    genSeq.Pythia8.Commands += ["25:onMode = off",
        "25:onIfMatch = -23 23"]
elif "QHtautaubWln" in shortname:
    genSeq.Pythia8.Commands += ["25:onMode = off",
        "25:onIfMatch = -15 15"]
else:
    raise RuntimeError("Higgs decay not found >:[ ")

#### For Rivet or Paver or whatever. 
#from AthenaCommon.AlgSequence import AlgSequence
#rivetSeq = AlgSequence()
#from Rivet_i.Rivet_iConf import Rivet_i
#rivet = Rivet_i()
#rivet.Analyses += ["MC_TTBAR:TTMODE=TWOLEP"]
#rivet.HistoFile = 'out.yoda'
#rivet.SkipWeights=True
#rivetSeq += rivet

include('GeneratorFilters/MultiLeptonFilter.py')
filtSeq.MultiLeptonFilter.Ptcut = 5000.
filtSeq.MultiLeptonFilter.Etacut = 4.5
filtSeq.MultiLeptonFilter.NLeptons = 2
