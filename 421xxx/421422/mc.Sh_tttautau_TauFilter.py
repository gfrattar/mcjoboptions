include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa tttautau@LO"
evgenConfig.keywords = ["top", "SM", "multilepton"]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob = 10

genSeq.Sherpa_i.RunCard="""
(run){
  # ME setup
  ME_SIGNAL_GENERATOR Comix;
  INTEGRATION_ERROR=0.2;
  FINISH_OPTIMIZATION=Off;
  CORE_SCALE VAR{H_TM2/4}
  EXCLUSIVE_CLUSTER_MODE 1;

  # top/W decays
  HARD_DECAYS On;
  HARD_SPIN_CORRELATIONS 1;
  SOFT_SPIN_CORRELATIONS 1;
  SPECIAL_TAU_SPIN_CORRELATIONS=1
  STABLE[24] 0;
  STABLE[6] 0;
}(run)

(processes){
  Process 93 93 -> 6 -6 15 -15;
  Order (*,2);
  End process;
}(processes)

(selector){
  Mass 15 -15  5 E_CMS;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[24]=0.0", "LOG_FILE="]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
if not hasattr(prefiltSeq, 'xAODCnv'):
  from xAODTruthCnv.xAODTruthCnvConf import xAODMaker__xAODTruthCnvAlg
  prefiltSeq += xAODMaker__xAODTruthCnvAlg('xAODCnv',WriteTruthMetaData=False)
  prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'

if not hasattr(prefiltSeq, "TauTruthParticleSlimmer"):
  from GeneratorFilters.GeneratorFiltersConf import TauTruthParticleSlimmer
  prefiltSeq += TauTruthParticleSlimmer('TauTruthParticleSlimmer')


from GeneratorFilters.GeneratorFiltersConf import xAODTauFilter
tauLepFilter = xAODTauFilter("tauLepFilter")
filtSeq += tauLepFilter

filtSeq.tauLepFilter.Ntaus = 2 
filtSeq.tauLepFilter.EtaMaxe = 2.7 
filtSeq.tauLepFilter.EtaMaxmu = 2.7 
filtSeq.tauLepFilter.EtaMaxhad = 2.7 # no hadronic tau decays
filtSeq.tauLepFilter.Ptcute = 12000.0
filtSeq.tauLepFilter.Ptcutmu = 12000.0





