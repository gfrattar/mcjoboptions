# based on DSID=450694
# using 421429 but changing to XAOD filter format
evgenConfig.description = "Dijet with the A14 NNPDF23 LO tune, filtered for 4 jets (2 b-jets)"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["bill.balunas@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection = on",
                            "PhaseSpace:pTHatMin = 150."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
 
include("GeneratorFilters/xAODMultiBjetFilter_Common.py" )

# Example configuration
"""
filtSeq.xAODMultiBjetFilter.LeadJetPtMin = 0
filtSeq.xAODMultiBjetFilter.LeadJetPtMax = 50000
filtSeq.xAODMultiBjetFilter.BottomPtMin = 5.0
filtSeq.xAODMultiBjetFilter.BottomEtaMax = 3.0
filtSeq.xAODMultiBjetFilter.JetPtMin = 13000
filtSeq.xAODMultiBjetFilter.JetEtaMax = 2.7
filtSeq.xAODMultiBjetFilter.DeltaRFromTruth = 0.3
filtSeq.xAODMultiBjetFilter.TruthContainerName = "AntiKt4TruthJets"
"""
filtSeq.xAODMultiBjetFilter.NJetsMin = 4
filtSeq.xAODMultiBjetFilter.NBJetsMin = 2


