# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "showering of POWHEG files with H7."
evgenConfig.keywords = ["SM"]
evgenConfig.contact = ["james.robinson@cern.ch","ewelina@mail.desy.de"]
evgenConfig.generators  += ["Powheg", "Herwig7", "EvtGen"]

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10nlo")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()


