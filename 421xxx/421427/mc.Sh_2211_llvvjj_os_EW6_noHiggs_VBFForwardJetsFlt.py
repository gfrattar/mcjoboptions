include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "opposite sign llvvjj-EW-noHiggs with high mjj filter"
evgenConfig.keywords = ["SM", "diboson", "2lepton", "VBS"]
evgenConfig.contact  = ["atlas-generators-sherpa@cern.ch", "chris.g@cern.ch"]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  EW_SCHEME=3;
  ACTIVE[25]=0;
  MASSIVE[5]=1; 
  MASSIVE[11]=1;
  MASSIVE[13]=1;
  MASSIVE[15]=1;
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;

  %scales, tags for scale variations
  FSCF:=1.0; RSCF:=1.0; QSCF:=1.0;
  SCALES=STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};
  CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  # simplified setup as long as only 2->6 taken into account:
  #SCALES=VAR{FSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{RSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{QSCF*Abs2(p[2]+p[3]+p[4]+p[5])};

  %tags for process setup
  NJET:=0; QCUT:=20.;

  EXCLUSIVE_CLUSTER_MODE=1;
  SOFT_SPIN_CORRELATIONS=1

  % improve colour-flow treatment
  CSS_CSMODE=1
  
  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;
}(run)

(processes){
  # V V -> e- antinu_e e+ nu_e
  Process 93 93 -> 11 -12 -11 12 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> e- antinu_e mu+ nu_mu
  Process 93 93 -> 11 -12 -13 14 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> e- antinu_e tau+ nu_tau
  Process 93 93 -> 11 -12 -15 16 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> mu- antinu_mu e+ nu_e
  Process 93 93 -> 13 -14 -11 12 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> mu- antinu_mu mu+ nu_mu
  Process 93 93 -> 13 -14 -13 14 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> mu- antinu_mu tau+ nu_tau
  Process 93 93 -> 13 -14 -15 16 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> tau- antinu_tau e+ nu_e
  Process 93 93 -> 15 -16 -11 12 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> tau- antinu_tau mu+ nu_mu
  Process 93 93 -> 15 -16 -13 14 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> tau- antinu_tau tau+ nu_tau
  Process 93 93 -> 15 -16 -15 16 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> e- e+  nu_mu antinu_mu
  Process 93 93 -> 11 -11 14 -14 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> e- e+  nu_tau antinu_tau
  Process 93 93 -> 11 -11 16 -16 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> mu- mu+  nu_e antinu_e
  Process 93 93 -> 13 -13 12 -12 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> mu- mu+  nu_tau antinu_tau
  Process 93 93 -> 13 -13 16 -16 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> tau- tau+  nu_e antinu_e
  Process 93 93 -> 15 -15 12 -12 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

  # V V -> tau- tau+  nu_mu antinu_mu
  Process 93 93 -> 15 -15 14 -14 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;

}(processes)

(selector){
  Mass 11 -11 0.1 E_CMS
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -5.0,5.0:-5.0,5.0 [PT_UP]
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.NCores = 16

#--------------------------------------------------------------
# VBF Mjj Interval filter
#--------------------------------------------------------------

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

# Set up VBF filters
if not hasattr(filtSeq, "VBFForwardJetsFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
  VBFfilter = VBFForwardJetsFilter("VBFfilter")
  filtSeq += VBFfilter

filtSeq.VBFfilter.JetMinPt=20.*GeV
filtSeq.VBFfilter.JetMaxEta=5.0
filtSeq.VBFfilter.NJets=2
filtSeq.VBFfilter.Jet1MinPt=30.*GeV
filtSeq.VBFfilter.Jet1MaxEta=5.0
filtSeq.VBFfilter.Jet2MinPt=20.*GeV
filtSeq.VBFfilter.Jet2MaxEta=5.0
filtSeq.VBFfilter.UseOppositeSignEtaJet1Jet2=False
filtSeq.VBFfilter.TruthJetContainer="AntiKt4TruthJets"
filtSeq.VBFfilter.LGMinPt=15.*GeV
filtSeq.VBFfilter.LGMaxEta=2.5
filtSeq.VBFfilter.DeltaRJLG=0.05
filtSeq.VBFfilter.RatioPtJLG=0.3
filtSeq.VBFfilter.UseLeadingJJ=True
filtSeq.VBFfilter.MassJJ = 300.*GeV
filtSeq.VBFfilter.DeltaEtaJJ = 3.0


