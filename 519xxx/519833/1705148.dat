# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:03
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.98801641E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.26020206E+03  # scale for input parameters
    1    5.02574446E+01  # M_1
    2   -1.79212611E+03  # M_2
    3    4.44910239E+03  # M_3
   11   -3.83043781E+03  # A_t
   12    1.53935679E+03  # A_b
   13    2.70223324E+02  # A_tau
   23    1.60779889E+02  # mu
   25    2.82119075E+00  # tan(beta)
   26    4.97716950E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.45136953E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.99959736E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.12301944E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.26020206E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.26020206E+03  # (SUSY scale)
  1  1     8.89550698E-06   # Y_u(Q)^DRbar
  2  2     4.51891755E-03   # Y_c(Q)^DRbar
  3  3     1.07464869E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.26020206E+03  # (SUSY scale)
  1  1     5.04293538E-05   # Y_d(Q)^DRbar
  2  2     9.58157723E-04   # Y_s(Q)^DRbar
  3  3     5.00101167E-02   # Y_b(Q)^DRbar
Block Ye Q=  4.26020206E+03  # (SUSY scale)
  1  1     8.80029101E-06   # Y_e(Q)^DRbar
  2  2     1.81962107E-03   # Y_mu(Q)^DRbar
  3  3     3.06030330E-02   # Y_tau(Q)^DRbar
Block Au Q=  4.26020206E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.83044050E+03   # A_t(Q)^DRbar
Block Ad Q=  4.26020206E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.53935664E+03   # A_b(Q)^DRbar
Block Ae Q=  4.26020206E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     2.70223272E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.26020206E+03  # soft SUSY breaking masses at Q
   1    5.02574446E+01  # M_1
   2   -1.79212611E+03  # M_2
   3    4.44910239E+03  # M_3
  21    2.19571162E+07  # M^2_(H,d)
  22    3.10418263E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.45136953E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.99959736E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.12301944E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.14093612E+02  # h0
        35     4.97751929E+03  # H0
        36     4.97716950E+03  # A0
        37     4.98062628E+03  # H+
   1000001     1.01030249E+04  # ~d_L
   2000001     1.00794152E+04  # ~d_R
   1000002     1.01027280E+04  # ~u_L
   2000002     1.00821595E+04  # ~u_R
   1000003     1.01030252E+04  # ~s_L
   2000003     1.00794152E+04  # ~s_R
   1000004     1.01027283E+04  # ~c_L
   2000004     1.00821599E+04  # ~c_R
   1000005     2.33365561E+03  # ~b_1
   2000005     3.59986583E+03  # ~b_2
   1000006     3.59860189E+03  # ~t_1
   2000006     5.04343692E+03  # ~t_2
   1000011     1.00202798E+04  # ~e_L-
   2000011     1.00092922E+04  # ~e_R-
   1000012     1.00195822E+04  # ~nu_eL
   1000013     1.00202799E+04  # ~mu_L-
   2000013     1.00092923E+04  # ~mu_R-
   1000014     1.00195823E+04  # ~nu_muL
   1000015     1.00093143E+04  # ~tau_1-
   2000015     1.00202913E+04  # ~tau_2-
   1000016     1.00195935E+04  # ~nu_tauL
   1000021     4.92718585E+03  # ~g
   1000022     4.02085101E+01  # ~chi_10
   1000023     1.89411438E+02  # ~chi_20
   1000025     2.01440227E+02  # ~chi_30
   1000035     1.90699504E+03  # ~chi_40
   1000024     1.90451905E+02  # ~chi_1+
   1000037     1.90696297E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.23203436E-01   # alpha
Block Hmix Q=  4.26020206E+03  # Higgs mixing parameters
   1    1.60779889E+02  # mu
   2    2.82119075E+00  # tan[beta](Q)
   3    2.42923503E+02  # v(Q)
   4    2.47722162E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99052339E-01   # Re[R_st(1,1)]
   1  2     4.35249867E-02   # Re[R_st(1,2)]
   2  1    -4.35249867E-02   # Re[R_st(2,1)]
   2  2     9.99052339E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.07913275E-04   # Re[R_sb(1,1)]
   1  2     9.99999953E-01   # Re[R_sb(1,2)]
   2  1    -9.99999953E-01   # Re[R_sb(2,1)]
   2  2    -3.07913275E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.36888425E-03   # Re[R_sta(1,1)]
   1  2     9.99999063E-01   # Re[R_sta(1,2)]
   2  1    -9.99999063E-01   # Re[R_sta(2,1)]
   2  2     1.36888425E-03   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.63777851E-01   # Re[N(1,1)]
   1  2    -8.15225447E-03   # Re[N(1,2)]
   1  3    -2.35944017E-01   # Re[N(1,3)]
   1  4     1.24081484E-01   # Re[N(1,4)]
   2  1     8.03565372E-02   # Re[N(2,1)]
   2  2     2.03120681E-02   # Re[N(2,2)]
   2  3    -7.01293021E-01   # Re[N(2,3)]
   2  4    -7.08038378E-01   # Re[N(2,4)]
   3  1     2.54311288E-01   # Re[N(3,1)]
   3  2    -3.36927996E-02   # Re[N(3,2)]
   3  3    -6.72615350E-01   # Re[N(3,3)]
   3  4     6.94103130E-01   # Re[N(3,4)]
   4  1     9.21454507E-04   # Re[N(4,1)]
   4  2    -9.99192552E-01   # Re[N(4,2)]
   4  3     1.03494149E-02   # Re[N(4,3)]
   4  4    -3.88108830E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1     1.46089312E-02   # Re[U(1,1)]
   1  2     9.99893284E-01   # Re[U(1,2)]
   2  1    -9.99893284E-01   # Re[U(2,1)]
   2  2     1.46089312E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.48876509E-02   # Re[V(1,1)]
   1  2     9.98492537E-01   # Re[V(1,2)]
   2  1     9.98492537E-01   # Re[V(2,1)]
   2  2    -5.48876509E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02886595E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.28918623E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.45311110E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     6.46274774E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.35498369E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.90379891E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.30084296E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.39474168E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01561429E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.38835967E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04566161E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02899777E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.28895013E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.45938524E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     6.46317110E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.35499030E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.90378268E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.30323053E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.39692703E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01559962E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.38835291E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04563219E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.06616310E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.22304012E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     8.21234204E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.57982691E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.67450233E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.35684954E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.89858072E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.97619556E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.01869216E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.01146106E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.39842748E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.03733356E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.35498344E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.35674359E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.66357341E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     9.34908413E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02132139E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.95967573E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02825308E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.35499005E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.35670291E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.66356531E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     9.34903861E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02130668E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.96453245E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02822375E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.35685070E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.34525269E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.66128592E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     9.33622873E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.01716744E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.33158456E-03    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     6.01996787E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.66423615E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.12061609E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.79652985E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.87936326E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.88716010E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.01844491E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.70788704E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.01582209E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.40467529E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.86850078E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.66423982E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.12061740E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.79830320E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.87935549E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.88720330E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.01844756E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.70926575E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.01577085E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.40466522E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.86844357E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.52869829E+00   # ~b_1
#    BR                NDA      ID1      ID2
     7.95336207E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     4.23115369E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     8.82310580E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     7.41198438E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.07103069E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.04659312E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.23069030E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.66802066E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.64602663E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.62168037E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.54827085E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     4.83593114E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.32449600E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.00421677E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.00870705E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.53445874E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.88700638E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.44115322E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.01116166E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.54798153E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.40073176E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.86818636E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.83601309E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.32443575E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.04659185E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.01273453E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.53429738E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.88704923E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.44124280E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.01111126E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.55107152E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.40072161E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.86812908E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.07011182E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.85769897E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.83947547E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.64582931E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     7.58010322E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.72983330E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.53547114E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.80913169E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.86058107E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.16539783E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.02035176E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.98574059E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.16669511E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.03134579E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.32175814E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.79019410E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     3.39146774E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.24367167E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.07524339E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999634E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.52281284E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.35250106E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.43808244E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.27813228E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.41972320E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.39917935E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.21367883E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.93060959E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.20546771E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     8.27809557E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.51325146E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     9.12556261E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.74003801E-02    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     6.35444647E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     2.19124065E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     7.80853750E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.66546272E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.21476336E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.21476336E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     7.97423727E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.66160252E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.47459064E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.18140777E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     6.04956609E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.26256956E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.04833596E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     6.01621567E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     6.38395343E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.20195659E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.20195659E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.51292599E+02   # ~g
#    BR                NDA      ID1      ID2
     1.02924241E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.02924241E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.90004070E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.90004070E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.04759586E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.04759586E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.13268227E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.07782693E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.08156300E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.07705738E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.07705738E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     6.86747934E-03   # Gamma(h0)
     9.08326175E-04   2        22        22   # BR(h0 -> photon photon)
     2.65287307E-04   2        22        23   # BR(h0 -> photon Z)
     3.43521459E-03   2        23        23   # BR(h0 -> Z Z)
     3.47668344E-02   2       -24        24   # BR(h0 -> W W)
     3.14174450E-02   2        21        21   # BR(h0 -> gluon gluon)
     2.28081277E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.01451823E-04   2       -13        13   # BR(h0 -> Muon muon)
     2.92449974E-02   2       -15        15   # BR(h0 -> Tau tau)
     6.60196973E-08   2        -2         2   # BR(h0 -> Up up)
     1.28102101E-02   2        -4         4   # BR(h0 -> Charm charm)
     2.68371089E-07   2        -1         1   # BR(h0 -> Down down)
     9.70630626E-05   2        -3         3   # BR(h0 -> Strange strange)
     2.57970396E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     6.28982438E-01   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     6.89914282E+01   # Gamma(HH)
     1.96938114E-07   2        22        22   # BR(HH -> photon photon)
     3.16023300E-08   2        22        23   # BR(HH -> photon Z)
     1.32931386E-05   2        23        23   # BR(HH -> Z Z)
     2.20904294E-06   2       -24        24   # BR(HH -> W W)
     4.31191636E-05   2        21        21   # BR(HH -> gluon gluon)
     8.74171353E-11   2       -11        11   # BR(HH -> Electron electron)
     3.89284240E-06   2       -13        13   # BR(HH -> Muon muon)
     1.12438995E-03   2       -15        15   # BR(HH -> Tau tau)
     1.72420070E-11   2        -2         2   # BR(HH -> Up up)
     3.34521318E-06   2        -4         4   # BR(HH -> Charm charm)
     2.50664635E-01   2        -6         6   # BR(HH -> Top top)
     6.05587302E-09   2        -1         1   # BR(HH -> Down down)
     2.19045826E-06   2        -3         3   # BR(HH -> Strange strange)
     5.62714326E-03   2        -5         5   # BR(HH -> Bottom bottom)
     1.52417885E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.18556069E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.18556069E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.20732915E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     6.63381673E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.64910802E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     7.70851871E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     9.98133608E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.30779009E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     9.91333607E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.71008187E-01   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.66101494E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.80307890E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.05210014E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.13579410E-04   2        25        25   # BR(HH -> h0 h0)
     5.33049572E-07   2  -1000005   1000005   # BR(HH -> Sbottom1 sbottom1)
DECAY        36     6.72966566E+01   # Gamma(A0)
     2.09754823E-07   2        22        22   # BR(A0 -> photon photon)
     7.48236614E-08   2        22        23   # BR(A0 -> photon Z)
     5.51751064E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.57512702E-11   2       -11        11   # BR(A0 -> Electron electron)
     3.81873447E-06   2       -13        13   # BR(A0 -> Muon muon)
     1.10298544E-03   2       -15        15   # BR(A0 -> Tau tau)
     1.59050796E-11   2        -2         2   # BR(A0 -> Up up)
     3.08575619E-06   2        -4         4   # BR(A0 -> Charm charm)
     2.32107507E-01   2        -6         6   # BR(A0 -> Top top)
     5.94040908E-09   2        -1         1   # BR(A0 -> Down down)
     2.14867957E-06   2        -3         3   # BR(A0 -> Strange strange)
     5.51879726E-03   2        -5         5   # BR(A0 -> Bottom bottom)
     2.20038886E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.23581108E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.23581108E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.46586931E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.41089906E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.34204189E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.96173093E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.28985166E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.30736879E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.49368464E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.50010380E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.62519787E-02   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.56260680E-01   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.23770425E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     3.75396206E-05   2        23        25   # BR(A0 -> Z h0)
     9.13143437E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     7.27025170E+01   # Gamma(Hp)
     1.01148892E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     4.32443107E-06   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.22319556E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.10296446E-09   2        -1         2   # BR(Hp -> Down up)
     1.03265807E-07   2        -3         2   # BR(Hp -> Strange up)
     6.60742403E-08   2        -5         2   # BR(Hp -> Bottom up)
     1.71248897E-07   2        -1         4   # BR(Hp -> Down charm)
     5.82697144E-06   2        -3         4   # BR(Hp -> Strange charm)
     9.25843212E-06   2        -5         4   # BR(Hp -> Bottom charm)
     1.87837621E-05   2        -1         6   # BR(Hp -> Down top)
     4.09551547E-04   2        -3         6   # BR(Hp -> Strange top)
     2.93284878E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.34931376E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.07731253E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.84707369E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.04013067E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     5.67400059E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.81536323E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.07672278E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.70107299E-10   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     3.48287255E-05   2        24        25   # BR(Hp -> W h0)
     5.42376783E-12   2        24        35   # BR(Hp -> W HH)
     9.24704216E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.03736765E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    8.05538048E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    7.95911725E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.01209471E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.13547362E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.25642074E-01        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.03736765E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    8.05538048E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    7.95911725E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99695900E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.04100015E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99695900E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.04100015E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.28997323E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.08273210E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    3.59447367E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.04100015E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99695900E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.22387377E-04   # BR(b -> s gamma)
    2    1.59087832E-06   # BR(b -> s mu+ mu-)
    3    3.52721563E-05   # BR(b -> s nu nu)
    4    2.58449794E-15   # BR(Bd -> e+ e-)
    5    1.10406818E-10   # BR(Bd -> mu+ mu-)
    6    2.31125710E-08   # BR(Bd -> tau+ tau-)
    7    8.70390116E-14   # BR(Bs -> e+ e-)
    8    3.71830378E-09   # BR(Bs -> mu+ mu-)
    9    7.88683611E-07   # BR(Bs -> tau+ tau-)
   10    9.68067467E-05   # BR(B_u -> tau nu)
   11    9.99976631E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42467845E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93827761E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15924181E-03   # epsilon_K
   17    2.28167815E-15   # Delta(M_K)
   18    2.48152793E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29402105E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.67214002E-17   # Delta(g-2)_electron/2
   21   -1.56995470E-12   # Delta(g-2)_muon/2
   22   -4.44087920E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -1.20141505E-03   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.91021866E-01   # C7
     0305 4322   00   2    -5.41563145E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.88218648E-02   # C8
     0305 6321   00   2    -7.62771465E-05   # C8'
 03051111 4133   00   0     1.62851500E+00   # C9 e+e-
 03051111 4133   00   2     1.62875742E+00   # C9 e+e-
 03051111 4233   00   2     3.13254289E-06   # C9' e+e-
 03051111 4137   00   0    -4.45120710E+00   # C10 e+e-
 03051111 4137   00   2    -4.45146002E+00   # C10 e+e-
 03051111 4237   00   2    -2.23627073E-05   # C10' e+e-
 03051313 4133   00   0     1.62851500E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62875742E+00   # C9 mu+mu-
 03051313 4233   00   2     3.13254288E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.45120710E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45146002E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.23627073E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50540609E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.83254113E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50540609E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.83254113E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50540609E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.83254121E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822536E-07   # C7
     0305 4422   00   2    -9.05841460E-08   # C7
     0305 4322   00   2     6.85709559E-08   # C7'
     0305 6421   00   0     3.30482518E-07   # C8
     0305 6421   00   2    -4.24610897E-07   # C8
     0305 6321   00   2     1.57794909E-08   # C8'
 03051111 4133   00   2    -9.66407749E-08   # C9 e+e-
 03051111 4233   00   2     5.77448376E-08   # C9' e+e-
 03051111 4137   00   2     2.62914741E-06   # C10 e+e-
 03051111 4237   00   2    -4.12684536E-07   # C10' e+e-
 03051313 4133   00   2    -9.66407921E-08   # C9 mu+mu-
 03051313 4233   00   2     5.77448375E-08   # C9' mu+mu-
 03051313 4137   00   2     2.62914743E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.12684536E-07   # C10' mu+mu-
 03051212 4137   00   2    -5.45064513E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     8.91810820E-08   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.45064512E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     8.91810820E-08   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.45064299E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     8.91810817E-08   # C11' nu_3 nu_3
