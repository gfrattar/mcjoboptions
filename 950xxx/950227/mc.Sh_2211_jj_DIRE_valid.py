include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "pp -> j j @ LO in QCD (no electroweak processes included) using the DIRE parton shower."
evgenConfig.keywords = [ "jets", "dijet", "LO", "QCD", "SM"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch"]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""
(run){
 ACTIVE[25]=0
 CORE_SCALE QCD
 SHOWER_GENERATOR=Dire
 CSS_IS_AS_FAC=1
 CSS_FS_AS_FAC=1
 CSS_IS_PT2MIN=3
 CSS_FS_PT2MIN=3
}(run)

(processes){
 Process 93 93 -> 93 93
 Enhance_Function: VAR{PPerp2(p[2])/100} {2}
 Order (*,0)
 Integration_Error 0.02 {2}
 End process
}(processes)

(selector){
 NJetFinder  2  10.0  0.0  0.4  -1  999.0  10.0
 NJetFinder  1  20.0  0.0  0.4  -1  999.0  10.0
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "MASS[6]=14000.0", "WIDTH[6]=0" ]

