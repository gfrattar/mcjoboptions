include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa LO onshell Z->cc/bb"
evgenConfig.keywords = ["SM", "Z", "LO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch","matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 50000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic;

  # tags for process setup
  NJET:=0; LJET:=0; QCUT:=20.;

  % Force Z->cc/bb
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,5,-5]=2
  HDH_STATUS[23,4,-4]=2

}(run)

(processes){
  Process 93 93 -> 23;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  Max_N_Quarks 4 {1};
  Max_Epsilon 0.01 {1};
  Integration_Error 0.99 {1};
  End process;
}(processes)
"""

genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]

include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"

# Don't some states with mass mismatches between EvtGen and Sherpa
# These are all strongly decaying states, so it is fine to use Sherpa decays
genSeq.EvtInclusiveDecay.whiteList.remove(10411)
genSeq.EvtInclusiveDecay.whiteList.remove(-10411)
genSeq.EvtInclusiveDecay.whiteList.remove(10421)
genSeq.EvtInclusiveDecay.whiteList.remove(-10421)
genSeq.EvtInclusiveDecay.whiteList.remove(-10423)
genSeq.EvtInclusiveDecay.whiteList.remove(10423)
genSeq.EvtInclusiveDecay.whiteList.remove(10431)
genSeq.EvtInclusiveDecay.whiteList.remove(-10431)
genSeq.EvtInclusiveDecay.whiteList.remove(-10433)
genSeq.EvtInclusiveDecay.whiteList.remove(10433)
genSeq.EvtInclusiveDecay.whiteList.remove(-20433)
genSeq.EvtInclusiveDecay.whiteList.remove(20433)
