from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo lvll+jets 0,1j@NLO FxFx merged'
evgenConfig.contact = ["danning.liu@cern.ch","matthew.gignac@cern.ch"]
evgenConfig.keywords += ['WZ','neutrino','3lepton','jets']
evgenConfig.generators += ["aMcAtNlo","Pythia8"]

evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*5.0 if runArgs.maxEvents>0 else 5.0*evgenConfig.nEventsPerJob

gridpack_mode=True

name='FxFx_lvll'

if not is_gen_from_gridpack():
    process="""
    import model loop_sm-no_b_mass
    define wpm = w+ w-
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define l = l+ l-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define v = vl vl~
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > l v l+ l- [QCD] @0
    add process p p > l v l+ l- j [QCD] @1
    output -f"""
    process_dir=str(new_process(process))
else:
    process_dir=str(MADGRAPH_GRIDPACK_LOCATION)

#Fet#ch default LO run_card.dat and set parameters
settings = {'lhe_version'   :'3.0',
            'nevents':int(nevents),
            'ickkw'         : 3,
            'ptl'           : '10.0',
            'etal'          : '3.0',
            'ptj'           : 10, 
            'etaj'          : 5,
            'drll'          : '0.0',
            'mll_sf'        : -1,
            'mll'           : 10,
            'maxjetflavor'  : 5,
            'parton_shower' : 'PYTHIA8',
            'jetradius'     : 1.0,
            'PDF_set_min'   : 260001, 
            'PDF_set_max'   : 260100 
            }

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

runName='FxFx_lvlljj'

input_events=process_dir+'/Events/GridRun_'+str(runArgs.randomSeed)+'/events.lhe.gz'
    
generate(process_dir=process_dir, runArgs=runArgs, grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_aMcAtNlo.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 3 

#simil-NLO parameters
genSeq.Pythia8.Commands += ["JetMatching:merge          = on", 
                            "SpaceShower:alphaSuseCMW    = on",              
                            "SpaceShower:alphaSorder     = 2",               
                            "TimeShower:alphaSuseCMW     = on",              
                            "TimeShower:alphaSorder      = 2",               
                            "SpaceShower:alphaSvalue    = 0.118",
                            "TimeShower:alphaSvalue     = 0.118" 
] 

PYTHIA8_nJetMax=1
PYTHIA8_qCut=25.

print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "SpaceShower:rapidityOrder    = off",
                            "SpaceShower:pTmaxFudge       = 1.0", 
                            "JetMatching:qCut             = %f"%PYTHIA8_qCut,
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx           = on",
                            "JetMatching:qCutME           = 8.0",
                            "JetMatching:nJetMax          = %i"%PYTHIA8_nJetMax, 
                            'JetMatching:jetAlgorithm = 2', #explicit setting of kt-merging for FxFx (also imposed by Py8-FxFx inteface)
                            'JetMatching:slowJetPower = 1', #explicit setting of kt-merging for FxFx (also imposed by Py8-FxFx inteface)
                            'JetMatching:nQmatch = 5', #4 corresponds to 4-flavour scheme (no matching of b-quarks), 5 for 5-flavour scheme
                            "JetMatching:eTjetMin = %f"%PYTHIA8_qCut #This is 20 in the Pythia default, it should be <= qCut
                            ]

genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS = True
