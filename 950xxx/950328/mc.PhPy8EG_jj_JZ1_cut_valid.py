#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dijet production with A14 NNPDF2.3 tune. Test a tighter bornktmin cut, no Powheg weights"
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["amoroso@cern.ch", "jan.kretzschmar@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.generators = ["Powheg", "Pythia8"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_jj_Common.py")
PowhegConfig.bornktmin =10
PowhegConfig.bornsuppfact =60

PowhegConfig.mu_F         = 1.
PowhegConfig.mu_R         = 1.
PowhegConfig.PDF          = 14000

PowhegConfig.ncall1       = 160000
PowhegConfig.ncall2       = 160000
PowhegConfig.nubound      = 9600000
PowhegConfig.itmx1        = 20
PowhegConfig.itmx2        = 20

### Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = 5
PowhegConfig.foldphi      = 5
PowhegConfig.foldy        = 5

# filteff ~ 0.56
safetyfactor = 1/0.56*1.1
PowhegConfig.nEvents = runArgs.maxEvents*safetyfactor if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*safetyfactor

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(1, filtSeq)
