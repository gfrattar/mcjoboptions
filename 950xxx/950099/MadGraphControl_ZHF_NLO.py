import os, shutil
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
MADGRAPH_CATCH_ERRORS=False

# General settings
nevents = int(1.1*runArgs.maxEvents)
#mode = 2
#njobs = 8
#gridpack_mode = True
#gridpack_dir = 'madevent/'

## Specific settings
mllcut = 60
model = "loop_sm"
parton_shower = 'PYTHIA8'
jets = "g u c d s u~ c~ d~ s~"
mgproc = "generate p p > mu+ mu- b b~ [QCD]"
name = '4fl_Zmmbb'
keyword = ['SM','Z','2lepton']

## Cutfile setup
cutsfile = None
if "PTV0_150" in jofile:
    name += "_PTV0_150"
    cutsfile = "cuts_PTLL0_150.f"
    #pt_max_pdg="{23:150}"
elif "PTV150" in jofile:
    name += "_PTV150_ECM"
    cutsfile = "cuts_PTLL150_ECM.f"
    #pt_min_pdg="{23:150}"
else:
    print("ERROR: JO name should contain 'PTV0_150' or 'PTV150_ECM'.")

import subprocess
getfile = subprocess.Popen(['get_files','-data',cutsfile])
getfile.wait()


## Process definition
process = """
import model {model}
define p = {jets}
define j = {jets}
{mgproc}
output -f
""".format(jets=jets, mgproc=mgproc, model=model)

## Gridpack/process handling
if not is_gen_from_gridpack():
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

## Settings
settings = {'nevents':int(nevents), 'parton_shower':'PYTHIA8', 'mll_sf' : mllcut}
lhe_version = 3

## Apply settings
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)
shutil.copyfile(cutsfile, process_dir+"/SubProcesses/cuts.f")

## Generation
generate(process_dir=process_dir, grid_pack=True, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=True)

## Metadata
evgenConfig.description = 'aMcAtNlo_'+str(name)
evgenConfig.keywords += keyword
evgenConfig.contact = ['andy.buckley@cern.ch', 'mcfayden@cern.ch']

## Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
