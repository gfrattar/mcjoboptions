# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 16.05.2022,  00:06
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.45653165E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.20007084E+03  # scale for input parameters
    1    1.24941502E+02  # M_1
    2   -8.87910843E+02  # M_2
    3    4.24154619E+03  # M_3
   11    3.56202828E+03  # A_t
   12   -1.56100234E+03  # A_b
   13   -1.71087831E+03  # A_tau
   23   -5.69583224E+02  # mu
   25    2.35826444E+01  # tan(beta)
   26    4.99857189E+03  # m_A, pole mass
   31    6.09163665E+02  # M_L11
   32    6.09163665E+02  # M_L22
   33    1.29324756E+03  # M_L33
   34    7.53953609E+01  # M_E11
   35    7.53953609E+01  # M_E22
   36    1.98963493E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.26718696E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.99999395E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.94646275E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.20007084E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.20007084E+03  # (SUSY scale)
  1  1     8.39190674E-06   # Y_u(Q)^DRbar
  2  2     4.26308862E-03   # Y_c(Q)^DRbar
  3  3     1.01380974E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.20007084E+03  # (SUSY scale)
  1  1     3.97679678E-04   # Y_d(Q)^DRbar
  2  2     7.55591388E-03   # Y_s(Q)^DRbar
  3  3     3.94373626E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.20007084E+03  # (SUSY scale)
  1  1     6.93980119E-05   # Y_e(Q)^DRbar
  2  2     1.43493078E-02   # Y_mu(Q)^DRbar
  3  3     2.41331752E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.20007084E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.56202828E+03   # A_t(Q)^DRbar
Block Ad Q=  3.20007084E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.56100233E+03   # A_b(Q)^DRbar
Block Ae Q=  3.20007084E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.71087831E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.20007084E+03  # soft SUSY breaking masses at Q
   1    1.24941502E+02  # M_1
   2   -8.87910843E+02  # M_2
   3    4.24154619E+03  # M_3
  21    2.46316453E+07  # M^2_(H,d)
  22   -5.69238517E+04  # M^2_(H,u)
  31    6.09163665E+02  # M_(L,11)
  32    6.09163665E+02  # M_(L,22)
  33    1.29324756E+03  # M_(L,33)
  34    7.53953609E+01  # M_(E,11)
  35    7.53953609E+01  # M_(E,22)
  36    1.98963493E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.26718696E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.99999395E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.94646275E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23892946E+02  # h0
        35     4.99758320E+03  # H0
        36     4.99857189E+03  # A0
        37     4.99880643E+03  # H+
   1000001     1.00865305E+04  # ~d_L
   2000001     1.00625173E+04  # ~d_R
   1000002     1.00862338E+04  # ~u_L
   2000002     1.00655329E+04  # ~u_R
   1000003     1.00865335E+04  # ~s_L
   2000003     1.00625216E+04  # ~s_R
   1000004     1.00862368E+04  # ~c_L
   2000004     1.00655342E+04  # ~c_R
   1000005     3.06578097E+03  # ~b_1
   2000005     3.35463199E+03  # ~b_2
   1000006     3.03460118E+03  # ~t_1
   2000006     3.37456317E+03  # ~t_2
   1000011     6.28362277E+02  # ~e_L-
   2000011     1.26087253E+02  # ~e_R-
   1000012     6.22871781E+02  # ~nu_eL
   1000013     6.28362913E+02  # ~mu_L-
   2000013     1.26032691E+02  # ~mu_R-
   1000014     6.22866260E+02  # ~nu_muL
   1000015     2.13255986E+02  # ~tau_1-
   2000015     1.30131211E+03  # ~tau_2-
   1000016     1.29827774E+03  # ~nu_tauL
   1000021     4.65778062E+03  # ~g
   1000022     1.21907250E+02  # ~chi_10
   1000023     5.65414985E+02  # ~chi_20
   1000025     5.76941845E+02  # ~chi_30
   1000035     9.48576215E+02  # ~chi_40
   1000024     5.65811968E+02  # ~chi_1+
   1000037     9.48635901E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.07254011E-02   # alpha
Block Hmix Q=  3.20007084E+03  # Higgs mixing parameters
   1   -5.69583224E+02  # mu
   2    2.35826444E+01  # tan[beta](Q)
   3    2.43178648E+02  # v(Q)
   4    2.49857209E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -2.36135824E-01   # Re[R_st(1,1)]
   1  2     9.71720059E-01   # Re[R_st(1,2)]
   2  1    -9.71720059E-01   # Re[R_st(2,1)]
   2  2    -2.36135824E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.60090066E-02   # Re[R_sb(1,1)]
   1  2     9.99871848E-01   # Re[R_sb(1,2)]
   2  1    -9.99871848E-01   # Re[R_sb(2,1)]
   2  2    -1.60090066E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.28515985E-02   # Re[R_sta(1,1)]
   1  2     9.99917415E-01   # Re[R_sta(1,2)]
   2  1    -9.99917415E-01   # Re[R_sta(2,1)]
   2  2    -1.28515985E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.96748635E-01   # Re[N(1,1)]
   1  2     8.06589872E-04   # Re[N(1,2)]
   1  3    -7.94156249E-02   # Re[N(1,3)]
   1  4    -1.35892083E-02   # Re[N(1,4)]
   2  1     4.63548589E-02   # Re[N(2,1)]
   2  2     1.56631447E-01   # Re[N(2,2)]
   2  3     7.02006744E-01   # Re[N(2,3)]
   2  4    -6.93184210E-01   # Re[N(2,4)]
   3  1     6.56496383E-02   # Re[N(3,1)]
   3  2    -3.58108048E-02   # Re[N(3,2)]
   3  3     7.02502225E-01   # Re[N(3,3)]
   3  4     7.07741715E-01   # Re[N(3,4)]
   4  1    -5.78884195E-03   # Re[N(4,1)]
   4  2     9.87007358E-01   # Re[N(4,2)]
   4  3    -8.58505316E-02   # Re[N(4,3)]
   4  4     1.35693221E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.21530589E-01   # Re[U(1,1)]
   1  2    -9.92587687E-01   # Re[U(1,2)]
   2  1    -9.92587687E-01   # Re[U(2,1)]
   2  2     1.21530589E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.92246543E-01   # Re[V(1,1)]
   1  2     9.81346660E-01   # Re[V(1,2)]
   2  1     9.81346660E-01   # Re[V(2,1)]
   2  2    -1.92246543E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     2.67608275E-03   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     7.34698551E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.91533540E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.48658555E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.97978108E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     2.60834276E-03   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     7.35761115E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.90122518E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.54430894E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.97410658E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.31647028E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     4.83661777E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     6.72287534E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.39101770E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.05990376E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.18053750E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.81621404E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.53115654E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.68971447E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     1.67010234E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.58883778E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     7.24705027E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     9.89032791E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.03726850E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.83477178E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     8.54646372E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     7.24770620E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.88933118E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.03672789E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.83356960E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     8.64679673E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     6.75672737E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.34875623E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     9.65683643E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.73663971E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.80163448E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.84729670E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.54726619E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     3.31111631E-02    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     4.95984785E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.12497816E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88677038E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.25091474E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.21660632E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.55384282E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.59233053E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.81134181E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.08480320E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.37483947E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.88388150E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.96007872E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.12494077E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88631902E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.25106793E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.21666791E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.56275608E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.68189583E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.81119014E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.09033232E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.37480835E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.88369317E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.93309374E+01   # ~b_1
#    BR                NDA      ID1      ID2
     9.15497341E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.27457935E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.24806211E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.80027649E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.50812797E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.57304616E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.19873821E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.24386739E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.25612000E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.06553186E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.02525787E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.18603529E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.26201473E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.49584945E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.04926227E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.45403886E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.13146560E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.35072075E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.87552847E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.56210265E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.25081775E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.25723505E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.91879769E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.78284441E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.21685623E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.34385568E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.88352049E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.13153893E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.35065909E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.91080060E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.56196798E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.25097060E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.25718153E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.92153920E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.78269047E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.23462160E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.34382590E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.88333210E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.21405349E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.13143277E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.38847399E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.44188154E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.34125200E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.64395356E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     5.54342610E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.22797873E+02   # ~t_2
#    BR                NDA      ID1      ID2
     7.96501763E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.45148214E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.56178254E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.13985605E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     8.10610434E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.15673198E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.46915153E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     3.50931588E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.46485934E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     5.79342396E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     1.78006077E-03    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     4.13818299E-01    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     5.81660126E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.72273400E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.25418104E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.03320562E-01    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.03320235E-01    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     2.94607910E-04    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.03759691E-01    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.03762999E-01    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     2.60624732E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.64604289E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.40894205E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.44103985E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.31853924E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.32220682E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     6.50582628E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     7.24771353E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.80301803E-03    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     3.80301803E-03    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     4.51576343E-03    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     4.51576343E-03    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     1.68815410E-01    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     1.68815410E-01    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     3.22861864E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.22695424E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.40826277E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     6.15346945E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     9.20541888E-03    2     2000011       -11   # BR(chi^0_3 -> ~e^-_R e^+)
     9.20541888E-03    2    -2000011        11   # BR(chi^0_3 -> ~e^+_R e^-)
     1.00677679E-02    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     1.00677679E-02    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     2.06890820E-01    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     2.06890820E-01    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.03575886E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.43890426E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.97224071E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     1.43736684E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     4.42806352E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     4.42806352E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     4.42809008E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     4.42809008E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     1.33395207E-04    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.33395207E-04    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     4.60674562E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     4.60674562E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     4.60686943E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     4.60686943E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     1.44511884E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.44511884E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.45193482E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.64796994E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.33440597E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.24916440E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.08153890E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     9.68164903E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.33257334E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.33257334E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.53396232E+02   # ~g
#    BR                NDA      ID1      ID2
     8.18395022E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     8.18395022E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.57098223E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.57098223E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     9.35590068E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     9.35590068E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.44369078E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.44369078E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.04080898E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.04080898E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.35892212E-03   # Gamma(h0)
     2.41629739E-03   2        22        22   # BR(h0 -> photon photon)
     1.60318048E-03   2        22        23   # BR(h0 -> photon Z)
     2.77750661E-02   2        23        23   # BR(h0 -> Z Z)
     2.34579320E-01   2       -24        24   # BR(h0 -> W W)
     8.17227342E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.02671043E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.23596218E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.44025186E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.43099139E-07   2        -2         2   # BR(h0 -> Up up)
     2.77722608E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.81736957E-07   2        -1         1   # BR(h0 -> Down down)
     2.10400386E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.59293896E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     9.90555243E+01   # Gamma(HH)
     1.89887745E-08   2        22        22   # BR(HH -> photon photon)
     4.58828621E-08   2        22        23   # BR(HH -> photon Z)
     2.38982604E-07   2        23        23   # BR(HH -> Z Z)
     3.95498589E-08   2       -24        24   # BR(HH -> W W)
     1.01931154E-06   2        21        21   # BR(HH -> gluon gluon)
     4.46136285E-09   2       -11        11   # BR(HH -> Electron electron)
     1.98674956E-04   2       -13        13   # BR(HH -> Muon muon)
     5.40999812E-02   2       -15        15   # BR(HH -> Tau tau)
     1.95581733E-13   2        -2         2   # BR(HH -> Up up)
     3.79453239E-08   2        -4         4   # BR(HH -> Charm charm)
     3.08622290E-03   2        -6         6   # BR(HH -> Top top)
     2.84651643E-07   2        -1         1   # BR(HH -> Down down)
     1.02966112E-04   2        -3         3   # BR(HH -> Strange strange)
     3.11377381E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.61513816E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.78137762E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.78137762E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.49692902E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     7.24559191E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.76870677E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.94060425E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.56839210E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.99013357E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     7.88815692E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     7.58939698E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.05980605E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.03875571E-01   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.79911060E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.09792439E-06   2        25        25   # BR(HH -> h0 h0)
     5.80906117E-08   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     5.29420062E-14   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     5.29420062E-14   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     8.71066103E-08   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     5.76927653E-08   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     2.26328831E-09   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     2.26328831E-09   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     8.70773606E-08   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
     2.24324584E-06   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
     3.41006514E-03   2  -2000015   1000015   # BR(HH -> Stau2 stau1)
     3.41006514E-03   2  -1000015   2000015   # BR(HH -> Stau1 stau2)
     2.74964877E-06   2  -2000015   2000015   # BR(HH -> Stau2 stau2)
DECAY        36     9.70596637E+01   # Gamma(A0)
     4.64654701E-08   2        22        22   # BR(A0 -> photon photon)
     7.32079804E-08   2        22        23   # BR(A0 -> photon Z)
     5.36092121E-06   2        21        21   # BR(A0 -> gluon gluon)
     4.30496801E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.91710635E-04   2       -13        13   # BR(A0 -> Muon muon)
     5.22026282E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.75800061E-13   2        -2         2   # BR(A0 -> Up up)
     3.41065356E-08   2        -4         4   # BR(A0 -> Charm charm)
     2.79604610E-03   2        -6         6   # BR(A0 -> Top top)
     2.74654665E-07   2        -1         1   # BR(A0 -> Down down)
     9.93517294E-05   2        -3         3   # BR(A0 -> Strange strange)
     3.00451057E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.92932937E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.79096802E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.79096802E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     8.29749356E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     7.20088265E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.26202090E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.57147150E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     8.37760310E-05   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     5.03590240E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.83170623E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     9.88144274E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     9.69036165E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     8.28524121E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     4.21041256E-03   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     6.75872331E-07   2        23        25   # BR(A0 -> Z h0)
     3.71142778E-39   2        25        25   # BR(A0 -> h0 h0)
     5.44483170E-14   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     5.44483170E-14   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     2.32783563E-09   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     2.32783563E-09   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
     3.48209547E-03   2  -2000015   1000015   # BR(A0 -> Stau2 stau1)
     3.48209547E-03   2  -1000015   2000015   # BR(A0 -> Stau1 stau2)
DECAY        37     1.05271790E+02   # Gamma(Hp)
     4.73870537E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.02594455E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     5.73052579E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.90692393E-07   2        -1         2   # BR(Hp -> Down up)
     4.93466551E-06   2        -3         2   # BR(Hp -> Strange up)
     3.66075284E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.54041370E-08   2        -1         4   # BR(Hp -> Down charm)
     1.04807888E-04   2        -3         4   # BR(Hp -> Strange charm)
     5.12636548E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.92580452E-07   2        -1         6   # BR(Hp -> Down top)
     4.35555421E-06   2        -3         6   # BR(Hp -> Strange top)
     3.48108206E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.61598464E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.24049660E-04   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     6.57447829E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.80836298E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     3.71176549E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.71574539E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.74165059E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.04257938E-07   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     6.23375224E-07   2        24        25   # BR(Hp -> W h0)
     3.54250811E-14   2        24        35   # BR(Hp -> W HH)
     9.34651773E-14   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     3.21999997E-07   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     3.99602131E-09   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     3.21956584E-07   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     6.42037147E-03   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
     2.19162237E-06   2  -2000015   1000016   # BR(Hp -> Stau2 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.23540270E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.56217577E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.56141117E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00013748E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.66062217E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.79810478E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.23540270E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.56217577E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.56141117E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997267E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.73331979E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997267E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.73331979E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26724392E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.64485333E-04        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.25947330E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.73331979E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997267E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.19587116E-04   # BR(b -> s gamma)
    2    1.58990119E-06   # BR(b -> s mu+ mu-)
    3    3.52429793E-05   # BR(b -> s nu nu)
    4    2.55488393E-15   # BR(Bd -> e+ e-)
    5    1.09141701E-10   # BR(Bd -> mu+ mu-)
    6    2.28446634E-08   # BR(Bd -> tau+ tau-)
    7    8.64966243E-14   # BR(Bs -> e+ e-)
    8    3.69513151E-09   # BR(Bs -> mu+ mu-)
    9    7.83655031E-07   # BR(Bs -> tau+ tau-)
   10    9.66734634E-05   # BR(B_u -> tau nu)
   11    9.98599866E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42045306E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93670496E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15755654E-03   # epsilon_K
   17    2.28166061E-15   # Delta(M_K)
   18    2.47914802E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28851007E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.27913571E-14   # Delta(g-2)_electron/2
   21   -5.50095063E-10   # Delta(g-2)_muon/2
   22   -1.72191150E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.55193133E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.87634236E-01   # C7
     0305 4322   00   2    -3.28448689E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.71963682E-02   # C8
     0305 6321   00   2    -6.42683569E-05   # C8'
 03051111 4133   00   0     1.61867804E+00   # C9 e+e-
 03051111 4133   00   2     1.61901028E+00   # C9 e+e-
 03051111 4233   00   2     1.30486842E-04   # C9' e+e-
 03051111 4137   00   0    -4.44137015E+00   # C10 e+e-
 03051111 4137   00   2    -4.43873410E+00   # C10 e+e-
 03051111 4237   00   2    -9.76857877E-04   # C10' e+e-
 03051313 4133   00   0     1.61867804E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61900939E+00   # C9 mu+mu-
 03051313 4233   00   2     1.30486697E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44137015E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43873499E+00   # C10 mu+mu-
 03051313 4237   00   2    -9.76858164E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50478931E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.11600219E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50478931E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.11600220E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50479611E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.11601577E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85835202E-07   # C7
     0305 4422   00   2     4.70207578E-06   # C7
     0305 4322   00   2     4.24262143E-07   # C7'
     0305 6421   00   0     3.30493368E-07   # C8
     0305 6421   00   2     3.99084000E-06   # C8
     0305 6321   00   2     1.96995951E-07   # C8'
 03051111 4133   00   2    -2.16543029E-07   # C9 e+e-
 03051111 4233   00   2     2.44843966E-06   # C9' e+e-
 03051111 4137   00   2     5.13362788E-07   # C10 e+e-
 03051111 4237   00   2    -1.83301676E-05   # C10' e+e-
 03051313 4133   00   2    -2.16557700E-07   # C9 mu+mu-
 03051313 4233   00   2     2.44843720E-06   # C9' mu+mu-
 03051313 4137   00   2     5.13387523E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.83301728E-05   # C10' mu+mu-
 03051212 4137   00   2     6.89564302E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     3.97056759E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     6.89558783E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     3.97056759E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     6.81475941E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     3.97058453E-06   # C11' nu_3 nu_3
