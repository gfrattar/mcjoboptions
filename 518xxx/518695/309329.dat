# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  14:08
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.08503772E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.82453198E+03  # scale for input parameters
    1    2.50643496E+02  # M_1
    2   -5.34345026E+02  # M_2
    3    7.56441911E+02  # M_3
   11   -6.56731594E+03  # A_t
   12    1.71004465E+03  # A_b
   13    2.31973997E+03  # A_tau
   23   -3.58773002E+03  # mu
   25    2.01135012E+01  # tan(beta)
   26    8.51460589E+02  # m_A, pole mass
   31    1.74587182E+03  # M_L11
   32    1.74587182E+03  # M_L22
   33    1.94812763E+03  # M_L33
   34    4.28930452E+02  # M_E11
   35    4.28930452E+02  # M_E22
   36    3.17580673E+02  # M_E33
   41    2.15221920E+03  # M_Q11
   42    2.15221920E+03  # M_Q22
   43    2.39608086E+03  # M_Q33
   44    2.94022103E+03  # M_U11
   45    2.94022103E+03  # M_U22
   46    1.61304684E+03  # M_U33
   47    2.88516740E+03  # M_D11
   48    2.88516740E+03  # M_D22
   49    7.71843407E+02  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.82453198E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.82453198E+03  # (SUSY scale)
  1  1     8.39472825E-06   # Y_u(Q)^DRbar
  2  2     4.26452195E-03   # Y_c(Q)^DRbar
  3  3     1.01415060E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.82453198E+03  # (SUSY scale)
  1  1     3.39292739E-04   # Y_d(Q)^DRbar
  2  2     6.44656204E-03   # Y_s(Q)^DRbar
  3  3     3.36472078E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.82453198E+03  # (SUSY scale)
  1  1     5.92090640E-05   # Y_e(Q)^DRbar
  2  2     1.22425566E-02   # Y_mu(Q)^DRbar
  3  3     2.05899662E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.82453198E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.56731613E+03   # A_t(Q)^DRbar
Block Ad Q=  1.82453198E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.71004473E+03   # A_b(Q)^DRbar
Block Ae Q=  1.82453198E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     2.31973995E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.82453198E+03  # soft SUSY breaking masses at Q
   1    2.50643496E+02  # M_1
   2   -5.34345026E+02  # M_2
   3    7.56441911E+02  # M_3
  21   -1.21165213E+07  # M^2_(H,d)
  22   -1.27903395E+07  # M^2_(H,u)
  31    1.74587182E+03  # M_(L,11)
  32    1.74587182E+03  # M_(L,22)
  33    1.94812763E+03  # M_(L,33)
  34    4.28930452E+02  # M_(E,11)
  35    4.28930452E+02  # M_(E,22)
  36    3.17580673E+02  # M_(E,33)
  41    2.15221920E+03  # M_(Q,11)
  42    2.15221920E+03  # M_(Q,22)
  43    2.39608086E+03  # M_(Q,33)
  44    2.94022103E+03  # M_(U,11)
  45    2.94022103E+03  # M_(U,22)
  46    1.61304684E+03  # M_(U,33)
  47    2.88516740E+03  # M_(D,11)
  48    2.88516740E+03  # M_(D,22)
  49    7.71843407E+02  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.12577839E+02  # h0
        35     8.49068281E+02  # H0
        36     8.51460589E+02  # A0
        37     8.43131879E+02  # H+
   1000001     2.18682499E+03  # ~d_L
   2000001     2.91842936E+03  # ~d_R
   1000002     2.18562246E+03  # ~u_L
   2000002     2.97321914E+03  # ~u_R
   1000003     2.18682313E+03  # ~s_L
   2000003     2.91842717E+03  # ~s_R
   1000004     2.18562158E+03  # ~c_L
   2000004     2.97321820E+03  # ~c_R
   1000005     8.07767240E+02  # ~b_1
   2000005     2.28945874E+03  # ~b_2
   1000006     1.42246730E+03  # ~t_1
   2000006     2.34024146E+03  # ~t_2
   1000011     1.74937960E+03  # ~e_L-
   2000011     4.40844353E+02  # ~e_R-
   1000012     1.74732311E+03  # ~nu_eL
   1000013     1.74937449E+03  # ~mu_L-
   2000013     4.40757943E+02  # ~mu_R-
   1000014     1.74731169E+03  # ~nu_muL
   1000015     3.00278025E+02  # ~tau_1-
   2000015     1.94963208E+03  # ~tau_2-
   1000016     1.94643791E+03  # ~nu_tauL
   1000021     9.22199480E+02  # ~g
   1000022     2.47760459E+02  # ~chi_10
   1000023     5.64448814E+02  # ~chi_20
   1000025     3.55844811E+03  # ~chi_30
   1000035     3.55866564E+03  # ~chi_40
   1000024     5.64633796E+02  # ~chi_1+
   1000037     3.55932777E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.46751444E-02   # alpha
Block Hmix Q=  1.82453198E+03  # Higgs mixing parameters
   1   -3.58773002E+03  # mu
   2    2.01135012E+01  # tan[beta](Q)
   3    2.43735572E+02  # v(Q)
   4    7.24985135E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     2.62958215E-01   # Re[R_st(1,1)]
   1  2     9.64807223E-01   # Re[R_st(1,2)]
   2  1    -9.64807223E-01   # Re[R_st(2,1)]
   2  2     2.62958215E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -4.04617744E-02   # Re[R_sb(1,1)]
   1  2     9.99181087E-01   # Re[R_sb(1,2)]
   2  1    -9.99181087E-01   # Re[R_sb(2,1)]
   2  2    -4.04617744E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.67909417E-02   # Re[R_sta(1,1)]
   1  2     9.99322984E-01   # Re[R_sta(1,2)]
   2  1    -9.99322984E-01   # Re[R_sta(2,1)]
   2  2    -3.67909417E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99918764E-01   # Re[N(1,1)]
   1  2     9.01345419E-05   # Re[N(1,2)]
   1  3    -1.27360455E-02   # Re[N(1,3)]
   1  4    -5.01110971E-04   # Re[N(1,4)]
   2  1     1.92342582E-04   # Re[N(2,1)]
   2  2     9.99742039E-01   # Re[N(2,2)]
   2  3     2.23377663E-02   # Re[N(2,3)]
   2  4    -4.10399943E-03   # Re[N(2,4)]
   3  1     9.36001269E-03   # Re[N(3,1)]
   3  2    -1.28949709E-02   # Re[N(3,2)]
   3  3     7.06951237E-01   # Re[N(3,3)]
   3  4     7.07082780E-01   # Re[N(3,4)]
   4  1     8.64981256E-03   # Re[N(4,1)]
   4  2    -1.86967166E-02   # Re[N(4,2)]
   4  3     7.06794713E-01   # Re[N(4,3)]
   4  4    -7.07118694E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.99501670E-01   # Re[U(1,1)]
   1  2    -3.15660034E-02   # Re[U(1,2)]
   2  1    -3.15660034E-02   # Re[U(2,1)]
   2  2     9.99501670E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99983129E-01   # Re[V(1,1)]
   1  2     5.80877621E-03   # Re[V(1,2)]
   2  1     5.80877621E-03   # Re[V(2,1)]
   2  2    -9.99983129E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     1.03659782E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     2.06349401E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.02282595E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.99406495E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.98310898E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     1.03601950E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     2.06450906E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.02232020E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.99258120E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.98014169E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.94136442E-04    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.53355523E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
DECAY   2000015     3.14611631E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.56211272E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.28430019E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.56443905E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.00085193E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.43389367E-02    2     1000015        36   # BR(~tau^-_2 -> ~tau^-_1 A^0)
     9.04147662E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
     2.46660537E-02    2     1000015        35   # BR(~tau^-_2 -> ~tau^-_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     2.06082668E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.02215710E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.99154692E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.98629598E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     2.06081138E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.02215746E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.99154047E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.98628472E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     3.17424620E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.45141435E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.26251253E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     4.52796552E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.97949663E-01    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
     4.84883891E-02    2     1000015        37   # BR(~nu_tau -> ~tau^-_1 H^+)
DECAY   1000001     1.42108986E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.09055971E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.89734356E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.17887207E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     8.21048798E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000001     1.88260264E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.52902121E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91470967E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     1.42108756E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.09056330E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.89734733E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.17887155E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     8.21048809E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   2000003     1.88260849E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.52899022E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91466805E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     3.77757741E-01   # ~b_1
#    BR                NDA      ID1      ID2
     9.82325290E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     7.84532211E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     9.82938831E-03    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     3.11518350E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.95792385E-04    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.84431448E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.60700522E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.07190881E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     4.13097616E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     5.64895995E-02    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.42754015E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     2.54568416E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.82963442E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     2.59548361E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   1000002     1.41989243E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.09535072E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.89895269E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.18016817E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.20898306E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000002     1.98249173E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.30228400E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.66977159E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     1.41988993E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.09535177E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.89895461E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.18016968E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.20898134E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   2000004     1.98248978E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.30228469E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.66977148E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     4.29160668E+01   # ~t_1
#    BR                NDA      ID1      ID2
     6.34451765E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     6.43730944E-03    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.34049398E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     9.15145839E-01    2     1000021         6   # BR(~t_1 -> ~g t)
     1.56252798E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.40857046E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.84948878E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.47711690E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     5.01512864E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.67325791E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.65123825E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     3.39793510E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     2.03189401E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.49318633E-02    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     2.44471906E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     3.33987767E-02    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.77209584E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98632696E-01    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     1.00589866E-03    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.03943726E-04    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.03948766E-04    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
DECAY   1000037     1.77967585E+02   # chi^+_2
#    BR                NDA      ID1      ID2
     8.23273930E-03    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     4.14102829E-03    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     4.07363137E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     3.36850849E-02    2     2000006        -5   # BR(chi^+_2 -> ~t_2 b_bar)
     1.01976388E-04    2    -1000001         2   # BR(chi^+_2 -> ~d^*_L u)
     1.06135219E-04    2    -1000003         4   # BR(chi^+_2 -> ~s^*_L c)
     6.27200969E-02    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     2.07285406E-01    2    -2000005         6   # BR(chi^+_2 -> ~b^*_2 t)
     1.37957382E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.10783269E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.13138292E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     4.03498717E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     4.10813083E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     3.76574283E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     4.50572553E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     3.78047636E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     4.31558196E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     7.47234223E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.54323621E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.69662373E-04    3     1000021        -5         6   # BR(chi^+_2 -> ~g b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.78814418E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     4.96424134E-01    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     4.96424134E-01    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     1.03854972E-03    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.26582143E-03    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.28454885E-04    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.28459012E-04    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.39738937E-04    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.80779730E+02   # chi^0_3
#    BR                NDA      ID1      ID2
     4.07656681E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     4.07656681E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.04208137E-03    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     2.04208137E-03    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     2.23995641E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     2.23995641E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     8.86112483E-02    2     2000006        -6   # BR(chi^0_3 -> ~t_2 t_bar)
     8.86112483E-02    2    -2000006         6   # BR(chi^0_3 -> ~t^*_2 t)
     2.98098312E-02    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     2.98098312E-02    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     1.14636083E-02    2     2000005        -5   # BR(chi^0_3 -> ~b_2 b_bar)
     1.14636083E-02    2    -2000005         5   # BR(chi^0_3 -> ~b^*_2 b)
     4.05192996E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     4.05192996E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.71515613E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     3.71515613E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     6.25364246E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.74686655E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.20956385E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     1.15814474E-02    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     6.39439344E-03    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.57553655E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     6.84919252E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     2.76814808E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.89333128E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.96604331E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     2.67898042E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     2.67898042E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
     3.50398768E-04    3     1000021         5        -5   # BR(chi^0_3 -> ~g b b_bar)
DECAY   1000035     1.81056692E+02   # chi^0_4
#    BR                NDA      ID1      ID2
     4.06288829E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     4.06288829E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     2.04399772E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     2.04399772E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     2.27065457E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     2.27065457E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     8.77811349E-02    2     2000006        -6   # BR(chi^0_4 -> ~t_2 t_bar)
     8.77811349E-02    2    -2000006         6   # BR(chi^0_4 -> ~t^*_2 t)
     2.97271479E-02    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     2.97271479E-02    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     1.14633856E-02    2     2000005        -5   # BR(chi^0_4 -> ~b_2 b_bar)
     1.14633856E-02    2    -2000005         5   # BR(chi^0_4 -> ~b^*_2 b)
     4.04632969E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.04632969E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.95877272E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     3.95877272E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     7.30741987E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.30353072E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.12609115E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.41034835E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     7.63630352E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.07395760E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     5.71294154E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     2.30899180E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.13902548E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.11211286E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.73242933E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.73242933E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
     3.49465014E-04    3     1000021         5        -5   # BR(chi^0_4 -> ~g b b_bar)
DECAY   1000021     1.48314104E+00   # ~g
#    BR                NDA      ID1      ID2
     4.99907168E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.99907168E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.40315944E-03   # Gamma(h0)
     2.51896148E-03   2        22        22   # BR(h0 -> photon photon)
     6.23826798E-04   2        22        23   # BR(h0 -> photon Z)
     7.74437665E-03   2        23        23   # BR(h0 -> Z Z)
     7.99247140E-02   2       -24        24   # BR(h0 -> W W)
     8.30431779E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.29781710E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.80128925E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.07472436E-02   2       -15        15   # BR(h0 -> Tau tau)
     2.14775763E-07   2        -2         2   # BR(h0 -> Up up)
     4.16676513E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.25904139E-07   2        -1         1   # BR(h0 -> Down down)
     2.62561221E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.03186411E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     8.17904453E+00   # Gamma(HH)
     6.40367827E-08   2        22        22   # BR(HH -> photon photon)
     5.37373166E-08   2        22        23   # BR(HH -> photon Z)
     1.52512691E-04   2        23        23   # BR(HH -> Z Z)
     2.41880169E-04   2       -24        24   # BR(HH -> W W)
     5.23847387E-05   2        21        21   # BR(HH -> gluon gluon)
     7.82968956E-09   2       -11        11   # BR(HH -> Electron electron)
     3.48479775E-04   2       -13        13   # BR(HH -> Muon muon)
     1.00151071E-01   2       -15        15   # BR(HH -> Tau tau)
     3.49892501E-13   2        -2         2   # BR(HH -> Up up)
     6.78561259E-08   2        -4         4   # BR(HH -> Charm charm)
     5.53738061E-03   2        -6         6   # BR(HH -> Top top)
     8.92028993E-07   2        -1         1   # BR(HH -> Down down)
     3.22450638E-04   2        -3         3   # BR(HH -> Strange strange)
     8.87194860E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.84616673E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     7.98177196E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.73920307E-03   2        25        25   # BR(HH -> h0 h0)
     1.16041246E-03   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
DECAY        36     8.04022552E+00   # Gamma(A0)
     1.41016099E-07   2        22        22   # BR(A0 -> photon photon)
     3.30066133E-08   2        22        23   # BR(A0 -> photon Z)
     1.33654672E-04   2        21        21   # BR(A0 -> gluon gluon)
     7.83696181E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.48803890E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.00246355E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.85665735E-13   2        -2         2   # BR(A0 -> Up up)
     1.13475078E-07   2        -4         4   # BR(A0 -> Charm charm)
     1.05257955E-02   2        -6         6   # BR(A0 -> Top top)
     8.92669599E-07   2        -1         1   # BR(A0 -> Down down)
     3.22682128E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.88135209E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.88293196E-05   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.42907734E-05   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.43192079E-04   2        23        25   # BR(A0 -> Z h0)
     1.06710856E-11   2        23        35   # BR(A0 -> Z HH)
     1.63421816E-33   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.74065125E+00   # Gamma(Hp)
     8.99253465E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.84458931E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.08745961E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.46049803E-07   2        -1         2   # BR(Hp -> Down up)
     1.57009169E-05   2        -3         2   # BR(Hp -> Strange up)
     9.89377171E-06   2        -5         2   # BR(Hp -> Bottom up)
     5.13087656E-08   2        -1         4   # BR(Hp -> Down charm)
     3.40877134E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.38547647E-03   2        -5         4   # BR(Hp -> Bottom charm)
     7.92122085E-07   2        -1         6   # BR(Hp -> Down top)
     1.77289992E-05   2        -3         6   # BR(Hp -> Strange top)
     8.88708734E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.05877950E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.83492148E-04   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    8.08891968E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.04744039E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.04552931E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00047239E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.99947129E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.47186443E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    8.08891968E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.04744039E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.04552931E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99974982E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.50177868E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99974982E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.50177868E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.22195276E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.73043733E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.08774049E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.50177868E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99974982E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.80777617E-04   # BR(b -> s gamma)
    2    1.58283268E-06   # BR(b -> s mu+ mu-)
    3    3.51562904E-05   # BR(b -> s nu nu)
    4    1.75985421E-15   # BR(Bd -> e+ e-)
    5    7.51748264E-11   # BR(Bd -> mu+ mu-)
    6    1.54933146E-08   # BR(Bd -> tau+ tau-)
    7    6.34464969E-14   # BR(Bs -> e+ e-)
    8    2.71031602E-09   # BR(Bs -> mu+ mu-)
    9    5.67956350E-07   # BR(Bs -> tau+ tau-)
   10    9.08511049E-05   # BR(B_u -> tau nu)
   11    9.38457131E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42708126E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93492273E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16428709E-03   # epsilon_K
   17    2.28173902E-15   # Delta(M_K)
   18    2.48483066E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.27963177E-11   # BR(K^+ -> pi^+ nu nu)
   20   -9.01240778E-16   # Delta(g-2)_electron/2
   21   -3.85072772E-11   # Delta(g-2)_muon/2
   22   -1.40337802E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    2.67949544E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.61524612E-01   # C7
     0305 4322   00   2    -1.34110890E-03   # C7'
     0305 6421   00   0    -9.52278347E-02   # C8
     0305 6421   00   2    -1.19907726E-01   # C8
     0305 6321   00   2    -1.01330247E-03   # C8'
 03051111 4133   00   0     1.59880553E+00   # C9 e+e-
 03051111 4133   00   2     1.59747698E+00   # C9 e+e-
 03051111 4233   00   2     1.09337514E-05   # C9' e+e-
 03051111 4137   00   0    -4.42149763E+00   # C10 e+e-
 03051111 4137   00   2    -4.41019146E+00   # C10 e+e-
 03051111 4237   00   2    -7.54552594E-05   # C10' e+e-
 03051313 4133   00   0     1.59880553E+00   # C9 mu+mu-
 03051313 4133   00   2     1.59747613E+00   # C9 mu+mu-
 03051313 4233   00   2     1.09319535E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.42149763E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.41019231E+00   # C10 mu+mu-
 03051313 4237   00   2    -7.54535201E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50293210E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.63887612E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50293210E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.63891530E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50293166E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.64998972E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85650567E-07   # C7
     0305 4422   00   2    -1.52993324E-04   # C7
     0305 4322   00   2    -8.87680493E-06   # C7'
     0305 6421   00   0     3.30335215E-07   # C8
     0305 6421   00   2     7.74740238E-04   # C8
     0305 6321   00   2    -7.49428763E-07   # C8'
 03051111 4133   00   2    -3.54512907E-05   # C9 e+e-
 03051111 4233   00   2     1.43322246E-06   # C9' e+e-
 03051111 4137   00   2     2.96205053E-04   # C10 e+e-
 03051111 4237   00   2    -1.09006721E-05   # C10' e+e-
 03051313 4133   00   2    -3.54512371E-05   # C9 mu+mu-
 03051313 4233   00   2     1.43322251E-06   # C9' mu+mu-
 03051313 4137   00   2     2.96205057E-04   # C10 mu+mu-
 03051313 4237   00   2    -1.09006732E-05   # C10' mu+mu-
 03051212 4137   00   2    -6.36692331E-05   # C11 nu_1 nu_1
 03051212 4237   00   2     2.37145897E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.36692337E-05   # C11 nu_2 nu_2
 03051414 4237   00   2     2.37145897E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.36787346E-05   # C11 nu_3 nu_3
 03051616 4237   00   2     2.37146481E-06   # C11' nu_3 nu_3
