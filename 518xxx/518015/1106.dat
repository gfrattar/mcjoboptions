# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  12:47
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.06333139E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.76134164E+03  # scale for input parameters
    1   -1.41445305E+02  # M_1
    2   -1.08972702E+03  # M_2
    3    2.12840174E+03  # M_3
   11    4.72252545E+03  # A_t
   12   -1.71303751E+03  # A_b
   13   -1.09890142E+03  # A_tau
   23    7.19086938E+02  # mu
   25    3.94139451E+01  # tan(beta)
   26    1.22362822E+03  # m_A, pole mass
   31    1.00779903E+03  # M_L11
   32    1.00779903E+03  # M_L22
   33    8.40084358E+02  # M_L33
   34    9.60593947E+02  # M_E11
   35    9.60593947E+02  # M_E22
   36    1.21378854E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.36249208E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.24843083E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.10294209E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.76134164E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.76134164E+03  # (SUSY scale)
  1  1     8.38707031E-06   # Y_u(Q)^DRbar
  2  2     4.26063172E-03   # Y_c(Q)^DRbar
  3  3     1.01322546E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.76134164E+03  # (SUSY scale)
  1  1     6.64263575E-04   # Y_d(Q)^DRbar
  2  2     1.26210079E-02   # Y_s(Q)^DRbar
  3  3     6.58741317E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.76134164E+03  # (SUSY scale)
  1  1     1.15918851E-04   # Y_e(Q)^DRbar
  2  2     2.39683420E-02   # Y_mu(Q)^DRbar
  3  3     4.03108083E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.76134164E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.72252545E+03   # A_t(Q)^DRbar
Block Ad Q=  2.76134164E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.71303756E+03   # A_b(Q)^DRbar
Block Ae Q=  2.76134164E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.09890142E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.76134164E+03  # soft SUSY breaking masses at Q
   1   -1.41445305E+02  # M_1
   2   -1.08972702E+03  # M_2
   3    2.12840174E+03  # M_3
  21    1.02712940E+06  # M^2_(H,d)
  22   -4.07724661E+05  # M^2_(H,u)
  31    1.00779903E+03  # M_(L,11)
  32    1.00779903E+03  # M_(L,22)
  33    8.40084358E+02  # M_(L,33)
  34    9.60593947E+02  # M_(E,11)
  35    9.60593947E+02  # M_(E,22)
  36    1.21378854E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.36249208E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.24843083E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.10294209E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26359466E+02  # h0
        35     1.22365795E+03  # H0
        36     1.22362822E+03  # A0
        37     1.22893631E+03  # H+
   1000001     1.01117133E+04  # ~d_L
   2000001     1.00873448E+04  # ~d_R
   1000002     1.01113579E+04  # ~u_L
   2000002     1.00908085E+04  # ~u_R
   1000003     1.01117219E+04  # ~s_L
   2000003     1.00873595E+04  # ~s_R
   1000004     1.01113664E+04  # ~c_L
   2000004     1.00908103E+04  # ~c_R
   1000005     3.15334633E+03  # ~b_1
   2000005     3.36811166E+03  # ~b_2
   1000006     2.25626799E+03  # ~t_1
   2000006     3.37947783E+03  # ~t_2
   1000011     1.02531050E+03  # ~e_L-
   2000011     9.63630970E+02  # ~e_R-
   1000012     1.02183295E+03  # ~nu_eL
   1000013     1.02532879E+03  # ~mu_L-
   2000013     9.63560642E+02  # ~mu_R-
   1000014     1.02181567E+03  # ~nu_muL
   1000015     8.51838482E+02  # ~tau_1-
   2000015     1.20699914E+03  # ~tau_2-
   1000016     8.50559631E+02  # ~nu_tauL
   1000021     2.52180390E+03  # ~g
   1000022     1.40121986E+02  # ~chi_10
   1000023     7.24740522E+02  # ~chi_20
   1000025     7.33423412E+02  # ~chi_30
   1000035     1.15085550E+03  # ~chi_40
   1000024     7.25112706E+02  # ~chi_1+
   1000037     1.15089848E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.49625853E-02   # alpha
Block Hmix Q=  2.76134164E+03  # Higgs mixing parameters
   1    7.19086938E+02  # mu
   2    3.94139451E+01  # tan[beta](Q)
   3    2.43313706E+02  # v(Q)
   4    1.49726602E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99043284E-02   # Re[R_st(1,1)]
   1  2     9.94997048E-01   # Re[R_st(1,2)]
   2  1    -9.94997048E-01   # Re[R_st(2,1)]
   2  2    -9.99043284E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     4.29626992E-02   # Re[R_sb(1,1)]
   1  2     9.99076677E-01   # Re[R_sb(1,2)]
   2  1    -9.99076677E-01   # Re[R_sb(2,1)]
   2  2     4.29626992E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.97583546E-01   # Re[R_sta(1,1)]
   1  2     6.94771042E-02   # Re[R_sta(1,2)]
   2  1    -6.94771042E-02   # Re[R_sta(2,1)]
   2  2     9.97583546E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.97917346E-01   # Re[N(1,1)]
   1  2     7.18422146E-04   # Re[N(1,2)]
   1  3    -6.35754483E-02   # Re[N(1,3)]
   1  4    -1.08910942E-02   # Re[N(1,4)]
   2  1     5.24937387E-02   # Re[N(2,1)]
   2  2     1.28952603E-01   # Re[N(2,2)]
   2  3    -7.03046949E-01   # Re[N(2,3)]
   2  4    -6.97381259E-01   # Re[N(2,4)]
   3  1     3.71598078E-02   # Re[N(3,1)]
   3  2    -3.07763521E-02   # Re[N(3,2)]
   3  3    -7.04865416E-01   # Re[N(3,3)]
   3  4     7.07698178E-01   # Re[N(3,4)]
   4  1    -4.95235038E-03   # Re[N(4,1)]
   4  2     9.91172803E-01   # Re[N(4,2)]
   4  3     6.96268319E-02   # Re[N(4,3)]
   4  4     1.12712255E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.86285051E-02   # Re[U(1,1)]
   1  2     9.95124323E-01   # Re[U(1,2)]
   2  1    -9.95124323E-01   # Re[U(2,1)]
   2  2    -9.86285051E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.59647269E-01   # Re[V(1,1)]
   1  2     9.87174123E-01   # Re[V(1,2)]
   2  1     9.87174123E-01   # Re[V(2,1)]
   2  2    -1.59647269E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.62369924E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99199610E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     5.44421692E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.55957927E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.28171888E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.61056944E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.17276929E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.00233346E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.71151290E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.62742062E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.98325885E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.65836154E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.65014032E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.43264665E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.28462814E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.58934691E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.28064273E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.18098824E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.70778927E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.25970864E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     8.30180092E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     8.91783257E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     7.17557671E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.88581548E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     1.03001761E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     5.68012860E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     7.55398985E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.61329318E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.49026753E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.41004139E-04    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     6.59871248E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     3.17534423E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     3.33353266E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     1.30446513E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     9.45937081E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.67063290E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.07735616E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     4.33149304E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.30729400E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.43872967E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.65113167E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.07267690E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     4.54032248E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.23212165E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.19512590E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.32229853E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.12269341E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.77652842E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   2000001     7.05441393E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.94693146E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92019951E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.33472012E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.69927100E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.56120344E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.12015622E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.02823807E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.03037618E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.42203255E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.05505781E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.94640027E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91931243E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.33508120E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.69935371E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.74842769E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.11995720E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.03241745E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.03033358E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.42167675E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.28355468E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.30543281E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.47062262E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.45467249E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.78758644E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.92154657E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     7.64751780E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.90573271E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     5.44722446E-04    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     2.18062192E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.42900369E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     6.02954906E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.04714100E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.30560576E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.77350976E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.13274300E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.35301673E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.97331958E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.53347605E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.35785022E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.22641164E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.10417993E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.68829805E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.33459851E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.67197754E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.01088255E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.10185190E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.69413499E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.01395557E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.42178089E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.22648505E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.10414889E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.68820139E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.33495914E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.67190705E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.01295036E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.10164055E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.73159755E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.01391621E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.42142509E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.86322920E+01   # ~t_1
#    BR                NDA      ID1      ID2
     6.23697759E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.30945372E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.35941986E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.44821683E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.66439055E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.85441462E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.33698074E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.15707998E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.29283381E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.32167985E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.22963847E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.11391419E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.01707534E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.25690917E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     3.62715793E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     9.35629300E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.51379655E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     4.85143285E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.90548919E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     9.22032757E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     2.25771922E-04    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.04819062E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.03752934E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     2.03697016E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     9.65669764E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     2.11092110E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     2.11149271E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     9.74306007E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.33544883E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.96238467E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.74135066E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.76137981E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.72060675E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.53898728E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.62250097E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.20033174E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.36482731E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.71075525E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.26467206E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.09374003E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     1.02471899E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.25973109E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
DECAY   1000025     6.51792143E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.17143226E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.81164614E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.46451247E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.50496823E-04    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
DECAY   1000035     1.27641943E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     8.25044660E-03    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     8.25044660E-03    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     8.24823244E-03    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     8.24823244E-03    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     3.92253109E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     3.92253109E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     8.77925433E-03    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     8.77925433E-03    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     8.78146642E-03    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     8.78146642E-03    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     4.03556689E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     4.03556689E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.61935561E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.61935561E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.99351161E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.33950866E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.50468247E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.00886531E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.33086947E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.52866490E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.47091512E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.47091512E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.69105943E+00   # ~g
#    BR                NDA      ID1      ID2
     4.85277003E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.85277003E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.53569269E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.32148103E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.33454393E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     4.68278460E-04    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.00445047E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     3.91857122E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.97371146E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     3.87151867E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     5.07013800E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     7.11131507E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     5.90346272E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     5.90346272E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     5.86228694E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     5.86228694E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.72515168E-03   # Gamma(h0)
     2.55203570E-03   2        22        22   # BR(h0 -> photon photon)
     1.75719209E-03   2        22        23   # BR(h0 -> photon Z)
     3.40688240E-02   2        23        23   # BR(h0 -> Z Z)
     2.74076496E-01   2       -24        24   # BR(h0 -> W W)
     7.78975468E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.72154362E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.10023507E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.05542089E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.30878459E-07   2        -2         2   # BR(h0 -> Up up)
     2.54019328E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.46935850E-07   2        -1         1   # BR(h0 -> Down down)
     1.97812935E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.23283245E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.93057997E+01   # Gamma(HH)
     7.37884140E-09   2        22        22   # BR(HH -> photon photon)
     4.12576427E-10   2        22        23   # BR(HH -> photon Z)
     2.75879760E-06   2        23        23   # BR(HH -> Z Z)
     2.90215634E-06   2       -24        24   # BR(HH -> W W)
     4.08898743E-05   2        21        21   # BR(HH -> gluon gluon)
     1.21434565E-08   2       -11        11   # BR(HH -> Electron electron)
     5.40534014E-04   2       -13        13   # BR(HH -> Muon muon)
     1.52810615E-01   2       -15        15   # BR(HH -> Tau tau)
     9.75646750E-14   2        -2         2   # BR(HH -> Up up)
     1.89211501E-08   2        -4         4   # BR(HH -> Charm charm)
     1.02895606E-03   2        -6         6   # BR(HH -> Top top)
     1.14399478E-06   2        -1         1   # BR(HH -> Down down)
     4.13691024E-04   2        -3         3   # BR(HH -> Strange strange)
     8.14956768E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.29163041E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.29842808E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.66540316E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.42268605E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.87917729E+01   # Gamma(A0)
     1.85318955E-07   2        22        22   # BR(A0 -> photon photon)
     1.81203361E-07   2        22        23   # BR(A0 -> photon Z)
     6.99351818E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.21281098E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.39851470E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.52620610E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.00691104E-13   2        -2         2   # BR(A0 -> Up up)
     1.95144859E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.16822088E-03   2        -6         6   # BR(A0 -> Top top)
     1.14248072E-06   2        -1         1   # BR(A0 -> Down down)
     4.13143662E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.13903692E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.64418761E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.84421661E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.22725364E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.88540109E-06   2        23        25   # BR(A0 -> Z h0)
     6.91601947E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.19516174E+01   # Gamma(Hp)
     1.52857940E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     6.53515428E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.84850684E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.06174501E-06   2        -1         2   # BR(Hp -> Down up)
     1.77213254E-05   2        -3         2   # BR(Hp -> Strange up)
     8.49346752E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.99236661E-08   2        -1         4   # BR(Hp -> Down charm)
     3.82565668E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.18938725E-03   2        -5         4   # BR(Hp -> Bottom charm)
     9.34425071E-08   2        -1         6   # BR(Hp -> Down top)
     2.57909228E-06   2        -3         6   # BR(Hp -> Strange top)
     7.85483456E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.74069932E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.38394988E-06   2        24        25   # BR(Hp -> W h0)
     2.53210733E-10   2        24        35   # BR(Hp -> W HH)
     2.60420167E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.68429935E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.55349064E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.55345907E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002032E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.23402286E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.43724718E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.68429935E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.55349064E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.55345907E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999837E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.62976380E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999837E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.62976380E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25990745E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.59624409E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.46592223E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.62976380E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999837E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.71598027E-04   # BR(b -> s gamma)
    2    1.58713488E-06   # BR(b -> s mu+ mu-)
    3    3.52473414E-05   # BR(b -> s nu nu)
    4    2.29089769E-15   # BR(Bd -> e+ e-)
    5    9.78636603E-11   # BR(Bd -> mu+ mu-)
    6    2.04369959E-08   # BR(Bd -> tau+ tau-)
    7    7.60845376E-14   # BR(Bs -> e+ e-)
    8    3.25029462E-09   # BR(Bs -> mu+ mu-)
    9    6.87452557E-07   # BR(Bs -> tau+ tau-)
   10    8.95501209E-05   # BR(B_u -> tau nu)
   11    9.25018464E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42853192E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93639215E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16199245E-03   # epsilon_K
   17    2.28171289E-15   # Delta(M_K)
   18    2.47910598E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28849887E-11   # BR(K^+ -> pi^+ nu nu)
   20   -7.85665199E-15   # Delta(g-2)_electron/2
   21   -3.35901331E-10   # Delta(g-2)_muon/2
   22   -1.00795442E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.52283555E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.42994038E-01   # C7
     0305 4322   00   2    -6.45077531E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.42353717E-01   # C8
     0305 6321   00   2    -6.96901933E-04   # C8'
 03051111 4133   00   0     1.61399366E+00   # C9 e+e-
 03051111 4133   00   2     1.61462410E+00   # C9 e+e-
 03051111 4233   00   2     2.23453483E-04   # C9' e+e-
 03051111 4137   00   0    -4.43668576E+00   # C10 e+e-
 03051111 4137   00   2    -4.43452325E+00   # C10 e+e-
 03051111 4237   00   2    -1.68301288E-03   # C10' e+e-
 03051313 4133   00   0     1.61399366E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61461992E+00   # C9 mu+mu-
 03051313 4233   00   2     2.23445757E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43668576E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43452743E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.68300786E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50489082E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.64960052E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50489082E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.64961579E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50489079E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.65391556E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85764906E-07   # C7
     0305 4422   00   2    -6.50577212E-06   # C7
     0305 4322   00   2    -2.31914704E-06   # C7'
     0305 6421   00   0     3.30433154E-07   # C8
     0305 6421   00   2    -2.13676860E-05   # C8
     0305 6321   00   2    -1.39611709E-06   # C8'
 03051111 4133   00   2    -1.20297478E-07   # C9 e+e-
 03051111 4233   00   2     6.80750479E-06   # C9' e+e-
 03051111 4137   00   2     6.56607794E-07   # C10 e+e-
 03051111 4237   00   2    -5.13040568E-05   # C10' e+e-
 03051313 4133   00   2    -1.20313207E-07   # C9 mu+mu-
 03051313 4233   00   2     6.80749261E-06   # C9' mu+mu-
 03051313 4137   00   2     6.56597874E-07   # C10 mu+mu-
 03051313 4237   00   2    -5.13040930E-05   # C10' mu+mu-
 03051212 4137   00   2     2.22855226E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.11253979E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     2.22854713E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.11253979E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     2.15925808E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.11253939E-05   # C11' nu_3 nu_3
