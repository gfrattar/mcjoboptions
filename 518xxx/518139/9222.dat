# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  16:23
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.93989323E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.67696884E+03  # scale for input parameters
    1   -1.79051089E+02  # M_1
    2   -8.52010304E+02  # M_2
    3    4.67289440E+03  # M_3
   11    3.94694791E+03  # A_t
   12    8.55068926E+02  # A_b
   13   -1.32526652E+03  # A_tau
   23   -3.71283814E+02  # mu
   25    3.83797431E+01  # tan(beta)
   26    4.40817256E+03  # m_A, pole mass
   31    1.16365555E+03  # M_L11
   32    1.16365555E+03  # M_L22
   33    1.53820990E+03  # M_L33
   34    8.09767883E+02  # M_E11
   35    8.09767883E+02  # M_E22
   36    1.02746011E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.41070642E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.92121212E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.05810848E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.67696884E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.67696884E+03  # (SUSY scale)
  1  1     8.38721766E-06   # Y_u(Q)^DRbar
  2  2     4.26070657E-03   # Y_c(Q)^DRbar
  3  3     1.01324326E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.67696884E+03  # (SUSY scale)
  1  1     6.46844999E-04   # Y_d(Q)^DRbar
  2  2     1.22900550E-02   # Y_s(Q)^DRbar
  3  3     6.41467547E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.67696884E+03  # (SUSY scale)
  1  1     1.12879182E-04   # Y_e(Q)^DRbar
  2  2     2.33398349E-02   # Y_mu(Q)^DRbar
  3  3     3.92537626E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.67696884E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.94694791E+03   # A_t(Q)^DRbar
Block Ad Q=  3.67696884E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.55068919E+02   # A_b(Q)^DRbar
Block Ae Q=  3.67696884E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.32526651E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.67696884E+03  # soft SUSY breaking masses at Q
   1   -1.79051089E+02  # M_1
   2   -8.52010304E+02  # M_2
   3    4.67289440E+03  # M_3
  21    1.95591367E+07  # M^2_(H,d)
  22    1.32484893E+05  # M^2_(H,u)
  31    1.16365555E+03  # M_(L,11)
  32    1.16365555E+03  # M_(L,22)
  33    1.53820990E+03  # M_(L,33)
  34    8.09767883E+02  # M_(E,11)
  35    8.09767883E+02  # M_(E,22)
  36    1.02746011E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.41070642E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.92121212E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.05810848E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24595962E+02  # h0
        35     4.40637257E+03  # H0
        36     4.40817256E+03  # A0
        37     4.40599917E+03  # H+
   1000001     1.00913588E+04  # ~d_L
   2000001     1.00670986E+04  # ~d_R
   1000002     1.00909945E+04  # ~u_L
   2000002     1.00697117E+04  # ~u_R
   1000003     1.00913627E+04  # ~s_L
   2000003     1.00671053E+04  # ~s_R
   1000004     1.00909984E+04  # ~c_L
   2000004     1.00697126E+04  # ~c_R
   1000005     3.18139094E+03  # ~b_1
   2000005     4.50268945E+03  # ~b_2
   1000006     2.99972510E+03  # ~t_1
   2000006     4.50711296E+03  # ~t_2
   1000011     1.17236602E+03  # ~e_L-
   2000011     8.21160141E+02  # ~e_R-
   1000012     1.16926722E+03  # ~nu_eL
   1000013     1.17233783E+03  # ~mu_L-
   2000013     8.21078882E+02  # ~mu_R-
   1000014     1.16923755E+03  # ~nu_muL
   1000015     1.01494124E+03  # ~tau_1-
   2000015     1.53698959E+03  # ~tau_2-
   1000016     1.53445910E+03  # ~nu_tauL
   1000021     5.12340044E+03  # ~g
   1000022     1.73629526E+02  # ~chi_10
   1000023     3.79089112E+02  # ~chi_20
   1000025     3.84293440E+02  # ~chi_30
   1000035     9.15383852E+02  # ~chi_40
   1000024     3.76681091E+02  # ~chi_1+
   1000037     9.15397538E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.54103248E-02   # alpha
Block Hmix Q=  3.67696884E+03  # Higgs mixing parameters
   1   -3.71283814E+02  # mu
   2    3.83797431E+01  # tan[beta](Q)
   3    2.43033391E+02  # v(Q)
   4    1.94319853E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -4.85827176E-02   # Re[R_st(1,1)]
   1  2     9.98819163E-01   # Re[R_st(1,2)]
   2  1    -9.98819163E-01   # Re[R_st(2,1)]
   2  2    -4.85827176E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.48384153E-03   # Re[R_sb(1,1)]
   1  2     9.99993931E-01   # Re[R_sb(1,2)]
   2  1    -9.99993931E-01   # Re[R_sb(2,1)]
   2  2    -3.48384153E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.76990415E-02   # Re[R_sta(1,1)]
   1  2     9.99843360E-01   # Re[R_sta(1,2)]
   2  1    -9.99843360E-01   # Re[R_sta(2,1)]
   2  2    -1.76990415E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.85936398E-01   # Re[N(1,1)]
   1  2    -8.18192772E-03   # Re[N(1,2)]
   1  3    -1.50538283E-01   # Re[N(1,3)]
   1  4     7.21158766E-02   # Re[N(1,4)]
   2  1     1.57719247E-01   # Re[N(2,1)]
   2  2     1.07720687E-01   # Re[N(2,2)]
   2  3     6.95083477E-01   # Re[N(2,3)]
   2  4    -6.93094404E-01   # Re[N(2,4)]
   3  1     5.48661163E-02   # Re[N(3,1)]
   3  2    -4.28674134E-02   # Re[N(3,2)]
   3  3     7.01461892E-01   # Re[N(3,3)]
   3  4     7.09297757E-01   # Re[N(3,4)]
   4  1     6.61563171E-03   # Re[N(4,1)]
   4  2    -9.93222883E-01   # Re[N(4,2)]
   4  3     4.63508311E-02   # Re[N(4,3)]
   4  4    -1.06377344E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.55229440E-02   # Re[U(1,1)]
   1  2    -9.97851063E-01   # Re[U(1,2)]
   2  1    -9.97851063E-01   # Re[U(2,1)]
   2  2     6.55229440E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.50507362E-01   # Re[V(1,1)]
   1  2     9.88608888E-01   # Re[V(1,2)]
   2  1     9.88608888E-01   # Re[V(2,1)]
   2  2    -1.50507362E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     3.73130136E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.80937936E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.70316765E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.03035264E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     3.85190512E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     3.44562094E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.95883377E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.96172884E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.99939234E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     9.25052177E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     4.06463638E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     3.74189303E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.78096346E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.76902139E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.73741223E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.47602771E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     3.86194157E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     3.43728679E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.07593881E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.48512279E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.99385601E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     9.22615741E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.05336666E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     9.39619539E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.05237760E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.28521055E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.20204009E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.46014581E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.46816706E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.26873549E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.51590404E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.39452427E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.88395702E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.47106816E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.82494174E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     4.50228986E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     4.22038676E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     3.89280687E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     3.61755419E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     5.82891600E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.47993879E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.96839790E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     4.81091722E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.87232787E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     3.90247569E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     3.60849148E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     5.81425735E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.46615574E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.96315564E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.05835055E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.86204201E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     1.47250085E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.27890445E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.22638177E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.09918339E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.89070464E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.99154688E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.72600521E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     8.96206081E-03    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     4.41927143E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.23553532E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     3.15557294E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.87290086E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.71319328E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.62347705E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.91720707E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.14858043E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.56171713E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     6.65701672E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.52285513E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.68101558E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.41988005E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.23552062E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     3.48396493E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.87155710E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.71353573E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.62451963E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.17190932E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.40812220E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.56127804E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     6.71877571E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.52276590E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.68056229E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.22180826E+01   # ~b_1
#    BR                NDA      ID1      ID2
     4.43117514E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.35021613E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.38266512E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.06292063E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.79997766E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.59605372E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.07231853E+02   # ~b_2
#    BR                NDA      ID1      ID2
     7.16594507E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.53062114E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.65534405E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.72961509E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.26891186E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.85063191E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.21159927E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.26752842E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.37195476E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.59054471E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.75898472E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.21510078E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.47033843E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.51045909E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.71300987E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.17700687E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.42983323E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.52588691E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.51256634E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.49476840E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.68059967E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.59061786E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.75891329E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.21887486E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.51006034E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.51030892E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.71335198E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.17691034E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.43280508E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.52544571E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.56490881E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.49468175E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.68014632E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.24723675E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.49074188E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.28381414E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.39224535E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.83076344E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.68946773E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     5.70315331E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.14128151E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.14988211E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.04481140E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.11568631E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     8.90477627E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.69863511E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.70524373E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.42484382E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.88663435E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     9.10558715E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.81780331E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99966490E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     7.79954753E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.61525593E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.47577619E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.46601838E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.37592946E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.36878824E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.59793261E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.59567179E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.82991779E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.78914700E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.21072692E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     4.33730345E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.56077714E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.43909009E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     9.24624521E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.20692945E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.20692945E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.86152159E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.90423143E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.72430207E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.60979550E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.68269116E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.44489800E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     2.62132091E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.62132091E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.38503675E+02   # ~g
#    BR                NDA      ID1      ID2
     2.65888165E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.65888165E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.38375686E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.38375686E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.62131859E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.62131859E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.06677459E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.06677459E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.83644194E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     2.83644194E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.56164035E-03   # Gamma(h0)
     2.51766854E-03   2        22        22   # BR(h0 -> photon photon)
     1.57336284E-03   2        22        23   # BR(h0 -> photon Z)
     2.86361363E-02   2        23        23   # BR(h0 -> Z Z)
     2.38464667E-01   2       -24        24   # BR(h0 -> W W)
     7.83418246E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.01479631E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.23066850E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.42953287E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.42584567E-07   2        -2         2   # BR(h0 -> Up up)
     2.76716310E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.80144661E-07   2        -1         1   # BR(h0 -> Down down)
     2.09824713E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.58065762E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.41573943E+02   # Gamma(HH)
     5.96055625E-09   2        22        22   # BR(HH -> photon photon)
     2.18219639E-08   2        22        23   # BR(HH -> photon Z)
     9.90937153E-08   2        23        23   # BR(HH -> Z Z)
     1.87557868E-08   2       -24        24   # BR(HH -> W W)
     3.42007422E-06   2        21        21   # BR(HH -> gluon gluon)
     7.31136321E-09   2       -11        11   # BR(HH -> Electron electron)
     3.25573499E-04   2       -13        13   # BR(HH -> Muon muon)
     9.23037402E-02   2       -15        15   # BR(HH -> Tau tau)
     5.08451818E-14   2        -2         2   # BR(HH -> Up up)
     9.86444621E-09   2        -4         4   # BR(HH -> Charm charm)
     7.88749165E-04   2        -6         6   # BR(HH -> Top top)
     4.85058918E-07   2        -1         1   # BR(HH -> Down down)
     1.75458123E-04   2        -3         3   # BR(HH -> Strange strange)
     5.14193346E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.32869439E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.12795791E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.12795791E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     9.33068642E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.62998114E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.68755887E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.77331389E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.73684605E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.15770970E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     9.13469303E-05   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     5.06754556E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     6.90731372E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.21747235E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     4.75277395E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     4.26482797E-07   2        25        25   # BR(HH -> h0 h0)
     1.67127468E-08   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.62397709E-14   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.62397709E-14   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     2.35812196E-08   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     1.65262013E-08   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     6.94287353E-10   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     6.94287353E-10   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     2.34390678E-08   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
     5.27774190E-06   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
     3.57338532E-03   2  -2000015   1000015   # BR(HH -> Stau2 stau1)
     3.57338532E-03   2  -1000015   2000015   # BR(HH -> Stau1 stau2)
     4.08381405E-06   2  -2000015   2000015   # BR(HH -> Stau2 stau2)
DECAY        36     1.35196864E+02   # Gamma(A0)
     3.80961930E-08   2        22        22   # BR(A0 -> photon photon)
     5.91697239E-08   2        22        23   # BR(A0 -> photon Z)
     6.94835984E-06   2        21        21   # BR(A0 -> gluon gluon)
     7.08708535E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.15587498E-04   2       -13        13   # BR(A0 -> Muon muon)
     8.94721138E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.63030881E-14   2        -2         2   # BR(A0 -> Up up)
     8.98296033E-09   2        -4         4   # BR(A0 -> Charm charm)
     7.24318336E-04   2        -6         6   # BR(A0 -> Top top)
     4.70152809E-07   2        -1         1   # BR(A0 -> Down down)
     1.70066082E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.98411898E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.13540936E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.17684642E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.17684642E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.44929929E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.80666514E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.98126085E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.64571226E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.89898377E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.38069356E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.20671217E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.28969851E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     6.72448920E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     5.49121892E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     7.36336186E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.44776332E-07   2        23        25   # BR(A0 -> Z h0)
     1.53556417E-13   2        23        35   # BR(A0 -> Z HH)
     1.74088569E-39   2        25        25   # BR(A0 -> h0 h0)
     1.70423810E-14   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     1.70423810E-14   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     7.28615028E-10   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     7.28615028E-10   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
     3.74608287E-03   2  -2000015   1000015   # BR(A0 -> Stau2 stau1)
     3.74608287E-03   2  -1000015   2000015   # BR(A0 -> Stau1 stau2)
DECAY        37     1.50925729E+02   # Gamma(Hp)
     7.49394843E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.20389701E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.06244602E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.67374248E-07   2        -1         2   # BR(Hp -> Down up)
     7.92326930E-06   2        -3         2   # BR(Hp -> Strange up)
     5.72029183E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.23440835E-08   2        -1         4   # BR(Hp -> Down charm)
     1.68456976E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.01045686E-04   2        -5         4   # BR(Hp -> Bottom charm)
     4.66320089E-08   2        -1         6   # BR(Hp -> Down top)
     1.26810240E-06   2        -3         6   # BR(Hp -> Strange top)
     5.40418889E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.33618070E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.39251375E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.99036961E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.07882109E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     9.33464126E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.06336370E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.07038183E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.98008512E-08   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     2.19004677E-07   2        24        25   # BR(Hp -> W h0)
     2.94370908E-14   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     8.69094030E-08   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     1.25858553E-09   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     8.67218451E-08   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     6.71362418E-03   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
     2.42969771E-06   2  -2000015   1000016   # BR(Hp -> Stau2 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.51537308E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.47305314E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.47300468E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00003290E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.45983900E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.78884469E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.51537308E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.47305314E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.47300468E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999591E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.08567511E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999591E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.08567511E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26725834E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.03250931E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.11805653E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.08567511E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999591E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.20125862E-04   # BR(b -> s gamma)
    2    1.58995892E-06   # BR(b -> s mu+ mu-)
    3    3.52455985E-05   # BR(b -> s nu nu)
    4    2.71273084E-15   # BR(Bd -> e+ e-)
    5    1.15884399E-10   # BR(Bd -> mu+ mu-)
    6    2.42363872E-08   # BR(Bd -> tau+ tau-)
    7    9.21915142E-14   # BR(Bs -> e+ e-)
    8    3.93840451E-09   # BR(Bs -> mu+ mu-)
    9    8.34521999E-07   # BR(Bs -> tau+ tau-)
   10    9.63770867E-05   # BR(B_u -> tau nu)
   11    9.95538408E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41970071E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93631428E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15731312E-03   # epsilon_K
   17    2.28165853E-15   # Delta(M_K)
   18    2.47939686E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28910024E-11   # BR(K^+ -> pi^+ nu nu)
   20    7.67103657E-15   # Delta(g-2)_electron/2
   21    3.28123207E-10   # Delta(g-2)_muon/2
   22    6.74261349E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.92010736E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.88163342E-01   # C7
     0305 4322   00   2    -3.80179713E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.79093834E-02   # C8
     0305 6321   00   2    -7.73732878E-05   # C8'
 03051111 4133   00   0     1.62404848E+00   # C9 e+e-
 03051111 4133   00   2     1.62429029E+00   # C9 e+e-
 03051111 4233   00   2     2.83031065E-04   # C9' e+e-
 03051111 4137   00   0    -4.44674058E+00   # C10 e+e-
 03051111 4137   00   2    -4.44443078E+00   # C10 e+e-
 03051111 4237   00   2    -2.10257323E-03   # C10' e+e-
 03051313 4133   00   0     1.62404848E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62428871E+00   # C9 mu+mu-
 03051313 4233   00   2     2.83030613E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44674058E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44443235E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.10257423E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50485718E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.54916945E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50485718E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.54916960E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50485715E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.54921393E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85832282E-07   # C7
     0305 4422   00   2     4.50012871E-06   # C7
     0305 4322   00   2     5.79196333E-07   # C7'
     0305 6421   00   0     3.30490866E-07   # C8
     0305 6421   00   2     4.66048825E-06   # C8
     0305 6321   00   2     2.80054875E-07   # C8'
 03051111 4133   00   2    -9.72367220E-08   # C9 e+e-
 03051111 4233   00   2     5.43140361E-06   # C9' e+e-
 03051111 4137   00   2     2.38047745E-07   # C10 e+e-
 03051111 4237   00   2    -4.03501364E-05   # C10' e+e-
 03051313 4133   00   2    -9.72612054E-08   # C9 mu+mu-
 03051313 4233   00   2     5.43139681E-06   # C9' mu+mu-
 03051313 4137   00   2     2.38073070E-07   # C10 mu+mu-
 03051313 4237   00   2    -4.03501556E-05   # C10' mu+mu-
 03051212 4137   00   2     6.95536004E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     8.73024759E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     6.95539971E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     8.73024759E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     6.86288657E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     8.73025102E-06   # C11' nu_3 nu_3
