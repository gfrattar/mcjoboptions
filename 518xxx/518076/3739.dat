# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  13:55
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    8.09416028E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.74978964E+03  # scale for input parameters
    1    1.71290047E+03  # M_1
    2   -4.37641178E+02  # M_2
    3    3.72785520E+03  # M_3
   11    6.20962242E+03  # A_t
   12   -1.81843254E+03  # A_b
   13   -9.52126471E+02  # A_tau
   23    1.66399766E+02  # mu
   25    7.69968327E+00  # tan(beta)
   26    4.72361432E+02  # m_A, pole mass
   31    1.06781662E+03  # M_L11
   32    1.06781662E+03  # M_L22
   33    7.09754450E+02  # M_L33
   34    9.52391864E+02  # M_E11
   35    9.52391864E+02  # M_E22
   36    4.56425880E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.87562990E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.65896349E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.06853511E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.74978964E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.74978964E+03  # (SUSY scale)
  1  1     8.45478871E-06   # Y_u(Q)^DRbar
  2  2     4.29503267E-03   # Y_c(Q)^DRbar
  3  3     1.02140639E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.74978964E+03  # (SUSY scale)
  1  1     1.30814494E-04   # Y_d(Q)^DRbar
  2  2     2.48547539E-03   # Y_s(Q)^DRbar
  3  3     1.29726987E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.74978964E+03  # (SUSY scale)
  1  1     2.28280859E-05   # Y_e(Q)^DRbar
  2  2     4.72012415E-03   # Y_mu(Q)^DRbar
  3  3     7.93847231E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.74978964E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.20962266E+03   # A_t(Q)^DRbar
Block Ad Q=  2.74978964E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.81843255E+03   # A_b(Q)^DRbar
Block Ae Q=  2.74978964E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -9.52126478E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.74978964E+03  # soft SUSY breaking masses at Q
   1    1.71290047E+03  # M_1
   2   -4.37641178E+02  # M_2
   3    3.72785520E+03  # M_3
  21    1.84284822E+05  # M^2_(H,d)
  22    1.54400367E+05  # M^2_(H,u)
  31    1.06781662E+03  # M_(L,11)
  32    1.06781662E+03  # M_(L,22)
  33    7.09754450E+02  # M_(L,33)
  34    9.52391864E+02  # M_(E,11)
  35    9.52391864E+02  # M_(E,22)
  36    4.56425880E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.87562990E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.65896349E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.06853511E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27768425E+02  # h0
        35     4.73006598E+02  # H0
        36     4.72361432E+02  # A0
        37     4.80453590E+02  # H+
   1000001     1.00883188E+04  # ~d_L
   2000001     1.00634644E+04  # ~d_R
   1000002     1.00880171E+04  # ~u_L
   2000002     1.00664158E+04  # ~u_R
   1000003     1.00883201E+04  # ~s_L
   2000003     1.00634650E+04  # ~s_R
   1000004     1.00880184E+04  # ~c_L
   2000004     1.00664176E+04  # ~c_R
   1000005     2.18609429E+03  # ~b_1
   2000005     2.89845870E+03  # ~b_2
   1000006     2.55062789E+03  # ~t_1
   2000006     2.96450263E+03  # ~t_2
   1000011     1.07453559E+03  # ~e_L-
   2000011     9.68557718E+02  # ~e_R-
   1000012     1.07131790E+03  # ~nu_eL
   1000013     1.07453502E+03  # ~mu_L-
   2000013     9.68556386E+02  # ~mu_R-
   1000014     1.07131732E+03  # ~nu_muL
   1000015     4.84919461E+02  # ~tau_1-
   2000015     7.18791660E+02  # ~tau_2-
   1000016     7.14120435E+02  # ~nu_tauL
   1000021     4.12420912E+03  # ~g
   1000022     1.72057482E+02  # ~chi_10
   1000023     1.85223309E+02  # ~chi_20
   1000025     4.83683083E+02  # ~chi_30
   1000035     1.69567783E+03  # ~chi_40
   1000024     1.77998832E+02  # ~chi_1+
   1000037     4.84025348E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.36314907E-01   # alpha
Block Hmix Q=  2.74978964E+03  # Higgs mixing parameters
   1    1.66399766E+02  # mu
   2    7.69968327E+00  # tan[beta](Q)
   3    2.43298823E+02  # v(Q)
   4    2.23125322E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -4.13669754E-01   # Re[R_st(1,1)]
   1  2     9.10427007E-01   # Re[R_st(1,2)]
   2  1    -9.10427007E-01   # Re[R_st(2,1)]
   2  2    -4.13669754E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.84205297E-03   # Re[R_sb(1,1)]
   1  2     9.99998303E-01   # Re[R_sb(1,2)]
   2  1    -9.99998303E-01   # Re[R_sb(2,1)]
   2  2     1.84205297E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.44843453E-02   # Re[R_sta(1,1)]
   1  2     9.99895096E-01   # Re[R_sta(1,2)]
   2  1    -9.99895096E-01   # Re[R_sta(2,1)]
   2  2     1.44843453E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -1.36586892E-02   # Re[N(1,1)]
   1  2    -1.56363768E-01   # Re[N(1,2)]
   1  3     7.17441215E-01   # Re[N(1,3)]
   1  4     6.78706059E-01   # Re[N(1,4)]
   2  1     2.30869966E-02   # Re[N(2,1)]
   2  2     9.80755812E-02   # Re[N(2,2)]
   2  3     6.95152360E-01   # Re[N(2,3)]
   2  4    -7.11766371E-01   # Re[N(2,4)]
   3  1    -3.45784028E-03   # Re[N(3,1)]
   3  2     9.82817658E-01   # Re[N(3,2)]
   3  3     4.47674409E-02   # Re[N(3,3)]
   3  4     1.79034553E-01   # Re[N(3,4)]
   4  1     9.99634170E-01   # Re[N(4,1)]
   4  2    -1.00193479E-03   # Re[N(4,2)]
   4  3    -6.09710544E-03   # Re[N(4,3)]
   4  4     2.63314887E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.54015792E-02   # Re[U(1,1)]
   1  2     9.97859025E-01   # Re[U(1,2)]
   2  1    -9.97859025E-01   # Re[U(2,1)]
   2  2    -6.54015792E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.57143219E-01   # Re[V(1,1)]
   1  2     9.66373305E-01   # Re[V(1,2)]
   2  1     9.66373305E-01   # Re[V(2,1)]
   2  2    -2.57143219E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     3.29203547E-03   # ~e^-_R
#    BR                NDA      ID1      ID2
     2.58662885E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.31375411E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.95886120E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     9.09088683E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.32295523E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.97073743E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.18318062E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     4.20975190E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.58271896E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.09238542E-03   # ~mu^-_R
#    BR                NDA      ID1      ID2
     2.58724190E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.35376169E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.12990777E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.75171605E-02    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     2.52572791E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     9.09132732E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.32544652E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.99430390E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.18302388E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.20954544E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.58239297E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     9.30662042E-02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.70057037E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.35284433E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.94658502E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     3.01047767E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.81681542E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.40713109E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.02840864E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.50758539E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.25179889E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     1.10236966E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.12982574E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     9.19838390E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.08112307E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.54680833E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.15057513E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     6.41025081E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.06481940E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     9.19882283E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.08107084E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.54663694E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.15042135E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     6.41481284E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.06452392E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     3.14154449E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.97264969E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.39921859E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.84183857E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.41486625E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.46091703E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     2.11209884E-03    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     5.55230096E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.54555324E-03    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.90447037E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.86315797E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.53214934E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.70865272E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.22857854E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.95776931E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     5.53138641E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.28252126E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.04848166E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.55232621E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.54551056E-03    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.90442640E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.86320877E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.53306488E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.71731124E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.22853361E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.95775517E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     5.58168763E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.28251549E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.04842395E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.62933769E+00   # ~b_1
#    BR                NDA      ID1      ID2
     2.29135819E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.14001941E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     6.00791539E-04    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.18773015E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.36233817E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.25461668E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.26682388E+02   # ~b_2
#    BR                NDA      ID1      ID2
     6.27259142E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.56016314E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     9.20668512E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.39648353E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.37523239E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.20615052E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.36587242E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     4.65132127E-04    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     4.63173822E-04    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     5.71440787E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.71112080E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.62860035E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.86305981E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.63048837E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.75130341E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.21289023E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.91414696E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     8.55068930E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.20284355E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.04816287E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.71448320E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.71107301E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.62847599E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.86311028E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.63296060E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.77856920E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.21286204E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.91413636E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     8.55242037E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.20283494E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.04810511E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.02806361E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.52751175E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.68557208E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.75758393E-03    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.47578679E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.60743371E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.34974911E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.37098234E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.89555973E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.14428458E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.09614743E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.32450955E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.31754358E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.96255215E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     7.01846265E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.49048203E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.02861653E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.79923928E-08   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.72289275E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.98553860E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.24130350E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.23935069E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     8.10914462E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     3.09924423E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.70591157E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.67453469E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.50952535E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.09982140E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.66186636E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     3.31217226E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.33008160E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     1.73526338E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.08142902E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     9.55500262E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.38660234E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.38576752E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     3.70709090E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.12843857E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.12547375E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.39978692E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.85468959E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     6.56656543E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     6.56656543E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     5.65117540E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     5.65117540E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     2.18907166E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     2.18907166E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     2.18674017E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     2.18674017E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.63989077E-03    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.63989077E-03    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     4.06319325E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.28519875E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.28519875E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     5.48768173E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.62598698E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.05771359E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.18189515E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.28301374E-04    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     7.28301374E-04    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     2.92395314E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     6.60866598E-02    2     2000011       -11   # BR(chi^0_4 -> ~e^-_R e^+)
     6.60866598E-02    2    -2000011        11   # BR(chi^0_4 -> ~e^+_R e^-)
     1.29859484E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     1.29859484E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     6.60868343E-02    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     6.60868343E-02    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     1.29859676E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     1.29859676E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     1.22724626E-01    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.22724626E-01    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     2.44211119E-02    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     2.44211119E-02    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     1.31889857E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     1.31889857E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     1.31890047E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     1.31890047E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.47239096E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     2.47239096E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     3.84998991E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.84998991E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.42317233E-03    2     1000037       -24   # BR(chi^0_4 -> chi^+_2 W^-)
     1.42317233E-03    2    -1000037        24   # BR(chi^0_4 -> chi^-_2 W^+)
     2.86641168E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     2.86641168E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     1.45813308E-04    2          37  -1000037   # BR(chi^0_4 -> H^+ chi^-_2)
     1.45813308E-04    2         -37   1000037   # BR(chi^0_4 -> H^- chi^+_2)
     2.72627482E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.15326371E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.10015682E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     9.37262136E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.45493036E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.47238850E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.84059116E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     8.13039355E-04    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.43033868E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     1.34199532E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     2.54494028E-04    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.71046884E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     8.03891939E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.20307979E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.20307979E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.70125954E+02   # ~g
#    BR                NDA      ID1      ID2
     1.50251501E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.50251501E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     7.23040289E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     7.23040289E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.85491325E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.85491325E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     9.18929405E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     9.18929405E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     4.32294634E-03   # Gamma(h0)
     2.26823206E-03   2        22        22   # BR(h0 -> photon photon)
     1.68923572E-03   2        22        23   # BR(h0 -> photon Z)
     3.47820202E-02   2        23        23   # BR(h0 -> Z Z)
     2.72572568E-01   2       -24        24   # BR(h0 -> W W)
     6.82855533E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.82440332E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.14599515E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.18473247E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.10807516E-07   2        -2         2   # BR(h0 -> Up up)
     2.15075979E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.56742742E-07   2        -1         1   # BR(h0 -> Down down)
     2.01362086E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.36630834E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     7.50058871E-01   # Gamma(HH)
     1.26511889E-07   2        22        22   # BR(HH -> photon photon)
     1.87845912E-06   2        22        23   # BR(HH -> photon Z)
     3.42935986E-03   2        23        23   # BR(HH -> Z Z)
     7.34257007E-03   2       -24        24   # BR(HH -> W W)
     8.57033196E-04   2        21        21   # BR(HH -> gluon gluon)
     5.81270651E-09   2       -11        11   # BR(HH -> Electron electron)
     2.58663066E-04   2       -13        13   # BR(HH -> Muon muon)
     7.43562072E-02   2       -15        15   # BR(HH -> Tau tau)
     3.02736340E-11   2        -2         2   # BR(HH -> Up up)
     5.86941029E-06   2        -4         4   # BR(HH -> Charm charm)
     1.63319533E-01   2        -6         6   # BR(HH -> Top top)
     5.48354491E-07   2        -1         1   # BR(HH -> Down down)
     1.98334954E-04   2        -3         3   # BR(HH -> Strange strange)
     5.17330254E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.51510935E-01   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     3.50582435E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     7.47548718E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.94989680E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.06329923E-02   2        25        25   # BR(HH -> h0 h0)
DECAY        36     9.03589859E-01   # Gamma(A0)
     2.74382389E-07   2        22        22   # BR(A0 -> photon photon)
     2.34652859E-06   2        22        23   # BR(A0 -> photon Z)
     1.04905165E-03   2        21        21   # BR(A0 -> gluon gluon)
     4.80241421E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.13705409E-04   2       -13        13   # BR(A0 -> Muon muon)
     6.14372041E-02   2       -15        15   # BR(A0 -> Tau tau)
     2.04194231E-11   2        -2         2   # BR(A0 -> Up up)
     3.95702588E-06   2        -4         4   # BR(A0 -> Charm charm)
     2.40502229E-01   2        -6         6   # BR(A0 -> Top top)
     4.53123653E-07   2        -1         1   # BR(A0 -> Down down)
     1.63890841E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.27609462E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.13433979E-01   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     3.23948791E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.37812133E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.50778877E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.73255371E-03   2        23        25   # BR(A0 -> Z h0)
     9.03253795E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     7.15647211E-01   # Gamma(Hp)
     7.27372244E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.10974323E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.79588853E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.92252399E-07   2        -1         2   # BR(Hp -> Down up)
     9.75157646E-06   2        -3         2   # BR(Hp -> Strange up)
     6.11381004E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.27041825E-07   2        -1         4   # BR(Hp -> Down charm)
     2.19997092E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.56150173E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.50168286E-05   2        -1         6   # BR(Hp -> Down top)
     5.45680938E-04   2        -3         6   # BR(Hp -> Strange top)
     8.38818544E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.73012147E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.73669030E-02   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.57973411E-03   2        24        25   # BR(Hp -> W h0)
     4.27857159E-08   2        24        35   # BR(Hp -> W HH)
     6.47586215E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.11328203E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.91718404E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.92851225E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98089200E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.87784385E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.68676383E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.11328203E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.91718404E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.92851225E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99948701E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.12985697E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99948701E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.12985697E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.24554515E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.54643219E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.35995916E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.12985697E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99948701E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.19206413E-04   # BR(b -> s gamma)
    2    1.59299994E-06   # BR(b -> s mu+ mu-)
    3    3.53167628E-05   # BR(b -> s nu nu)
    4    2.46403257E-15   # BR(Bd -> e+ e-)
    5    1.05260684E-10   # BR(Bd -> mu+ mu-)
    6    2.20358830E-08   # BR(Bd -> tau+ tau-)
    7    8.28815957E-14   # BR(Bs -> e+ e-)
    8    3.54069948E-09   # BR(Bs -> mu+ mu-)
    9    7.51031778E-07   # BR(Bs -> tau+ tau-)
   10    9.50814975E-05   # BR(B_u -> tau nu)
   11    9.82155467E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.44583169E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94521816E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16854448E-03   # epsilon_K
   17    2.28177701E-15   # Delta(M_K)
   18    2.48417423E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.30050054E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.30026178E-15   # Delta(g-2)_electron/2
   21   -5.55902187E-11   # Delta(g-2)_muon/2
   22   -1.57260863E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -6.13434956E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.85362713E-01   # C7
     0305 4322   00   2    -1.82732004E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.95296027E-01   # C8
     0305 6321   00   2    -1.92310965E-03   # C8'
 03051111 4133   00   0     1.61311084E+00   # C9 e+e-
 03051111 4133   00   2     1.61395668E+00   # C9 e+e-
 03051111 4233   00   2    -3.99877520E-06   # C9' e+e-
 03051111 4137   00   0    -4.43580294E+00   # C10 e+e-
 03051111 4137   00   2    -4.44036326E+00   # C10 e+e-
 03051111 4237   00   2     3.10187974E-05   # C10' e+e-
 03051313 4133   00   0     1.61311084E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61395658E+00   # C9 mu+mu-
 03051313 4233   00   2    -3.99903084E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.43580294E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44036336E+00   # C10 mu+mu-
 03051313 4237   00   2     3.10190488E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50635709E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -6.72766995E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50635709E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -6.72761470E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50635747E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -6.71190440E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85819328E-07   # C7
     0305 4422   00   2    -8.97479518E-07   # C7
     0305 4322   00   2    -3.80427590E-07   # C7'
     0305 6421   00   0     3.30479771E-07   # C8
     0305 6421   00   2    -2.24489509E-06   # C8
     0305 6321   00   2    -1.95978486E-07   # C8'
 03051111 4133   00   2    -3.39580816E-07   # C9 e+e-
 03051111 4233   00   2     3.12681297E-07   # C9' e+e-
 03051111 4137   00   2     2.08618077E-06   # C10 e+e-
 03051111 4237   00   2    -2.34702262E-06   # C10' e+e-
 03051313 4133   00   2    -3.39581618E-07   # C9 mu+mu-
 03051313 4233   00   2     3.12681276E-07   # C9' mu+mu-
 03051313 4137   00   2     2.08618103E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.34702268E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.33722834E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     5.09073404E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.33722817E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     5.09073404E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.27151954E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     5.09075927E-07   # C11' nu_3 nu_3
