def find_in_slha_file(slha_file, blockname, find):
  """
  slha_file: file name for spectrum file in SLHA format including decay information
  blockname: name of block to work on
  find: 3-tuple - 1: index for compare tag, 2 - tag to compare to, 3 - index of return value
  """

  search = False
  parameter = None

  for l in open(slha_file).readlines():
    if l.upper().startswith('BLOCK %s' % blockname.upper()):
      search = True
    elif l.upper().startswith('BLOCK'):
      search = False

    if not search:
      continue

    if l.split()[find[0]] == find[1]:
      parameter = l.split()[find[2]]

  return parameter

def remove_decay(slha_file, pid):
  """
  rewrite slha file, removing DECAY block for given pid and remove decay from MSSM.model file
  """

  # rewrite slha file
  import shutil
  shutil.move(slha_file, slha_file+'.org')
  
  update = open(slha_file, 'w')

  skip = False
  for l in open(slha_file+'.org'):
    if l.startswith('DECAY   %s '%pid):
      skip = True
      continue

    if l.startswith('DECAY '):
      skip = False
    
    if not skip:
      update.write(l)
    
  update.close()

  # remove also from MSSM.model file

  # rewrite MSSM.model file
#   from PyJobTransformsCore.trfutil import get_files
#   get_files( 'MSSM.model', keepDir=False, errorIfNotFound=True )

#   import shutil
#   shutil.move('MSSM.model', 'MSSM.model.org')
  
#   update = open('MSSM.model', 'w')

#   for l in open('MSSM.model.org'):
#     if l.startswith('insert /Herwig/Shower/ShowerHandler:DecayInShower 0  %s '%pid):
#       continue
#     update.write(l)
    
#   update.close()



def mass_extract(slha_file, pids):

  masses = []

  # extract
  # (find block: Block MASS, pid in first column (index 0), mass in second (index 1))
  for id in pids:
    masses += [find_in_slha_file(slha_file, 'MASS', [0, id, 1])]

  return masses


def get_mg_variations(mass, syst_mod, xqcut=None):
    if xqcut is None:
        xqcut = 500  # default
        if mass < xqcut*4.:
            xqcut = mass*0.25
            pass
        if syst_mod:
            if 'qcup' in syst_mod:
                xqcut = xqcut*2.
            elif 'qcdw' in syst_mod:
                xqcut = xqcut*0.5

    mglog.info('For matching, will use xqcut of '+str(xqcut))

    alpsfact = 1.0
    scalefact = 1.0
    if syst_mod:
        if 'alup' in syst_mod:
            alpsfact = 2.0
        elif 'aldw' in syst_mod:
            alpsfact = 0.5

        if 'scup' in syst_mod:
            scalefact = 2.0
        elif 'scdw' in syst_mod:
            scalefact = 0.5

    return xqcut, alpsfact, scalefact


###
from MadGraphControl.MadGraphUtils import *

# in case someone needs to be able to keep the output directory for testing
keepOutput = False

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

jobConfigParts = JOName.split("_")

prod = str(jobConfigParts[3])
m0 = 5000
m32 = int(float(jobConfigParts[4].replace('p', '.'))*1000)
print("Set m_3/2 to "+str(m32))

tanb = 5
dsid = int((runArgs.jobConfig[0])[-6:])
print("Set DSID to "+str(dsid))

slha_file = 'susy.mAMSB_%s_%s_%s_P.dat' % ( m0, m32, tanb)
print("Got m0="+str(m0)+", m3/2="+str(m32)+"and tan(b)="+str(tanb)+" from slha file name")

if prod == 'CC':
    prod = 'C1C1'
elif prod == 'CN':
    prod = 'C1N1'

print("Set prod to "+prod)

import re
lifetime = float(0)
print("Looking for lifetime in "+jobConfigParts[5])
match = re.search(r'L(\d+p\d+)|(Stable)', jobConfigParts[5])
if match:
    if match.group(1):
        lifetime = float(match.group(1).replace('p', '.'))
        print("Set lifetime to "+str(lifetime))
    elif match.group(2):
        lifetime = float(-1.0)
        print("Set lifetime to "+str(lifetime))
else:
    print("No lifetime found!")

jetptmin = float(0)
metmin = float(0)
if (len(jobConfigParts) > 6):
    print("Looking for jet/met cuts in "+jobConfigParts[6])
    matches = re.findall(r'JET(\d+)|MET(\d+)',jobConfigParts[6])
else:
    match = None

for match in matches:
    jetcut = match[0]
    if jetcut != '':
        jetptmin=float(jetcut)
    metcut = match[1]
    if metcut != '':
        metmin=float(metcut)


print("jetptmin = "+str(jetptmin))
print("metmin = "+str(metmin))

# Event multipliers for getting more events out of madgraph to feed through athena (esp. for filters)
if jetptmin == 0 and metmin == 0:
    evt_multiplier = 2.0
else:
    evt_multiplier = 20.0  # assuming GenFiltEff. ~5%

#nevts = 5000*evt_multiplier

if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevts=runArgs.maxEvents*evt_multiplier
    else:
        nevts=5000*evt_multiplier

# MadGraph5 systematics variations
list_mg_syst = ['scup',
                'scdw',
                'alup',
                'aldw',
                'qcup',
                'qcdw']

# Pythia8 systematics variations
dict_py8_syst = {'py1up': 'Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py',
                 'py1dw': 'Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py',
                 'py2up': 'Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py',
                 'py2dw': 'Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py',
                 'py3aup': 'Pythia8_A14_NNPDF23LO_Var3aUp_EvtGen_Common.py',
                 'py3adw': 'Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py',
                 'py3bup': 'Pythia8_A14_NNPDF23LO_Var3bUp_EvtGen_Common.py',
                 'py3bdw': 'Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py',
                 'py3cup': 'Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py',
                 'py3cdw': 'Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py'}

mg_syst_mod = None
par = [x for x in jobConfigParts if x in list_mg_syst]
if par:
    mg_syst_mod = par[0]

py8_syst_mod = None
par = [x for x in jobConfigParts if x in dict_py8_syst.keys()]
if par:
    py8_syst_mod = par[0]

if prod == 'C1C1':
    process = '''
generate p p > x1+ x1- $ susystrong @1
add process p p > x1+ x1- j $ susystrong @2
add process p p > x1+ x1- j j $ susystrong @3
'''
elif prod == 'C1N1':
    process = '''
generate p p > x1+ n1 $ susystrong @1
add process p p > x1- n1 $ susystrong @1
add process p p > x1+ n1 j $ susystrong @2
add process p p > x1- n1 j $ susystrong @2
add process p p > x1+ n1 j j $ susystrong @3
add process p p > x1- n1 j j $ susystrong @3
'''
else:
    process = '''
generate p p > x1+ x1- $ susystrong @1
add process p p > x1+ n1 $ susystrong @1
add process p p > x1- n1 $ susystrong @1
add process p p > x1+ x1- j $ susystrong @2
add process p p > x1+ n1 j $ susystrong @2
add process p p > x1- n1 j $ susystrong @2
add process p p > x1+ x1- j j $ susystrong @3
add process p p > x1+ n1 j j $ susystrong @3
add process p p > x1- n1 j j $ susystrong @3
'''
njets = 2

pdlabel = 'nn23lo1'
lhaid = 247000

# Set beam energy
beamEnergy = 6500.
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy * 0.5

# Set random seed
rand_seed = 1234
if hasattr(runArgs, "randomSeed"):
    # Giving a unique seed number (not exceeding the limit of ~30081^2)
    rand_seed = 1000 * int(str(dsid)[1:6]) + runArgs.randomSeed

if not 'MADGRAPH_DATA' in os.environ:
    os.environ['MADGRAPH_DATA'] = os.getcwd()
    mglog.warning('Setting your MADGRAPH_DATA environmental variable to the working directory')

if py8_syst_mod:
    include("Pythia8_i/"+dict_py8_syst[py8_syst_mod])
else:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

include("Pythia8_i/Pythia8_MadGraph.py")

# Generate the new process!
full_proc = """
import model MSSM_SLHA2-full
"""+helpful_SUSY_definitions()+"""
# Specify process(es) to run

"""+process+"""
# Output processes to MadEvent directory
output -f
"""
thedir = new_process(process=full_proc)
if 1 == thedir:
    mglog.error('Error in process generation!')
mglog.info('Using process directory '+thedir)

# Grab the param card and move the new masses into place
from PyJobTransformsCore.trfutil import get_files
get_files(slha_file, keepDir=False, errorIfNotFound=True)
# (mN1, mC1) = mass_extract(slha_file, ['1000022', '1000024'])
# mglog.info('chargino1 mass = '+mC1+' neutralino1 mass = '+mN1)

if lifetime != 0: # remove chargino1 decay from slha file so it can decay in G4
    remove_decay(slha_file, '1000024') 

new_masses = {}
for pdgid in (5,6,15,23,24,25,35,36,37,1000001,1000002,1000003,1000004,1000005,1000006,1000011,1000012,1000013,1000014,1000015,1000016,1000021,1000022,1000023,1000024,1000025,1000035,1000037,2000001,2000002,2000003,2000004,2000005,2000006,2000011,2000013,2000015) : # Is there a better way than hard-coding this?
    pdgid_str = str(pdgid)
    mass_list = mass_extract(slha_file,[pdgid_str])
    mass = mass_list[0]
    new_masses.update({pdgid_str: mass})
    mglog.info("Adding mass "+mass+" for particle "+pdgid_str)
    if (pdgid == 1000022) :
        mN1 = float(mass)
    elif (pdgid == 1000024) :
        mC1 = float(mass)

shutil.copy(slha_file, 'param_card.dat')
# params = {'MASS': {'1000022': mN1, '1000024': mC1}}
params = {'MASS': new_masses}
# params = {}
modify_param_card(process_dir=thedir,params=params)

xqcut, alpsfact, scalefact = get_mg_variations(float(mC1), mg_syst_mod)
mglog.info("MG5 params: %s %s %s"%(xqcut, alpsfact, scalefact))
settings = {'ktdurham': xqcut,
          'lhe_version': '3.0',
          'cut_decays': 'F',
          'pdlabel': pdlabel,
          'lhaid': lhaid,
          'drjj': 0.0,
          'ickkw': 0,
          'nevents': nevts,
          'xqcut': 0,
          'scalefact':scalefact,
          'alpsfact':alpsfact}

modify_run_card(process_dir=thedir,runArgs=runArgs,settings=settings)

if generate(runArgs=runArgs, process_dir=thedir):
    mglog.error('Error generating events!')

# Move output files into the appropriate place, with the appropriate name
the_spot = arrange_output(process_dir=thedir,runArgs=runArgs,lhe_version=3,saveProcDir=False)
if the_spot == '':
    mglog.error('Error arranging output dataset!')

mglog.info('Removing process directory...')
shutil.rmtree(thedir, ignore_errors=True)

mglog.info('All done generating events!!')

outputDS = the_spot

if xqcut < 0 or outputDS is None or '' == outputDS:
    evgenLog.warning('Looks like something went wrong with the MadGraph generation - bailing out!')
    raise RuntimeError('Error in MadGraph generation')

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts, 'nprocs'):
        mglog.warning('Did not see option!')
    else:
        opts.nprocs = 0
    mglog.info(opts)

runArgs.qcut = xqcut
runArgs.inputGeneratorFile = outputDS
runArgs.gentype = prod
if mg_syst_mod:
    runArgs.syst_mod = mg_syst_mod
elif py8_syst_mod:
    runArgs.syst_mod = py8_syst_mod

# Pythia8 setup
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            ]
if njets > 0:
    genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on",
                                "Merging:nJetMax = "+str(njets),
                                "Merging:doKTMerging = on",
                                "Merging:TMS = "+str(xqcut),
                                "Merging:ktType = 1",
                                "Merging:Dparameter = 0.4",
                                "1000024:spinType = 1",
                                "1000022:spinType = 1",]
    
#    if prod == 'C1C1':
#        genSeq.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{x1-,-1000024}",]
#    elif prod == 'C1N1':
#        genSeq.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{x1-,-1000024}{n1,1000022}",]
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'

# Configuration for EvgenJobTransforms
# --------------------------------------------------------------
evgenLog.info('Registered generation of mAMSB')

evgenConfig.contact = ['shimpei.yamamoto@cern.ch']
evgenConfig.keywords += ['SUSY', 'chargino', 'longLived']
evgenConfig.description = 'C1N1/C1C1 production, C1 being long lived (mass:%s, lifetime:%s, slha:%s)' % (mC1, lifetime, slha_file)
if lifetime != 0:
    evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1Lifetime=%s*ns;preInclude=SimulationJobOptions/preInclude.AMSB.py' % (mC1, mN1, lifetime)

evgenConfig.generators += ["EvtGen"]

if not hasattr(runArgs, 'inputGeneratorFile'):
    mglog.error('something went wrong with the file name.')
    runArgs.inputGeneratorFile = 'madgraph.*._events.tar.gz'
evgenConfig.inputfilecheck = runArgs.inputGeneratorFile.split('._0')[0]

# Generator Filter
if jetptmin > 0:
    include("GeneratorFilters/JetFilterAkt4.py")
    filtSeq.QCDTruthJetFilter.MinPt = jetptmin*GeV
if metmin > 0:
    include("GeneratorFilters/MissingEtFilter.py")
    filtSeq.MissingEtFilter.METCut = metmin*GeV
    filtSeq.MissingEtFilter.UseChargedNonShowering = True
if jetptmin > 0 and metmin > 0:
    filtSeq.Expression = "(QCDTruthJetFilter) or (MissingEtFilter)"

bonus_file = open('pdg_extras.dat','w')
#  The most important number is the first: the PDGID of the particle
bonus_file.write( '1000024 Chargino 100.0 (MeV/c) boson Chargino 1\n')
bonus_file.write( '-1000024 Anti-chargino 100.0 (MeV/c) boson Chargino -1\n')
bonus_file.close()
 
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

import os
os.system("get_files %s" % testSeq.TestHepMC.G4ExtraWhiteFile)

# Clean up
del m0, m32, tanb, slha_file, mC1, mN1, lifetime, par, jetptmin, xqcut, alpsfact, scalefact
