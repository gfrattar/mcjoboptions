#---------------------------------------------------
# on-the-fly generation of th+ MG5 events
#---------------------------------------------------

from MadGraphControl.MadGraphUtils import *
import math

nevents=1.1*(runArgs.maxEvents)
mode=0

#Charged Higgs mass is parsed via JO file.

model_pars_str = str(jofile)[:-3]

for s in model_pars_str.split("_"):

    if 'mhc' in s:
        ss = s.replace("mhc","")  
        if ss.isdigit():    
            mhc = int(ss)

#Process.

process="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False   
    set loop_optimized_output True
    set complex_mass_scheme False
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt 
    define vl~ = ve~ vm~ vt~
    define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
    import model 2HDMtypeII
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate  p p > w+ b h- b~ / h1 h2 h3, w+ > wdec wdec
    output -f
    """

#Set the mass for h1, h2 and h3.

mh1=1.250e+02                 
mh2=math.sqrt(math.pow(mhc,2)+math.pow(8.0399e+01,2)) 
mh3=mh2

masses = {'25':str(mh1),
          '35':str(mh2),
          '36':str(mh3),
          '37':str(mhc)}

params = {}
params['mass'] = masses

runName='run_01'

scale=1.25e+02

extras = {'lhe_version'     :'3.0',                     
          'cut_decays'      :'F', 
          'pdlabel'         :"'lhapdf'",                
          'lhaid'           :'262400',
          'fixed_ren_scale' : 'T',
          'fixed_fac_scale' : 'T',
          'scale'           : str(scale),
          'dsqrt_q2fact1'   : str(scale),
          'dsqrt_q2fact2'   : str(scale),                    
          'use_syst'        :'True',                    
          'sys_scalefact'   :'1.0 0.5 2.0',             
          'sys_pdf'         :'NNPDF30_lo_as_0118_nf_4', 
          'nevents'         :int(nevents),              
        }


process_dir = new_process(process)
print("process_dir", process_dir)

#Run card.

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#Parameters card.

modify_param_card(process_dir=process_dir,params=params)     

print_cards()

#Generate the process.

generate(process_dir=process_dir, runArgs=runArgs)
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#Pythia 8 for the showering.

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

evgenConfig.description = 'aMcAtNlo single top with charged Higgs at NLO'
evgenConfig.keywords+=['Higgs','MSSM','BSMHiggs','chargedHiggs']
evgenConfig.contact = ['Adrian Berrocal <adrian.berrocal.guardia@cern.ch>']
        
runArgs.inputGeneratorFile=outputDS

#Use Pythia to simulate the decays of H+ into cb.

genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
			    "37:oneChannel = 1 1. 0 4 -5" #Turn off all decays of H+ and switch on H+ to cb.
]  
