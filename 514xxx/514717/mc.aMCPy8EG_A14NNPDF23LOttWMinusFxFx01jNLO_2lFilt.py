evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 5
evgenConfig.description= "ttWMinus FxFx01j@NLO showering"
evgenConfig.contact=["nedaa-alexandra.asbah@cern.ch","rohin.thampilali.narayan@cern.ch"]

#maxjetflavor=5
parton_shower='PYTHIA8'
nJetMax=1
qCut=150.
#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
# include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut
print ("PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax)
print ("PYTHIA8_qCut = %i"%PYTHIA8_qCut)
                         
include("Pythia8_i/Pythia8_FxFx_A14mod.py")

## Dilepton filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("ElecMuTauFilter")
filtSeq.ElecMuTauFilter.NLeptons = 2
filtSeq.ElecMuTauFilter.IncludeHadTaus = True
filtSeq.ElecMuTauFilter.MinVisPtHadTau = 7000.
filtSeq.ElecMuTauFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
filtSeq.ElecMuTauFilter.MaxEta = 2.8




