#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------
import os, sys, glob

for infile in glob.glob("*.events"):
    print "Hacking", infile
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#extracting run parameters from JO file
print(jo_physshortpart+" is being split")
spltPar = jo_physshortpart.split("_")
ma = int((spltPar[3].split("a"))[1])
decayProcess = spltPar[4]
if decayProcess == "4b":
	adecay = 5
else:
	print("Decay process not availabe. Recheck Job specific JO name or rewrite general JO code")
ctau = int((spltPar[5].split("ctau"))[1])


print(ma)
print(adecay)
print(ctau)

m_ma=ma-0.5
p_ma=ma+0.5
width=(1.9732699E-13)/ctau


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',

                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = on',
                            '35:onMode = off',
                            '35:onIfMatch = 36 36', # h->aa

                           # '36:onMode = off', # decay of the a
                           # '36:onIfAny = %d' % adecay, # decay a->XX
                           # '36:m0 = %.1f' % ma, #scalar mass
                           # '36:mMin = 0',
                            #'36:tau0 = %.1f' % ctau, #nominal proper lifetime (in mm/c)
			    '36:oneChannel = 1 1.0 101 {0} -{1}'.format(adecay,adecay),
			    '36:m0=%.1f' % ma,
			    '36:mMin=%.1f' %m_ma,
			    '36:mMax = %.1f' %p_ma,
			    '36:mWidth= %.7g' % width,
			    '36:tau0 %.1f' % ctau,
			    'ParticleDecays:tau0Max = 10000.0', #some very large value
                            'ParticleDecays:limitTau0 = off'
                            ]

genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))]
genSeq.Pythia8.Commands += [
                            'ParticleDecays:tau0Max = 100000.0',
                            'ParticleDecays:limitTau0 = off'
                           ]

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

if adecay==5:
    evgenConfig.description = "POWHEG+MiNLO+Pythia8 VBF H->-vbbbarbbbar production"
    evgenConfig.process = "VBF, H->2a->4b"
elif adecay==15:
    evgenConfig.description = "POWHEG+MiNLO+Pythia8 VBF H->-vtau+tau-tau+tau- production"
    evgenConfig.process = "VBF, H->2a->4tau"
elif adecay==2:
    evgenConfig.description = "POWHEG+MiNLO+Pythia8 VBF H->-vuubaruubar production"
    evgenConfig.process = "VBF, H->2a->4u"

evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.contact     = [ 'amber.rebecca.roepe@cern.ch' ]

