# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
# ME part can be found in https://its.cern.ch/jira/browse/ATLMCPROD-9266

#--------------------------------------------------------------
# Herwig7.2 showering
#--------------------------------------------------------------

include("Herwig7_i/Herwig72_LHEF.py")
# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=False)
Herwig7Config.tune_commands()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.generators += ["Powheg", "Herwig7"]
evgenConfig.contact = ['Meng-Ju Tsai','metsai@cern.ch']
evgenConfig.description = "Generation of pp> ttW > POWHEGBOX +Herwig7"
evgenConfig.keywords = ["Top"]
evgenConfig.tune = "H7.2-Default"

# run Herwig7
Herwig7Config.run()
