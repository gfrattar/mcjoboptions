#---------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 Z->mumu production without lepton filter.'
evgenConfig.contact = ["Christian Gutschow <chris.g@cern.ch>", "jan.kretzschmar@cern.ch"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2muon' ]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.nEventsPerJob = 1000
# Dilepton pT cut of >100 GeV has about 1.2% efficiency -> 100x more LHE events
filterMultiplier = 100
#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults 
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_Z_EW_Common.py")

PowhegConfig.decay_mode = "z > mu+ mu-"
PowhegConfig.no_ew=1
PowhegConfig.ptsqmin=4
PowhegConfig.mass_low=60
PowhegConfig.PHOTOS_enabled = False
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier

#Gmu EW scheme inputs matching what used in the Powheg V1 samples
PowhegConfig.scheme=0
PowhegConfig.alphaem=0.00781653
PowhegConfig.mass_W=79.958059

# tune Powheg settings
PowhegConfig.rwl_group_events = 100000
PowhegConfig.ncall1       = 200000
PowhegConfig.ncall2       = 200000
PowhegConfig.nubound      = 200000
PowhegConfig.itmx1        = 10
PowhegConfig.itmx2        = 20

# Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 1
PowhegConfig.foldy        = 1



PowhegConfig.PDF = 10800 #CT10
PowhegConfig.mu_F = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0]
PowhegConfig.mu_R = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5]
# with PDF weights?
#PowhegConfig.PDF = range(10800, 10852+1) #CT10
#PowhegConfig.PDF.extend(range(13000, 13056+1)) #CT14nnlo
#PowhegConfig.PDF.extend(range(303600,303700+1)) #NNPDF3.1nnlo

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------

PowhegConfig.generate()

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10nlo")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()

# --------------------------------------------------------------
# filter
# --------------------------------------------------------------    
include("GeneratorFilters/DiLeptonMassFilter.py")
DiLeptonMassFilter = filtSeq.DiLeptonMassFilter
DiLeptonMassFilter.MinPt = 5000.
DiLeptonMassFilter.MaxEta = 3.0
DiLeptonMassFilter.MinMass = 10000.
DiLeptonMassFilter.MaxMass = 1E9
DiLeptonMassFilter.MinDilepPt = 100000.
DiLeptonMassFilter.AllowSameCharge = False
