#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = ["VBF"]
evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->bb"
evgenConfig.process     = "VBF H->bb"
evgenConfig.contact     = ["yicong.huang@cern.ch"]
evgenConfig.nEventsPerJob = 5000

#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Increase number of events requested to compensate for potential Pythia losses
PowhegConfig.nEvents *= 2
PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
# For VBF, Nfinal = 3
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs-> bb in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['Higgs:useBSM = off',
                            '25:m0 = 125.',
                            '25:mWidth = 0.00407',
                            '25:onMode = off',
                            '25:doForceWidth = on',
                            '25:onIfMatch = 5 -5',
                           ]
