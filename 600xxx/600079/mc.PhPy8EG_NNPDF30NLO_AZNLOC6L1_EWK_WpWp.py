#evgenConfig.inputfilecheck = 'PhPy8EG_NNPDF30NLO_AZNLOC6L1_EWK_WpWp'
#runArgs.inputGeneratorFile = 'PhPy8EG_NNPDF30NLO_AZNLOC6L1_EWK_WpWp._00001.events.tar.gz'

#--------------------------------------------------------------
# Pythia8 showering with Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = 4']



#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 4' ]
#genSeq.Pythia8.Commands += [ 'NFinal = 4' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBS WpWp + 2 jets"
evgenConfig.keywords    = [ "SM", "VBS", "electroweak", "WW", "2jet", "NLO" ]
evgenConfig.contact     = [ "Cyril Becot <cyril.becot@cern.ch>", "Christian Johnson <christian.johnson@cern.ch>" ]
evgenConfig.process     = "VBS ssWW"
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob = 10000

