#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

runArgs.inputGeneratorFile=runArgs.inputGeneratorFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["Powheg", "Pythia8"]
evgenConfig.description = "Non-resonant diHiggs production with cHHH=10 with Powheg-Box-V2, at NLO + full top mass, which decays to bbZZ 2L."
evgenConfig.keywords = ["hh", "Higgs", "nonResonant", "bbZZ", "bbll", "bottom", "lepton"]
evgenConfig.contact = ['Stefano Manzoni <Stefano.Manzoni@cern.ch>']
evgenConfig.nEventsPerJob = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 8 # 10000 / 0.25 * 1.1 / 5500


#evgenConfig.tune = "MMHT2014"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += ["25:oneChannel = 1 0.5 100 23 23",  
                            "25:addChannel = 1 0.5 100 5 -5",
                            '23:oneChannel = 1 0.2354 100 11 -11',
                            '23:addChannel = 1 0.2354 100 13 -13',
                            '23:addChannel = 1 0.0292 100 15 -15',
                            '23:addChannel = 1 0.5 100 12 -12',                
                            "15:onMode = off",
                            "15:onIfAny = 11 13",
                            "24:mMin = 0", # W minimum mass
                            "24:mMax = 99999", # W maximum mass
                            "23:mMin = 0", # Z minimum mass
                            "23:mMax = 99999", # Z maximum mass
                            "TimeShower:mMaxGamma = 0"  # Z/gamma* combination scale
                            ]

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HZZFilter", PDGParent = [25], PDGChild = [23])
filtSeq += ParentChildFilter("ZvvFilter", PDGParent = [23], PDGChild = [12])
filtSeq += ParentChildFilter("ZllFilter", PDGParent = [23], PDGChild = [11,13,15])
filtSeq.Expression = "HbbFilter and HZZFilter and ZvvFilter and ZllFilter"

