#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z-> Zinc + Htautau production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# H->tautau decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15']


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z production: Z->all, H->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs","2tau" ]
evgenConfig.contact     = [ 'antonio.de.maria@cern.ch' ]
evgenConfig.process = "gg->ZH, H->tautau, Z->all"
evgenConfig.inputFilesPerJob = 5


