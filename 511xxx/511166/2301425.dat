# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:13
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.17602813E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.78572474E+03  # scale for input parameters
    1    1.91322587E+02  # M_1
    2    2.06181117E+02  # M_2
    3    3.66780502E+03  # M_3
   11    8.83413452E+02  # A_t
   12   -1.76955062E+02  # A_b
   13    1.98265101E+03  # A_tau
   23   -3.53362028E+02  # mu
   25    5.14584146E+01  # tan(beta)
   26    4.07838641E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.87123552E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.74106589E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.08088733E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.78572474E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.78572474E+03  # (SUSY scale)
  1  1     8.38595516E-06   # Y_u(Q)^DRbar
  2  2     4.26006522E-03   # Y_c(Q)^DRbar
  3  3     1.01309074E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.78572474E+03  # (SUSY scale)
  1  1     8.67139930E-04   # Y_d(Q)^DRbar
  2  2     1.64756587E-02   # Y_s(Q)^DRbar
  3  3     8.59931088E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.78572474E+03  # (SUSY scale)
  1  1     1.51322259E-04   # Y_e(Q)^DRbar
  2  2     3.12886438E-02   # Y_mu(Q)^DRbar
  3  3     5.26223516E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.78572474E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     8.83413466E+02   # A_t(Q)^DRbar
Block Ad Q=  3.78572474E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.76955062E+02   # A_b(Q)^DRbar
Block Ae Q=  3.78572474E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.98265103E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.78572474E+03  # soft SUSY breaking masses at Q
   1    1.91322587E+02  # M_1
   2    2.06181117E+02  # M_2
   3    3.66780502E+03  # M_3
  21    1.66607381E+07  # M^2_(H,d)
  22    1.68644495E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.87123552E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.74106589E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.08088733E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23316771E+02  # h0
        35     4.07400542E+03  # H0
        36     4.07838641E+03  # A0
        37     4.07615244E+03  # H+
   1000001     1.01032535E+04  # ~d_L
   2000001     1.00782262E+04  # ~d_R
   1000002     1.01028799E+04  # ~u_L
   2000002     1.00810666E+04  # ~u_R
   1000003     1.01032592E+04  # ~s_L
   2000003     1.00782365E+04  # ~s_R
   1000004     1.01028856E+04  # ~c_L
   2000004     1.00810674E+04  # ~c_R
   1000005     2.20048507E+03  # ~b_1
   2000005     2.98626234E+03  # ~b_2
   1000006     2.98848248E+03  # ~t_1
   2000006     4.79564860E+03  # ~t_2
   1000011     1.00214438E+04  # ~e_L-
   2000011     1.00091698E+04  # ~e_R-
   1000012     1.00206663E+04  # ~nu_eL
   1000013     1.00214715E+04  # ~mu_L-
   2000013     1.00092216E+04  # ~mu_R-
   1000014     1.00206933E+04  # ~nu_muL
   1000015     1.00241015E+04  # ~tau_1-
   2000015     1.00299968E+04  # ~tau_2-
   1000016     1.00286228E+04  # ~nu_tauL
   1000021     4.12204910E+03  # ~g
   1000022     1.87906408E+02  # ~chi_10
   1000023     2.16414861E+02  # ~chi_20
   1000025     3.70347818E+02  # ~chi_30
   1000035     3.88183416E+02  # ~chi_40
   1000024     2.14972033E+02  # ~chi_1+
   1000037     3.89182968E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.93411768E-02   # alpha
Block Hmix Q=  3.78572474E+03  # Higgs mixing parameters
   1   -3.53362028E+02  # mu
   2    5.14584146E+01  # tan[beta](Q)
   3    2.43013242E+02  # v(Q)
   4    1.66332357E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99977043E-01   # Re[R_st(1,1)]
   1  2     6.77595601E-03   # Re[R_st(1,2)]
   2  1    -6.77595601E-03   # Re[R_st(2,1)]
   2  2    -9.99977043E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.06163352E-02   # Re[R_sb(1,1)]
   1  2     9.99943645E-01   # Re[R_sb(1,2)]
   2  1    -9.99943645E-01   # Re[R_sb(2,1)]
   2  2    -1.06163352E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.19104279E-01   # Re[R_sta(1,1)]
   1  2     9.47719610E-01   # Re[R_sta(1,2)]
   2  1    -9.47719610E-01   # Re[R_sta(2,1)]
   2  2    -3.19104279E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.48411045E-01   # Re[N(1,1)]
   1  2    -2.04039473E-01   # Re[N(1,2)]
   1  3    -2.16751595E-01   # Re[N(1,3)]
   1  4    -1.09101463E-01   # Re[N(1,4)]
   2  1    -2.73306006E-01   # Re[N(2,1)]
   2  2    -9.13861897E-01   # Re[N(2,2)]
   2  3    -2.60000020E-01   # Re[N(2,3)]
   2  4    -1.50200699E-01   # Re[N(2,4)]
   3  1    -5.62681331E-02   # Re[N(3,1)]
   3  2     9.81587671E-02   # Re[N(3,2)]
   3  3    -6.95676452E-01   # Re[N(3,3)]
   3  4     7.09389194E-01   # Re[N(3,4)]
   4  1    -1.50513169E-01   # Re[N(4,1)]
   4  2     3.37029944E-01   # Re[N(4,2)]
   4  3    -6.33603196E-01   # Re[N(4,3)]
   4  4    -6.79929109E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.02665270E-01   # Re[U(1,1)]
   1  2    -4.30343363E-01   # Re[U(1,2)]
   2  1     4.30343363E-01   # Re[U(2,1)]
   2  2    -9.02665270E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.69103969E-01   # Re[V(1,1)]
   1  2     2.46652586E-01   # Re[V(1,2)]
   2  1     2.46652586E-01   # Re[V(2,1)]
   2  2     9.69103969E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02531778E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.99551536E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.46846331E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.15994909E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.26037912E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44743737E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.78886331E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.42004448E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.40940215E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.00087456E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.96154985E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     1.12533786E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06422861E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.92825215E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.43711452E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.99350861E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.39708863E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     7.12229717E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     3.12701473E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44938802E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.79143963E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.41636061E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.05743964E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.05208449E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.95488612E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.12382646E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.64549939E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.93300278E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.10012079E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.46829952E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     9.44807442E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.99806753E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.55570195E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.95909136E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.18364795E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.17737747E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.50709339E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.66976936E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.51150824E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.81588675E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44748825E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.53798520E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.79489588E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.99571841E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     5.29373934E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.71815100E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.69636794E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44943863E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.53591981E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.79248549E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.98900962E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     5.28663034E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.71296337E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.80078196E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.99966535E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.11417925E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.30029630E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.61911396E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.83501482E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.65367902E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.51215281E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.57238591E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.07604661E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.53551906E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.28176273E-04    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.89909942E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.88737213E+02   # ~d_L
#    BR                NDA      ID1      ID2
     8.97340153E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.82879413E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     7.53679971E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     8.52186795E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.05123787E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     2.38441188E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.04495204E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.57347880E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.07884838E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.59981588E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.67108971E-04    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.89717656E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.88795757E+02   # ~s_L
#    BR                NDA      ID1      ID2
     8.97635278E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.82892003E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     7.91742438E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     8.55276461E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.05115227E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     2.38470711E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.04427642E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.20775886E+01   # ~b_1
#    BR                NDA      ID1      ID2
     4.26197541E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.72163506E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.37830847E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.95431903E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     9.67702745E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.90130870E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.40923217E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.68508810E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.92337713E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.47630081E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.33784696E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.72266987E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.28593513E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.77361714E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     9.62709243E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.74391826E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.52299144E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.92494819E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.23757698E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     8.85279443E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.60836100E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.88720345E+02   # ~u_L
#    BR                NDA      ID1      ID2
     5.97634628E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.00233448E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.19559236E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.21166613E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     7.83297544E-03    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.04461394E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.74399134E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.52295424E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.92505416E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.26935886E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     8.88190047E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.60823959E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.88778856E+02   # ~c_L
#    BR                NDA      ID1      ID2
     5.97585358E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.02848304E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.19751299E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.21171006E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     7.89648323E-03    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.04393828E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.47580670E+02   # ~t_1
#    BR                NDA      ID1      ID2
     4.75134827E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.71072562E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     8.95637157E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.00474171E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.93646593E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.19447913E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.45866903E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.30964357E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     7.78283402E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.30935498E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.66161490E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.32899811E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.10590524E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.93392584E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.66000656E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.92096788E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.07845947E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     3.41288057E-03    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.70020012E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.45405360E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.99125524E-06   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.35290327E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.31780208E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11760162E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11759350E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.09409952E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.47446426E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.22349040E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.51073167E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.19688470E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.96996538E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     9.83359086E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     7.91256209E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.12994547E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.10146075E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.45028243E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.45010386E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.17793974E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.26316452E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.26253516E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.09105281E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.93408659E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     3.07178740E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.54567580E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.54567580E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.49591399E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.75426126E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.50325196E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     8.06979566E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.02324231E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.58056064E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.53615665E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.53615665E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.73294075E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.06830084E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.24145932E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.20576226E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     1.29868322E+02   # ~g
#    BR                NDA      ID1      ID2
     4.80053555E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     4.80053555E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.05077488E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.05077488E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.40179950E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.40179950E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.06048387E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.06048387E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     3.00634249E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.79999200E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.82301528E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.82301528E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.22556146E-03   # Gamma(h0)
     2.65833956E-03   2        22        22   # BR(h0 -> photon photon)
     1.54411607E-03   2        22        23   # BR(h0 -> photon Z)
     2.68624925E-02   2        23        23   # BR(h0 -> Z Z)
     2.29528083E-01   2       -24        24   # BR(h0 -> W W)
     8.39932915E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.04605176E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.24456489E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.47166849E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.43935899E-07   2        -2         2   # BR(h0 -> Up up)
     2.79350568E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.85481751E-07   2        -1         1   # BR(h0 -> Down down)
     2.11754402E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.62324991E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.83570543E+02   # Gamma(HH)
     3.16074889E-08   2        22        22   # BR(HH -> photon photon)
     7.88227144E-08   2        22        23   # BR(HH -> photon Z)
     3.15502360E-08   2        23        23   # BR(HH -> Z Z)
     6.52409492E-09   2       -24        24   # BR(HH -> W W)
     5.37157541E-06   2        21        21   # BR(HH -> gluon gluon)
     7.63630229E-09   2       -11        11   # BR(HH -> Electron electron)
     3.40038866E-04   2       -13        13   # BR(HH -> Muon muon)
     9.82120073E-02   2       -15        15   # BR(HH -> Tau tau)
     1.85636057E-14   2        -2         2   # BR(HH -> Up up)
     3.60135657E-09   2        -4         4   # BR(HH -> Charm charm)
     2.86754766E-04   2        -6         6   # BR(HH -> Top top)
     5.52998974E-07   2        -1         1   # BR(HH -> Down down)
     2.00018494E-04   2        -3         3   # BR(HH -> Strange strange)
     6.08637463E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.96898016E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.03835264E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     7.03835264E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     7.47414130E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.31736533E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     7.73842522E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.19366732E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.13574676E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.13436331E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.76555617E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     5.73137882E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.41203151E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.14618499E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.16750373E-02   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.01280293E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.69157355E+02   # Gamma(A0)
     7.92412222E-10   2        22        22   # BR(A0 -> photon photon)
     2.42342371E-08   2        22        23   # BR(A0 -> photon Z)
     9.01448288E-06   2        21        21   # BR(A0 -> gluon gluon)
     7.36367292E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.27897233E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.47052864E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.73335876E-14   2        -2         2   # BR(A0 -> Up up)
     3.36263073E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.70300324E-04   2        -6         6   # BR(A0 -> Top top)
     5.33187486E-07   2        -1         1   # BR(A0 -> Down down)
     1.92852992E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.86913183E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.13202859E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.73952695E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     7.73952695E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     7.19780528E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.38755166E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     8.14277840E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.72206729E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.14986688E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.19263983E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.02103742E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.77665298E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.71395552E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.25499093E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.20910238E-02   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     7.38966579E-08   2        23        25   # BR(A0 -> Z h0)
     1.04833182E-11   2        23        35   # BR(A0 -> Z HH)
     2.30598852E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.08915304E+02   # Gamma(Hp)
     8.64438466E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.69574443E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.04536702E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.41167788E-07   2        -1         2   # BR(Hp -> Down up)
     9.16596566E-06   2        -3         2   # BR(Hp -> Strange up)
     6.74765058E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.55250290E-08   2        -1         4   # BR(Hp -> Down charm)
     1.95034155E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.44912660E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.82211040E-08   2        -1         6   # BR(Hp -> Down top)
     6.87924757E-07   2        -3         6   # BR(Hp -> Strange top)
     6.37116788E-01   2        -5         6   # BR(Hp -> Bottom top)
     9.15929550E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.32471927E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     4.95060478E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     9.45632075E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     6.68806091E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     8.94623240E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     6.59709245E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.02414153E-03   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     5.97551504E-08   2        24        25   # BR(Hp -> W h0)
     2.97317630E-13   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.90805522E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.64797763E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.64796843E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000347E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.74175730E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.77648006E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.90805522E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.64797763E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.64796843E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999992E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.01833306E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999992E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.01833306E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26611401E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.16742188E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.37536347E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.01833306E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999992E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25885769E-04   # BR(b -> s gamma)
    2    1.58933081E-06   # BR(b -> s mu+ mu-)
    3    3.52452592E-05   # BR(b -> s nu nu)
    4    2.45344480E-15   # BR(Bd -> e+ e-)
    5    1.04808489E-10   # BR(Bd -> mu+ mu-)
    6    2.19470746E-08   # BR(Bd -> tau+ tau-)
    7    8.46839316E-14   # BR(Bs -> e+ e-)
    8    3.61769378E-09   # BR(Bs -> mu+ mu-)
    9    7.67281526E-07   # BR(Bs -> tau+ tau-)
   10    9.59535294E-05   # BR(B_u -> tau nu)
   11    9.91163224E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41847547E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93560263E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15690615E-03   # epsilon_K
   17    2.28165496E-15   # Delta(M_K)
   18    2.47948443E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28928640E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.87002134E-16   # Delta(g-2)_electron/2
   21   -1.22705008E-11   # Delta(g-2)_muon/2
   22   -3.49235640E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.82494119E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.95352880E-01   # C7
     0305 4322   00   2    -1.07913774E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.00439193E-01   # C8
     0305 6321   00   2    -9.63642177E-05   # C8'
 03051111 4133   00   0     1.62568004E+00   # C9 e+e-
 03051111 4133   00   2     1.62596944E+00   # C9 e+e-
 03051111 4233   00   2     6.82360429E-04   # C9' e+e-
 03051111 4137   00   0    -4.44837215E+00   # C10 e+e-
 03051111 4137   00   2    -4.44616684E+00   # C10 e+e-
 03051111 4237   00   2    -5.05580332E-03   # C10' e+e-
 03051313 4133   00   0     1.62568004E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62596929E+00   # C9 mu+mu-
 03051313 4233   00   2     6.82359827E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44837215E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44616700E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.05580403E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50487514E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.09350182E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50487514E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.09350188E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50487515E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.09351841E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821326E-07   # C7
     0305 4422   00   2    -1.55373266E-05   # C7
     0305 4322   00   2    -4.94307673E-07   # C7'
     0305 6421   00   0     3.30481482E-07   # C8
     0305 6421   00   2     1.19315025E-05   # C8
     0305 6321   00   2     1.61158564E-07   # C8'
 03051111 4133   00   2     1.52820296E-06   # C9 e+e-
 03051111 4233   00   2     1.29193201E-05   # C9' e+e-
 03051111 4137   00   2    -1.74750366E-07   # C10 e+e-
 03051111 4237   00   2    -9.57271775E-05   # C10' e+e-
 03051313 4133   00   2     1.52819471E-06   # C9 mu+mu-
 03051313 4233   00   2     1.29193142E-05   # C9' mu+mu-
 03051313 4137   00   2    -1.74741571E-07   # C10 mu+mu-
 03051313 4237   00   2    -9.57271950E-05   # C10' mu+mu-
 03051212 4137   00   2     7.35883358E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.07044939E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     7.35887761E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.07044939E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     7.37126852E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.07044939E-05   # C11' nu_3 nu_3
