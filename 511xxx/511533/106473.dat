# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  18:55
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.02170424E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.25520279E+03  # scale for input parameters
    1    1.15149726E+03  # M_1
    2   -9.87800141E+02  # M_2
    3    4.06966074E+03  # M_3
   11    3.61008366E+03  # A_t
   12    1.10331151E+03  # A_b
   13   -1.06024674E+03  # A_tau
   23    2.37441400E+02  # mu
   25    3.90747387E+01  # tan(beta)
   26    4.29891818E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.71833599E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.71344751E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.48518270E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.25520279E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.25520279E+03  # (SUSY scale)
  1  1     8.38711735E-06   # Y_u(Q)^DRbar
  2  2     4.26065562E-03   # Y_c(Q)^DRbar
  3  3     1.01323114E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.25520279E+03  # (SUSY scale)
  1  1     6.58550448E-04   # Y_d(Q)^DRbar
  2  2     1.25124585E-02   # Y_s(Q)^DRbar
  3  3     6.53075685E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.25520279E+03  # (SUSY scale)
  1  1     1.14921869E-04   # Y_e(Q)^DRbar
  2  2     2.37621977E-02   # Y_mu(Q)^DRbar
  3  3     3.99641074E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.25520279E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.61008366E+03   # A_t(Q)^DRbar
Block Ad Q=  3.25520279E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.10331151E+03   # A_b(Q)^DRbar
Block Ae Q=  3.25520279E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.06024673E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.25520279E+03  # soft SUSY breaking masses at Q
   1    1.15149726E+03  # M_1
   2   -9.87800141E+02  # M_2
   3    4.06966074E+03  # M_3
  21    1.83261473E+07  # M^2_(H,d)
  22    1.58762593E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.71833599E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.71344751E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.48518270E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24653903E+02  # h0
        35     4.29733289E+03  # H0
        36     4.29891818E+03  # A0
        37     4.29810278E+03  # H+
   1000001     1.00899066E+04  # ~d_L
   2000001     1.00657735E+04  # ~d_R
   1000002     1.00895353E+04  # ~u_L
   2000002     1.00687691E+04  # ~u_R
   1000003     1.00899126E+04  # ~s_L
   2000003     1.00657839E+04  # ~s_R
   1000004     1.00895412E+04  # ~c_L
   2000004     1.00687704E+04  # ~c_R
   1000005     2.60245245E+03  # ~b_1
   2000005     2.82014398E+03  # ~b_2
   1000006     2.81566881E+03  # ~t_1
   2000006     3.76334929E+03  # ~t_2
   1000011     1.00208378E+04  # ~e_L-
   2000011     1.00085821E+04  # ~e_R-
   1000012     1.00200705E+04  # ~nu_eL
   1000013     1.00208655E+04  # ~mu_L-
   2000013     1.00086357E+04  # ~mu_R-
   1000014     1.00200980E+04  # ~nu_muL
   1000015     1.00237652E+04  # ~tau_1-
   2000015     1.00288733E+04  # ~tau_2-
   1000016     1.00279346E+04  # ~nu_tauL
   1000021     4.48910311E+03  # ~g
   1000022     2.47191361E+02  # ~chi_10
   1000023     2.51892425E+02  # ~chi_20
   1000025     1.06979685E+03  # ~chi_30
   1000035     1.16448441E+03  # ~chi_40
   1000024     2.49369331E+02  # ~chi_1+
   1000037     1.06912447E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.48897363E-02   # alpha
Block Hmix Q=  3.25520279E+03  # Higgs mixing parameters
   1    2.37441400E+02  # mu
   2    3.90747387E+01  # tan[beta](Q)
   3    2.43164023E+02  # v(Q)
   4    1.84806975E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.96859348E-01   # Re[R_st(1,1)]
   1  2     7.91924206E-02   # Re[R_st(1,2)]
   2  1    -7.91924206E-02   # Re[R_st(2,1)]
   2  2    -9.96859348E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.44091222E-02   # Re[R_sb(1,1)]
   1  2     9.99896183E-01   # Re[R_sb(1,2)]
   2  1    -9.99896183E-01   # Re[R_sb(2,1)]
   2  2     1.44091222E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.81267729E-01   # Re[R_sta(1,1)]
   1  2     9.83433785E-01   # Re[R_sta(1,2)]
   2  1    -9.83433785E-01   # Re[R_sta(2,1)]
   2  2     1.81267729E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     2.13237220E-02   # Re[N(1,1)]
   1  2     6.76716616E-02   # Re[N(1,2)]
   1  3    -7.10016824E-01   # Re[N(1,3)]
   1  4    -7.00601138E-01   # Re[N(1,4)]
   2  1     3.50715711E-02   # Re[N(2,1)]
   2  2     4.49387195E-02   # Re[N(2,2)]
   2  3     7.03927927E-01   # Re[N(2,3)]
   2  4    -7.07980204E-01   # Re[N(2,4)]
   3  1     1.57160031E-03   # Re[N(3,1)]
   3  2    -9.96694007E-01   # Re[N(3,2)]
   3  3    -1.64549430E-02   # Re[N(3,3)]
   3  4    -7.95476025E-02   # Re[N(3,4)]
   4  1     9.99156051E-01   # Re[N(4,1)]
   4  2    -1.45390560E-03   # Re[N(4,2)]
   4  3    -9.52983909E-03   # Re[N(4,3)]
   4  4     3.99281163E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.33574598E-02   # Re[U(1,1)]
   1  2     9.99727177E-01   # Re[U(1,2)]
   2  1    -9.99727177E-01   # Re[U(2,1)]
   2  2    -2.33574598E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.12769044E-01   # Re[V(1,1)]
   1  2     9.93621227E-01   # Re[V(1,2)]
   2  1     9.93621227E-01   # Re[V(2,1)]
   2  2    -1.12769044E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.89393260E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     4.66702662E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.26237478E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.98268388E-01    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.41585671E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.94495894E-03    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.26146602E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01941883E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.59048120E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.39493176E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.08607387E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.91641737E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.61597256E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.38829145E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.93708720E-01    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.28272249E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.41698504E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.34339785E-03    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.65359972E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.01702534E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.58367271E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.39223781E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.08124518E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.14650925E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.44796552E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.35902581E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.04601855E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.15710233E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.72046463E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.10839847E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.71515391E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.19937404E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.44013222E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.42564976E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.84227168E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.79365220E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.88823593E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.41590634E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.85417307E-04    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.13591747E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.02926096E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     8.68358394E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     7.91247016E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01126585E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.41703455E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.84635449E-04    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.13422278E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.02685783E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     8.67669538E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     8.69907868E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00650127E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.73529803E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.04678103E-04    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.74416071E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.47373719E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     7.09118505E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.89749569E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.90985767E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.15371795E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.05962402E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.89385049E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.43971021E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.81100940E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.03190079E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.70149810E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     2.16028612E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.34775127E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.95590157E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.15435303E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.05949516E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.89265181E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.44006648E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.05588618E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.27268215E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.70113285E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     2.16017230E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.34767826E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.95547083E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.40908646E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.48366129E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.43387067E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.10370358E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.87169926E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.07366063E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.10915649E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.08939716E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.43088292E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.59211609E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.17252935E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.75880150E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     5.32092343E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.10659300E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.58862863E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.43953518E+02   # ~u_L
#    BR                NDA      ID1      ID2
     3.51829230E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.80446051E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.69389990E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     2.09079047E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.75199275E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.33132342E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.95553600E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.32099708E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.10653775E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.58849779E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.43989099E+02   # ~c_L
#    BR                NDA      ID1      ID2
     3.54584335E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.83269029E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.69353710E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     2.09068466E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.80047521E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.33125093E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.95510523E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.10406068E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.16003092E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     2.52524089E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.10722623E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.56559908E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     7.89428942E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.49567381E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.23510650E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.56998703E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.75716223E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.01835005E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.83258627E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.87686040E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     4.30309803E-03    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.39187623E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.66955051E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.66424441E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     9.38988912E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     4.70908294E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.41914652E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.26802472E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     5.43619215E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     9.35269398E-02    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.81212356E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.79106933E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     2.53455576E-03    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     9.24362323E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.49280344E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.52010867E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.47914838E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.37289946E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.14912505E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.81014054E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.53598584E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.07055095E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     7.16011854E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     4.42133435E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.55023203E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     5.66896730E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     5.64211937E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.27933881E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.26979703E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     8.41955767E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     7.58112643E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     2.29113621E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     2.29113621E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     6.48160333E-04    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     6.48160333E-04    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     7.63711969E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     7.63711969E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     7.57078859E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     7.57078859E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
DECAY   1000025     1.04062400E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.26104866E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.26104866E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     6.65944705E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.57264372E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.44890796E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.65544930E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.62295832E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.73949565E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.03997850E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.03997850E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     3.41015926E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.29486254E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.29486254E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.66943839E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.39627635E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.39594011E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.57212861E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.99410467E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.11172300E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.59493000E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.59493000E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.86686374E+02   # ~g
#    BR                NDA      ID1      ID2
     5.01021245E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     5.01021245E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.33143532E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.33143532E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.94218534E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.94218534E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.56855834E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.56855834E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.30420160E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.30420160E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.39340497E-03   # Gamma(h0)
     2.64541879E-03   2        22        22   # BR(h0 -> photon photon)
     1.65775863E-03   2        22        23   # BR(h0 -> photon Z)
     3.02758820E-02   2        23        23   # BR(h0 -> Z Z)
     2.51828242E-01   2       -24        24   # BR(h0 -> W W)
     8.23652757E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.86981747E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.16618013E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.24583753E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.38670237E-07   2        -2         2   # BR(h0 -> Up up)
     2.69137758E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.63921464E-07   2        -1         1   # BR(h0 -> Down down)
     2.03956634E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.41433989E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.14093683E+02   # Gamma(HH)
     1.94110185E-08   2        22        22   # BR(HH -> photon photon)
     4.09759597E-08   2        22        23   # BR(HH -> photon Z)
     8.81736459E-08   2        23        23   # BR(HH -> Z Z)
     1.71605894E-08   2       -24        24   # BR(HH -> W W)
     3.18013315E-06   2        21        21   # BR(HH -> gluon gluon)
     7.81819088E-09   2       -11        11   # BR(HH -> Electron electron)
     3.48142683E-04   2       -13        13   # BR(HH -> Muon muon)
     1.00553505E-01   2       -15        15   # BR(HH -> Tau tau)
     4.71094484E-14   2        -2         2   # BR(HH -> Up up)
     9.13936185E-09   2        -4         4   # BR(HH -> Charm charm)
     6.29361652E-04   2        -6         6   # BR(HH -> Top top)
     5.44651708E-07   2        -1         1   # BR(HH -> Down down)
     1.96988647E-04   2        -3         3   # BR(HH -> Strange strange)
     4.49074601E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.42994574E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.34857571E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.34857571E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.77136175E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     6.03687201E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     6.64658711E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     7.00228873E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.06788200E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.21274196E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     6.59521437E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.72852989E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     8.86521984E-05   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.78126924E-05   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     3.43976357E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     7.87055082E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.09902366E+02   # Gamma(A0)
     3.00576285E-10   2        22        22   # BR(A0 -> photon photon)
     1.01403837E-08   2        22        23   # BR(A0 -> photon Z)
     7.12352808E-06   2        21        21   # BR(A0 -> gluon gluon)
     7.57264706E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.37209668E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.73958208E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.36735446E-14   2        -2         2   # BR(A0 -> Up up)
     8.47263034E-09   2        -4         4   # BR(A0 -> Charm charm)
     5.86301396E-04   2        -6         6   # BR(A0 -> Top top)
     5.27517840E-07   2        -1         1   # BR(A0 -> Down down)
     1.90792971E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.34941721E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.55881488E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.40105286E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.40105286E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.46704315E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     5.75222163E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     9.05895259E-05   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     6.95943129E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.82657629E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.40969415E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     7.16650621E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.11748016E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     7.36362197E-05   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.60647806E-05   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     7.75476813E-06   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.09541264E-07   2        23        25   # BR(A0 -> Z h0)
     1.00100124E-13   2        23        35   # BR(A0 -> Z HH)
     4.97318497E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.29065564E+02   # Gamma(Hp)
     8.90732580E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.80815998E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.07716458E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.38243333E-07   2        -1         2   # BR(Hp -> Down up)
     9.12106290E-06   2        -3         2   # BR(Hp -> Strange up)
     5.22752824E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.57025622E-08   2        -1         4   # BR(Hp -> Down charm)
     1.93974301E-04   2        -3         4   # BR(Hp -> Strange charm)
     7.32041134E-04   2        -5         4   # BR(Hp -> Bottom charm)
     5.11793632E-08   2        -1         6   # BR(Hp -> Down top)
     1.40520476E-06   2        -3         6   # BR(Hp -> Strange top)
     4.94049546E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.52643593E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.22693060E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.12988874E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.19341618E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.20040739E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.16077887E-10   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     3.31796770E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     7.29829436E-05   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.78381384E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.46296906E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.52688891E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.52683520E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00003517E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.19776714E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.54949530E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.46296906E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.52688891E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.52683520E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999515E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.85335774E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999515E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.85335774E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26601211E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.15921445E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.13427682E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.85335774E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999515E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.28813947E-04   # BR(b -> s gamma)
    2    1.58901930E-06   # BR(b -> s mu+ mu-)
    3    3.52458132E-05   # BR(b -> s nu nu)
    4    2.50821301E-15   # BR(Bd -> e+ e-)
    5    1.07148188E-10   # BR(Bd -> mu+ mu-)
    6    2.24406398E-08   # BR(Bd -> tau+ tau-)
    7    8.39738773E-14   # BR(Bs -> e+ e-)
    8    3.58736789E-09   # BR(Bs -> mu+ mu-)
    9    7.61296494E-07   # BR(Bs -> tau+ tau-)
   10    9.62977094E-05   # BR(B_u -> tau nu)
   11    9.94718471E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42012895E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93640624E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15752121E-03   # epsilon_K
   17    2.28166069E-15   # Delta(M_K)
   18    2.47937678E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28906070E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.38405596E-16   # Delta(g-2)_electron/2
   21   -1.87434128E-11   # Delta(g-2)_muon/2
   22   -5.31763787E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.16973773E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.98617214E-01   # C7
     0305 4322   00   2    -1.12018645E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.02896643E-01   # C8
     0305 6321   00   2    -1.15856770E-04   # C8'
 03051111 4133   00   0     1.61978834E+00   # C9 e+e-
 03051111 4133   00   2     1.62014784E+00   # C9 e+e-
 03051111 4233   00   2     3.85548340E-04   # C9' e+e-
 03051111 4137   00   0    -4.44248044E+00   # C10 e+e-
 03051111 4137   00   2    -4.44024980E+00   # C10 e+e-
 03051111 4237   00   2    -2.88091153E-03   # C10' e+e-
 03051313 4133   00   0     1.61978834E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62014773E+00   # C9 mu+mu-
 03051313 4233   00   2     3.85548143E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44248044E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44024991E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.88091180E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50486846E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     6.23928610E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50486846E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     6.23928628E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50486846E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     6.23933564E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85809265E-07   # C7
     0305 4422   00   2    -8.87285226E-06   # C7
     0305 4322   00   2    -7.00312067E-07   # C7'
     0305 6421   00   0     3.30471151E-07   # C8
     0305 6421   00   2    -8.67910168E-06   # C8
     0305 6321   00   2    -3.53671236E-07   # C8'
 03051111 4133   00   2     4.96213687E-07   # C9 e+e-
 03051111 4233   00   2     7.28482794E-06   # C9' e+e-
 03051111 4137   00   2    -3.91497472E-07   # C10 e+e-
 03051111 4237   00   2    -5.44365735E-05   # C10' e+e-
 03051313 4133   00   2     4.96210370E-07   # C9 mu+mu-
 03051313 4233   00   2     7.28482588E-06   # C9' mu+mu-
 03051313 4137   00   2    -3.91494154E-07   # C10 mu+mu-
 03051313 4237   00   2    -5.44365797E-05   # C10' mu+mu-
 03051212 4137   00   2     1.10332306E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.17895107E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     1.10332462E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.17895107E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     1.10376039E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.17895107E-05   # C11' nu_3 nu_3
