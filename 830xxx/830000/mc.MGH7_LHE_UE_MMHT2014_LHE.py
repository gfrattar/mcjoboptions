## Initialise Herwig7 for showering
include("Herwig7_i/Herwig7_LHEF.py")

## Provide config information
evgenConfig.generators += ["aMcAtNlo"]
evgenConfig.tune        = "MMHT2014"
evgenConfig.description = "MG5aMCatNLO/Herwig7 LHEF"
evgenConfig.keywords    = ['SM', 'diboson', 'WZ', 'electroweak', '3lepton', 'VBS']
evgenConfig.contact     = ["Carsten Bittrich (carsten.bittrich@cern.ch)"]

## Configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile,
                                   me_pdf_order="NLO")
Herwig7Config.add_commands("""

cd /Herwig/Shower
do ShowerHandler:AddVariation showerscale_MUR0.5_MUF0.5 0.5 0.5 All
do ShowerHandler:AddVariation showerscale_MUR0.5_MUF1 0.5 1 All
do ShowerHandler:AddVariation showerscale_MUR0.5_MUF2 0.5 2 All

do ShowerHandler:AddVariation showerscale_MUR1_MUF0.5 1 0.5 All
do ShowerHandler:AddVariation showerscale_MUR1_MUF2 1 2 All

do ShowerHandler:AddVariation showerscale_MUR2_MUF0.5 2 0.5 All
do ShowerHandler:AddVariation showerscale_MUR2_MUF1 2 1 All
do ShowerHandler:AddVariation showerscale_MUR2_MUF2 2 2 All
set SplittingGenerator:Detuning 2.0

set ShowerHandler:SpinCorrelations Yes
set /Herwig/EventHandlers/LHEReader:IncludeSpin No
""")

## run generator
Herwig7Config.run()

evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 1


