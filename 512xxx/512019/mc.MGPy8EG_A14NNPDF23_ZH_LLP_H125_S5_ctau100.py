from MadGraphControl.MadGraphUtils import *

#############################################
### Production of scalar long-lived particles
### though a higgs-like scalar mediator
############################################

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short_tokens = get_physics_short().split ("_")
mH = int (phys_short_tokens [-3] [1:])
mhS = int (phys_short_tokens [-2] [1:])
avgtau = int (phys_short_tokens [-1] [4:])

print ("=============================")
print ("Process: ZH -> ll aa -> ll 4f")
print (" - mH =       \t %d GeV" % mH)
print (" - mS =       \t %d GeV" % mhS)
print (" - LLP ctau = \t %d mm" % avgtau)
print ("=============================")

include ("MadGraphControl_ZH_LLP.py")
