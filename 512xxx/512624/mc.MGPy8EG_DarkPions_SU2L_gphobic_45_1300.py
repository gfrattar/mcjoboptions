dmmodel = 'Gaugephobic_SU2L'
dpionmass = 1300.0
fermioneta = 0.45
ndark = 4

include("MadGraphControl_DarkPions_Pythia8EvtGen_A14_NNPDF23LO.py")
