from MadGraphControl.MadGraphUtils import *
import os
import fileinput
import re

# General settings
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*1.1)
else: nevents = int(evgenConfig.nEventsPerJob*1.1)

gridpack_dir='madevent/'
gridpack_mode = True
run_from_gridpack = True

#---------------------------------------------------------------------------
# Process type:
#---------------------------------------------------------------------------

process1="""
set max_t_for_channel 2
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > z a j j QCD=0 [QCD]
output -f"""

evgenConfig.keywords = ['Z','photon','bottom','VBFHiggs']
evgenConfig.contact = ['Carolyn Gee <Carolyn.Mei.Gee@cern.ch>']
description='MadGraph_aMC@NLO z->jj 0,2jets@NLO'
ptgmin=19

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""+process1+"""
output -f
""")
fcard.close()


#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = { 'nevents' : nevents,
           'lhe_version' : '3.0',
           'parton_shower' :'HERWIGPP',
           'pdlabel'       :"'lhapdf'",
           'lhaid'         :"90000",
           'ptj'           :"15.0",
           'etaj'          :"-1.0",
           'ptgmin'        :ptgmin,
           'epsgamma'      :'1.0', #'0.1',
           'R0gamma'       :'0.4',#'0.1',
           'xn'            :'1', #'2',
           'isoEM'         :'True',
           'etagamma'      :'3.0',
           'bwcutoff'      :'15',
           'maxjetflavor'  :'5',
           'reweight_scale':'True',
           'rw_Rscale_down':0.5,  
           'rw_Rscale_up'  :2.0,
           'rw_Fscale_down':0.5,
           'rw_Fscale_up'  :2.0,
           'reweight_PDF'  :'True',
           'PDF_set_min'   :90001,
           'PDF_set_max'   :90100,
           'dynamical_scale_choice' : '-1', #default value
           'store_rwgt_info':'True'
}

                              
process_dir = new_process(process1)


if run_from_gridpack == False:
    with open("PROCNLO_loop_sm-no_b_mass_0/Cards/FKS_params.dat","r+") as file:
        for line in file:
            print(line)

    print("editing FKS file")

    for line in fileinput.input("PROCNLO_loop_sm-no_b_mass_0/Cards/FKS_params.dat", inplace=2):
        line = re.sub('1.0d-5', '-1d0', line.rstrip())
        print(line)

    print(line)

else:
    print("FKS file already corrected")

modify_run_card(process_dir=process_dir,
               settings=extras)

print_cards()

#---------------------------------------------------------------------------
# MadSpin Card
#---------------------------------------------------------------------------

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))


madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
print("Madspin_card_loc", madspin_card_loc)
mscard = open(madspin_card_loc,'w')        
                                                                                                                            
mscard.write("""
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight

 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay z > b b~
# running the actual code
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

print("Wrote madspin card")

#---------------------------------------------------------------------------
# MG5 + Herwig7 setup and process (lhe) generation
#---------------------------------------------------------------------------
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)

if 'ATHENA_PROC_NUMBER' in os.environ:
    print('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print('Did not see option!')
    else: opts.nprocs = 0
    print(opts)


#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
arrange_output(process_dir=process_dir,
               runArgs=runArgs,
               lhe_version=3
               )
 
evgenConfig.generators  += ["aMcAtNlo","Herwig7"]
evgenConfig.description = 'MadGraph_Z_NLO_Herwigpp'
evgenConfig.keywords+=['jets']
evgenConfig.tune =  "PDF4LHC15_nlo_mc_pdfas" 

# configure Herwig7
include("Herwig7_i/Herwig72_LHEF.py")

Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_mc_pdfas")
Herwig7Config.tune_commands()

if hasattr(runArgs,'outputTXTFile'):
    lhe_filename=runArgs.outputTXTFile.split('.tar.gz')[0]+'.events'
else:
    lhe_filename="tmp_LHE_events.events"

Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="NLO") 

# Default 7 point mu_R, mu_F shower scale variations
include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")# Default 7 point mu_R, mu_F shower scale variations

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

testSeq.TestHepMC.EffFailThreshold = 0.96
