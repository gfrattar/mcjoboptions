# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:12
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.48583010E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.74511329E+03  # scale for input parameters
    1   -4.47891727E+01  # M_1
    2    1.72722933E+03  # M_2
    3    2.62997443E+03  # M_3
   11   -4.84228468E+03  # A_t
   12   -1.49046000E+03  # A_b
   13   -1.81067205E+03  # A_tau
   23    3.46445017E+02  # mu
   25    4.40783027E+01  # tan(beta)
   26    3.17251213E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.41742482E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.07399770E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.34579714E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.74511329E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.74511329E+03  # (SUSY scale)
  1  1     8.38652956E-06   # Y_u(Q)^DRbar
  2  2     4.26035701E-03   # Y_c(Q)^DRbar
  3  3     1.01316013E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.74511329E+03  # (SUSY scale)
  1  1     7.42826506E-04   # Y_d(Q)^DRbar
  2  2     1.41137036E-02   # Y_s(Q)^DRbar
  3  3     7.36651126E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.74511329E+03  # (SUSY scale)
  1  1     1.29628657E-04   # Y_e(Q)^DRbar
  2  2     2.68030951E-02   # Y_mu(Q)^DRbar
  3  3     4.50783966E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.74511329E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -4.84228459E+03   # A_t(Q)^DRbar
Block Ad Q=  2.74511329E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.49046004E+03   # A_b(Q)^DRbar
Block Ae Q=  2.74511329E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.81067206E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.74511329E+03  # soft SUSY breaking masses at Q
   1   -4.47891727E+01  # M_1
   2    1.72722933E+03  # M_2
   3    2.62997443E+03  # M_3
  21    9.64222931E+06  # M^2_(H,d)
  22    4.83293735E+03  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.41742482E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.07399770E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.34579714E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26140801E+02  # h0
        35     3.17185431E+03  # H0
        36     3.17251213E+03  # A0
        37     3.17275475E+03  # H+
   1000001     1.01033486E+04  # ~d_L
   2000001     1.00805472E+04  # ~d_R
   1000002     1.01029862E+04  # ~u_L
   2000002     1.00834543E+04  # ~u_R
   1000003     1.01033595E+04  # ~s_L
   2000003     1.00805666E+04  # ~s_R
   1000004     1.01029970E+04  # ~c_L
   2000004     1.00834562E+04  # ~c_R
   1000005     2.48681229E+03  # ~b_1
   2000005     4.38453925E+03  # ~b_2
   1000006     2.46007544E+03  # ~t_1
   2000006     3.06317720E+03  # ~t_2
   1000011     1.00193558E+04  # ~e_L-
   2000011     1.00090803E+04  # ~e_R-
   1000012     1.00185979E+04  # ~nu_eL
   1000013     1.00194068E+04  # ~mu_L-
   2000013     1.00091778E+04  # ~mu_R-
   1000014     1.00186482E+04  # ~nu_muL
   1000015     1.00333211E+04  # ~tau_1-
   2000015     1.00380175E+04  # ~tau_2-
   1000016     1.00331436E+04  # ~nu_tauL
   1000021     3.03843546E+03  # ~g
   1000022     4.47697846E+01  # ~chi_10
   1000023     3.41639644E+02  # ~chi_20
   1000025     3.45986367E+02  # ~chi_30
   1000035     1.83301619E+03  # ~chi_40
   1000024     3.40542579E+02  # ~chi_1+
   1000037     1.83298377E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.23437477E-02   # alpha
Block Hmix Q=  2.74511329E+03  # Higgs mixing parameters
   1    3.46445017E+02  # mu
   2    4.40783027E+01  # tan[beta](Q)
   3    2.43324596E+02  # v(Q)
   4    1.00648332E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.79053691E-01   # Re[R_st(1,1)]
   1  2     2.03602233E-01   # Re[R_st(1,2)]
   2  1    -2.03602233E-01   # Re[R_st(2,1)]
   2  2     9.79053691E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99995655E-01   # Re[R_sb(1,1)]
   1  2     2.94775484E-03   # Re[R_sb(1,2)]
   2  1    -2.94775484E-03   # Re[R_sb(2,1)]
   2  2     9.99995655E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.38012237E-01   # Re[R_sta(1,1)]
   1  2     3.46602141E-01   # Re[R_sta(1,2)]
   2  1    -3.46602141E-01   # Re[R_sta(2,1)]
   2  2     9.38012237E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.91395357E-01   # Re[N(1,1)]
   1  2     5.32703855E-04   # Re[N(1,2)]
   1  3     1.30149095E-01   # Re[N(1,3)]
   1  4     1.40062604E-02   # Re[N(1,4)]
   2  1    -8.24325810E-02   # Re[N(2,1)]
   2  2    -3.93719850E-02   # Re[N(2,2)]
   2  3     7.03888310E-01   # Re[N(2,3)]
   2  4    -7.04411786E-01   # Re[N(2,4)]
   3  1    -1.01679669E-01   # Re[N(3,1)]
   3  2     2.56538571E-02   # Re[N(3,2)]
   3  3     6.98217399E-01   # Re[N(3,3)]
   3  4     7.08163533E-01   # Re[N(3,4)]
   4  1     1.16646759E-03   # Re[N(4,1)]
   4  2    -9.98895111E-01   # Re[N(4,2)]
   4  3    -9.74294449E-03   # Re[N(4,3)]
   4  4     4.59594576E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.37973126E-02   # Re[U(1,1)]
   1  2     9.99904813E-01   # Re[U(1,2)]
   2  1     9.99904813E-01   # Re[U(2,1)]
   2  2     1.37973126E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.50486382E-02   # Re[V(1,1)]
   1  2     9.97882095E-01   # Re[V(1,2)]
   2  1     9.97882095E-01   # Re[V(2,1)]
   2  2     6.50486382E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02879590E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.82903865E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.77988010E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.03149179E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.36189244E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.10104194E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.24760324E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.65462427E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01459351E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.23002848E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04894161E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05738843E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.77404333E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     8.13977991E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.16323473E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.82152184E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.36332888E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.09327821E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.76443739E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     7.76013360E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01143564E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.22873877E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04260329E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.71456266E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.13470453E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.18224153E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.06663100E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.09580883E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.15594132E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     4.20501998E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     1.36823945E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.20844689E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.49735521E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.58833782E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.79815553E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.56405365E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     7.61990868E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.36193185E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.06355159E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.06589170E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02177770E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.73376325E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02380029E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.36336807E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.05404915E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.06372582E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01861169E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.77850339E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01749087E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.76882369E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.98874786E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.59298419E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.33050791E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.30843995E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.64619330E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     6.63307106E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.33888949E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91515734E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.85949912E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.75276978E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.08166064E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.28229620E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.05815852E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.39444266E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.63388381E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.33890103E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.16791075E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91396929E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.85994536E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.75353258E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.32866963E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.28200325E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.05809993E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.39397872E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.12660867E+01   # ~b_1
#    BR                NDA      ID1      ID2
     9.84716663E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.57629937E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.55064382E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.80493644E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.92927323E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.64818263E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.89674812E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.68429695E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.22238467E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.20334755E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.45724154E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.92926426E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     8.07846245E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     4.08452524E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.33529830E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.80461208E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.25240468E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.24351045E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.41328835E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.66910231E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.85936501E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.77401780E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.64922500E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.27777924E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.77872877E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.05385676E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.39416503E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.80468618E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.25236986E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.26998070E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.44001696E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.66899912E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.85981083E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.77391991E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.67210779E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.27748722E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.28518675E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.05379836E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.39370105E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.18087699E+01   # ~t_1
#    BR                NDA      ID1      ID2
     1.51415609E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     5.94287031E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.35781892E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.87373476E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.96554926E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.41364801E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.34667208E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.79740195E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.70108183E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.97631706E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.29669635E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.53497768E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.51427703E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.54405128E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.96263079E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.06665958E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.57752679E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     8.49289611E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.15552136E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.18459867E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99285963E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     7.14021288E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.69960656E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     8.02653777E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.44507324E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.44014842E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.43983897E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.37297104E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.17347951E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.98967711E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     8.82866190E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     8.48668748E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.42558562E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.67561794E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.32431087E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.89218067E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.81022401E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.18947676E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.83406041E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.27387681E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.27387681E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.16925930E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.70924394E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.59121699E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.88291832E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.53506131E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.40809515E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.51486344E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.36160571E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     4.31516381E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.35334690E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     7.42396214E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.42396214E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.30893238E+01   # ~g
#    BR                NDA      ID1      ID2
     8.43691509E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     8.43691509E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.93378449E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.93378449E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.12254801E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.12254801E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.30918548E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.48210298E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     9.75791707E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     4.73367082E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.88548646E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     4.55374091E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.85311616E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     4.47416712E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     4.47416712E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.57472999E-03   # Gamma(h0)
     2.64503531E-03   2        22        22   # BR(h0 -> photon photon)
     1.79737483E-03   2        22        23   # BR(h0 -> photon Z)
     3.45661200E-02   2        23        23   # BR(h0 -> Z Z)
     2.79245727E-01   2       -24        24   # BR(h0 -> W W)
     8.07926152E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.61532194E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.05298345E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.91962315E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.30823737E-07   2        -2         2   # BR(h0 -> Up up)
     2.53920937E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.33304754E-07   2        -1         1   # BR(h0 -> Down down)
     1.92883868E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.12775524E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.19042740E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     9.18386363E+01   # Gamma(HH)
     6.80727339E-09   2        22        22   # BR(HH -> photon photon)
     2.07070807E-08   2        22        23   # BR(HH -> photon Z)
     2.02164599E-07   2        23        23   # BR(HH -> Z Z)
     5.67686714E-08   2       -24        24   # BR(HH -> W W)
     8.75727679E-06   2        21        21   # BR(HH -> gluon gluon)
     8.83023733E-09   2       -11        11   # BR(HH -> Electron electron)
     3.93171659E-04   2       -13        13   # BR(HH -> Muon muon)
     1.13553696E-01   2       -15        15   # BR(HH -> Tau tau)
     3.92196654E-14   2        -2         2   # BR(HH -> Up up)
     7.60877942E-09   2        -4         4   # BR(HH -> Charm charm)
     5.34167606E-04   2        -6         6   # BR(HH -> Top top)
     6.36084615E-07   2        -1         1   # BR(HH -> Down down)
     2.30110850E-04   2        -3         3   # BR(HH -> Strange strange)
     6.44510527E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.22673330E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.65433056E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     6.65433056E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.24522832E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.84664137E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.87514627E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.02253715E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.26095631E-08   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.12594067E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.59600708E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     8.34028864E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.97620872E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     8.30384466E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.76901072E+01   # Gamma(A0)
     5.41644142E-08   2        22        22   # BR(A0 -> photon photon)
     6.98520329E-08   2        22        23   # BR(A0 -> photon Z)
     1.50126285E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.69716865E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.87247347E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.11842829E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.61993861E-14   2        -2         2   # BR(A0 -> Up up)
     7.02265856E-09   2        -4         4   # BR(A0 -> Charm charm)
     4.97618300E-04   2        -6         6   # BR(A0 -> Top top)
     6.26486329E-07   2        -1         1   # BR(A0 -> Down down)
     2.26637436E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.34790369E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.37594518E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.96686382E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     6.96686382E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.29313890E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.10213327E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.80164396E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     9.96369586E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.92228402E-08   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.49234698E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.21664494E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     8.39169103E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.67437659E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     3.59138847E-07   2        23        25   # BR(A0 -> Z h0)
     4.06726439E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.09270458E+02   # Gamma(Hp)
     9.66228953E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.13093055E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.16846215E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.10445454E-07   2        -1         2   # BR(Hp -> Down up)
     1.03125576E-05   2        -3         2   # BR(Hp -> Strange up)
     7.18938388E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.88638200E-08   2        -1         4   # BR(Hp -> Down charm)
     2.20043138E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.00677101E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.69616941E-08   2        -1         6   # BR(Hp -> Down top)
     1.13211832E-06   2        -3         6   # BR(Hp -> Strange top)
     6.79373912E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.31193684E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.66116160E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.41512206E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.58861919E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.24793981E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     5.48706259E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     5.60290183E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.88437383E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.70315483E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.94292645E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.94289677E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001528E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.99416901E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    5.14695385E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.70315483E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.94292645E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.94289677E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999885E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.15097471E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999885E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.15097471E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25702301E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.50238281E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.29297897E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.15097471E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999885E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    2.91019545E-04   # BR(b -> s gamma)
    2    1.59696905E-06   # BR(b -> s mu+ mu-)
    3    3.52795144E-05   # BR(b -> s nu nu)
    4    3.96516030E-15   # BR(Bd -> e+ e-)
    5    1.69380706E-10   # BR(Bd -> mu+ mu-)
    6    3.50858082E-08   # BR(Bd -> tau+ tau-)
    7    1.33155239E-13   # BR(Bs -> e+ e-)
    8    5.68817697E-09   # BR(Bs -> mu+ mu-)
    9    1.19417378E-06   # BR(Bs -> tau+ tau-)
   10    9.58318401E-05   # BR(B_u -> tau nu)
   11    9.89906219E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42017452E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93570153E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15764774E-03   # epsilon_K
   17    2.28166270E-15   # Delta(M_K)
   18    2.48121324E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29351870E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.53526623E-16   # Delta(g-2)_electron/2
   21    1.93897963E-11   # Delta(g-2)_muon/2
   22    5.49405839E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.28092963E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.51784004E-01   # C7
     0305 4322   00   2     5.25983737E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -8.09867548E-02   # C8
     0305 6321   00   2    -9.90280157E-05   # C8'
 03051111 4133   00   0     1.61396823E+00   # C9 e+e-
 03051111 4133   00   2     1.61485843E+00   # C9 e+e-
 03051111 4233   00   2     5.46385755E-04   # C9' e+e-
 03051111 4137   00   0    -4.43666033E+00   # C10 e+e-
 03051111 4137   00   2    -4.43779892E+00   # C10 e+e-
 03051111 4237   00   2    -4.11831909E-03   # C10' e+e-
 03051313 4133   00   0     1.61396823E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61485822E+00   # C9 mu+mu-
 03051313 4233   00   2     5.46385174E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43666033E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43779913E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.11831933E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50559836E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     8.93088361E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50559836E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     8.93088443E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50559836E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     8.93111537E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85846482E-07   # C7
     0305 4422   00   2     2.24082233E-05   # C7
     0305 4322   00   2     3.05751250E-06   # C7'
     0305 6421   00   0     3.30503029E-07   # C8
     0305 6421   00   2    -8.20378191E-06   # C8
     0305 6321   00   2     9.72845723E-07   # C8'
 03051111 4133   00   2     3.67522875E-07   # C9 e+e-
 03051111 4233   00   2     1.05730602E-05   # C9' e+e-
 03051111 4137   00   2     7.85234139E-07   # C10 e+e-
 03051111 4237   00   2    -7.96976982E-05   # C10' e+e-
 03051313 4133   00   2     3.67519031E-07   # C9 mu+mu-
 03051313 4233   00   2     1.05730566E-05   # C9' mu+mu-
 03051313 4137   00   2     7.85237248E-07   # C10 mu+mu-
 03051313 4237   00   2    -7.96977091E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.46890462E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.72830441E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.46890340E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.72830441E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.46855895E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.72830441E-05   # C11' nu_3 nu_3
