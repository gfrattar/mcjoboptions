# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 12.05.2022,  13:33
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.92742263E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.38668120E+03  # scale for input parameters
    1    3.66001217E+02  # M_1
    2    9.41866383E+02  # M_2
    3    4.38054240E+03  # M_3
   11   -1.76394464E+03  # A_t
   12    1.54180229E+03  # A_b
   13    2.11461043E+02  # A_tau
   23    1.20969080E+03  # mu
   25    5.63550426E+00  # tan(beta)
   26    2.64945622E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.67648015E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.01824856E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.82684590E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.38668120E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.38668120E+03  # (SUSY scale)
  1  1     8.51534942E-06   # Y_u(Q)^DRbar
  2  2     4.32579750E-03   # Y_c(Q)^DRbar
  3  3     1.02872261E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.38668120E+03  # (SUSY scale)
  1  1     9.64307409E-05   # Y_d(Q)^DRbar
  2  2     1.83218408E-03   # Y_s(Q)^DRbar
  3  3     9.56290779E-02   # Y_b(Q)^DRbar
Block Ye Q=  3.38668120E+03  # (SUSY scale)
  1  1     1.68278694E-05   # Y_e(Q)^DRbar
  2  2     3.47946969E-03   # Y_mu(Q)^DRbar
  3  3     5.85189562E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.38668120E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.76394499E+03   # A_t(Q)^DRbar
Block Ad Q=  3.38668120E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.54180226E+03   # A_b(Q)^DRbar
Block Ae Q=  3.38668120E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     2.11461029E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.38668120E+03  # soft SUSY breaking masses at Q
   1    3.66001217E+02  # M_1
   2    9.41866383E+02  # M_2
   3    4.38054240E+03  # M_3
  21    5.29601806E+06  # M^2_(H,d)
  22   -1.05278415E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.67648015E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.01824856E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.82684590E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18643443E+02  # h0
        35     2.64937318E+03  # H0
        36     2.64945622E+03  # A0
        37     2.65354396E+03  # H+
   1000001     1.00875773E+04  # ~d_L
   2000001     1.00637269E+04  # ~d_R
   1000002     1.00872722E+04  # ~u_L
   2000002     1.00667686E+04  # ~u_R
   1000003     1.00875781E+04  # ~s_L
   2000003     1.00637271E+04  # ~s_R
   1000004     1.00872730E+04  # ~c_L
   2000004     1.00667698E+04  # ~c_R
   1000005     2.80045057E+03  # ~b_1
   2000005     2.96143802E+03  # ~b_2
   1000006     2.80130555E+03  # ~t_1
   2000006     4.09438004E+03  # ~t_2
   1000011     1.00208578E+04  # ~e_L-
   2000011     1.00088569E+04  # ~e_R-
   1000012     1.00201107E+04  # ~nu_eL
   1000013     1.00208584E+04  # ~mu_L-
   2000013     1.00088579E+04  # ~mu_R-
   1000014     1.00201113E+04  # ~nu_muL
   1000015     1.00091229E+04  # ~tau_1-
   2000015     1.00210378E+04  # ~tau_2-
   1000016     1.00202617E+04  # ~nu_tauL
   1000021     4.80358837E+03  # ~g
   1000022     3.67658244E+02  # ~chi_10
   1000023     9.93119240E+02  # ~chi_20
   1000025     1.22446803E+03  # ~chi_30
   1000035     1.24317783E+03  # ~chi_40
   1000024     9.92715298E+02  # ~chi_1+
   1000037     1.24254089E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.67643808E-01   # alpha
Block Hmix Q=  3.38668120E+03  # Higgs mixing parameters
   1    1.20969080E+03  # mu
   2    5.63550426E+00  # tan[beta](Q)
   3    2.43150373E+02  # v(Q)
   4    7.01961826E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99470065E-01   # Re[R_st(1,1)]
   1  2     3.25513233E-02   # Re[R_st(1,2)]
   2  1    -3.25513233E-02   # Re[R_st(2,1)]
   2  2     9.99470065E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99912494E-01   # Re[R_sb(1,1)]
   1  2     1.32288973E-02   # Re[R_sb(1,2)]
   2  1    -1.32288973E-02   # Re[R_sb(2,1)]
   2  2     9.99912494E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     4.97535515E-02   # Re[R_sta(1,1)]
   1  2     9.98761525E-01   # Re[R_sta(1,2)]
   2  1    -9.98761525E-01   # Re[R_sta(2,1)]
   2  2     4.97535515E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.98948595E-01   # Re[N(1,1)]
   1  2     3.20675466E-03   # Re[N(1,2)]
   1  3    -4.16101513E-02   # Re[N(1,3)]
   1  4     1.89741040E-02   # Re[N(1,4)]
   2  1     1.45977164E-02   # Re[N(2,1)]
   2  2     9.65102814E-01   # Re[N(2,2)]
   2  3    -1.98443202E-01   # Re[N(2,3)]
   2  4     1.70246175E-01   # Re[N(2,4)]
   3  1     1.59171167E-02   # Re[N(3,1)]
   3  2    -2.06788756E-02   # Re[N(3,2)]
   3  3    -7.06305327E-01   # Re[N(3,3)]
   3  4    -7.07426190E-01   # Re[N(3,4)]
   4  1    -4.04383069E-02   # Re[N(4,1)]
   4  2     2.61033827E-01   # Re[N(4,2)]
   4  3     6.78248978E-01   # Re[N(4,3)]
   4  4    -6.85714524E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.57683094E-01   # Re[U(1,1)]
   1  2     2.87824759E-01   # Re[U(1,2)]
   2  1     2.87824759E-01   # Re[U(2,1)]
   2  2     9.57683094E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.68724287E-01   # Re[V(1,1)]
   1  2     2.48139590E-01   # Re[V(1,2)]
   2  1     2.48139590E-01   # Re[V(2,1)]
   2  2     9.68724287E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01524018E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.97954431E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.09494769E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.46506756E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.58956629E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42178860E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.70553700E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.87897046E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.72409489E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.57930831E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     4.98311782E-02    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01570851E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.97861433E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.11330880E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.69748202E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.61085148E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.42181211E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.70539652E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.87892957E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.72482372E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.57921640E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.98303574E-02    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.17066481E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.66436476E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     4.63126477E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.41172886E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.57869109E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     8.88783571E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     8.05400341E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.42614161E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.73690324E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.85785351E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.43847129E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.99936454E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.53400688E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.10128127E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42182296E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.91643678E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.78712005E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.56327693E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.40222389E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.70811789E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.70332716E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42184647E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.91628990E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.78707414E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.56323471E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.40218433E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.70803762E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.70477578E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.42846969E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.87508270E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.77419493E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.55139067E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.39108453E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.68551758E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.11119379E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.79237766E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.16679895E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88308079E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.08005516E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.38853523E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.63479154E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.09588767E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.31372159E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.17351715E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.83021136E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.79239087E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.16679599E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88305396E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.08009906E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.38852008E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.63474840E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.09634485E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.31371577E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.17407268E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.83015618E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.66821223E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.89173863E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.31220785E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.54822811E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.07310638E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.87692922E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.61915262E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.35837044E+00   # ~b_2
#    BR                NDA      ID1      ID2
     6.77823740E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.46318851E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.88870127E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.60353639E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.44823860E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.54100235E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     9.86380491E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     4.33964991E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     8.21875827E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.96366287E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.50751024E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.54832475E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.07992590E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.22232821E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.70659217E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.56331352E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.34417451E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.72209245E-03    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.82986320E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.96373655E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.50744395E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.54818488E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.07996948E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.22231463E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.70656134E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.56610554E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.34416587E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.72301458E-03    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.82980792E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.67958949E+01   # ~t_1
#    BR                NDA      ID1      ID2
     6.11292915E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.48159576E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.84473180E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.73855349E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.71943922E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.39886861E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.46633789E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.67685293E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.32654296E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.10817145E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.12433094E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.01433635E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.35948114E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.01528437E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.42039406E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.20087739E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.04446081E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     7.43937592E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.86639002E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.33603099E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     5.81536141E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.51022470E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.21094679E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.87923895E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.36593659E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.14942178E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.14536727E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     6.60837317E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     8.38877643E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     9.12464999E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.63866656E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     8.34629877E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.52490251E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.52490251E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.04529072E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.44641505E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.74656055E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     9.74055909E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.39275705E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     7.30812078E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     5.41828211E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.94765275E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.94765275E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.14495298E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.14186952E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.31874286E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.53044562E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.47392262E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.40840908E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.40840908E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.88520150E+02   # ~g
#    BR                NDA      ID1      ID2
     1.62727892E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.62727892E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.79456011E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.79456011E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.64318362E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.64318362E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.44935311E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.44935311E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.79513226E-03   # Gamma(h0)
     2.63306551E-03   2        22        22   # BR(h0 -> photon photon)
     1.11712975E-03   2        22        23   # BR(h0 -> photon Z)
     1.65061542E-02   2        23        23   # BR(h0 -> Z Z)
     1.54775599E-01   2       -24        24   # BR(h0 -> W W)
     8.65676250E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.64909641E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.51277941E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.24426424E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.61659527E-07   2        -2         2   # BR(h0 -> Up up)
     3.13716204E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.59984856E-07   2        -1         1   # BR(h0 -> Down down)
     2.38700178E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.34095359E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.97676997E+01   # Gamma(HH)
     5.36239636E-07   2        22        22   # BR(HH -> photon photon)
     6.42922368E-07   2        22        23   # BR(HH -> photon Z)
     5.29694518E-05   2        23        23   # BR(HH -> Z Z)
     1.88991938E-05   2       -24        24   # BR(HH -> W W)
     3.98762021E-05   2        21        21   # BR(HH -> gluon gluon)
     6.25852051E-10   2       -11        11   # BR(HH -> Electron electron)
     2.78647635E-05   2       -13        13   # BR(HH -> Muon muon)
     8.04752375E-03   2       -15        15   # BR(HH -> Tau tau)
     8.19204262E-12   2        -2         2   # BR(HH -> Up up)
     1.58915332E-06   2        -4         4   # BR(HH -> Charm charm)
     1.15116613E-01   2        -6         6   # BR(HH -> Top top)
     4.63132207E-08   2        -1         1   # BR(HH -> Down down)
     1.67520450E-05   2        -3         3   # BR(HH -> Strange strange)
     4.25423935E-02   2        -5         5   # BR(HH -> Bottom bottom)
     1.67520485E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.37032777E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.37032777E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.43510060E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     3.90466240E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.73763194E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     7.17862082E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.26689817E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     8.09902790E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.95134397E-01   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.74637776E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     4.39521929E-05   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.01697638E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.12985745E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.57526532E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.95359463E+01   # Gamma(A0)
     1.22508754E-06   2        22        22   # BR(A0 -> photon photon)
     1.56032825E-06   2        22        23   # BR(A0 -> photon Z)
     7.17474946E-05   2        21        21   # BR(A0 -> gluon gluon)
     6.17147241E-10   2       -11        11   # BR(A0 -> Electron electron)
     2.74772071E-05   2       -13        13   # BR(A0 -> Muon muon)
     7.93561041E-03   2       -15        15   # BR(A0 -> Tau tau)
     7.79102317E-12   2        -2         2   # BR(A0 -> Up up)
     1.51129833E-06   2        -4         4   # BR(A0 -> Charm charm)
     1.11344440E-01   2        -6         6   # BR(A0 -> Top top)
     4.56692526E-08   2        -1         1   # BR(A0 -> Down down)
     1.65191075E-05   2        -3         3   # BR(A0 -> Strange strange)
     4.19519251E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     5.81604242E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.06492470E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.06492470E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.33271940E-02   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     5.79060502E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     9.77027326E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.63096792E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     6.46550294E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.82722760E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.47897989E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.62728534E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.36849591E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     9.25892075E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.59274354E-02   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     8.05507944E-05   2        23        25   # BR(A0 -> Z h0)
     1.20565018E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.07245918E+01   # Gamma(Hp)
     7.43931168E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     3.18053809E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.99636855E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.79579900E-08   2        -1         2   # BR(Hp -> Down up)
     8.08233625E-07   2        -3         2   # BR(Hp -> Strange up)
     5.06844948E-07   2        -5         2   # BR(Hp -> Bottom up)
     8.90832443E-08   2        -1         4   # BR(Hp -> Down charm)
     1.91395082E-05   2        -3         4   # BR(Hp -> Strange charm)
     7.09792998E-05   2        -5         4   # BR(Hp -> Bottom charm)
     9.48371608E-06   2        -1         6   # BR(Hp -> Down top)
     2.06801925E-04   2        -3         6   # BR(Hp -> Strange top)
     1.93739834E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.35977505E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.08070521E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.89749330E-06   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.39288301E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.25418749E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.96772751E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.29571724E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.92587830E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     7.63458029E-05   2        24        25   # BR(Hp -> W h0)
     8.28761187E-11   2        24        35   # BR(Hp -> W HH)
     7.49467551E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.12078253E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.18468300E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.17589083E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00276841E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.87188163E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.14872285E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.12078253E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.18468300E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.17589083E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99936407E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.35926329E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99936407E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.35926329E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27330433E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.45318652E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.89544767E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.35926329E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99936407E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.27105573E-04   # BR(b -> s gamma)
    2    1.59008475E-06   # BR(b -> s mu+ mu-)
    3    3.52663997E-05   # BR(b -> s nu nu)
    4    2.55648215E-15   # BR(Bd -> e+ e-)
    5    1.09210014E-10   # BR(Bd -> mu+ mu-)
    6    2.28619897E-08   # BR(Bd -> tau+ tau-)
    7    8.61048457E-14   # BR(Bs -> e+ e-)
    8    3.67839623E-09   # BR(Bs -> mu+ mu-)
    9    7.80218077E-07   # BR(Bs -> tau+ tau-)
   10    9.67786221E-05   # BR(B_u -> tau nu)
   11    9.99686115E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42334616E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93778962E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15871796E-03   # epsilon_K
   17    2.28167266E-15   # Delta(M_K)
   18    2.48078162E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29238593E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.12480040E-17   # Delta(g-2)_electron/2
   21    1.76348127E-12   # Delta(g-2)_muon/2
   22    4.98803979E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -2.31849103E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.95565963E-01   # C7
     0305 4322   00   2    -1.53542311E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.05173429E-01   # C8
     0305 6321   00   2    -2.05445772E-04   # C8'
 03051111 4133   00   0     1.62040535E+00   # C9 e+e-
 03051111 4133   00   2     1.62075281E+00   # C9 e+e-
 03051111 4233   00   2     8.38904914E-06   # C9' e+e-
 03051111 4137   00   0    -4.44309746E+00   # C10 e+e-
 03051111 4137   00   2    -4.44278324E+00   # C10 e+e-
 03051111 4237   00   2    -6.20894188E-05   # C10' e+e-
 03051313 4133   00   0     1.62040535E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62075281E+00   # C9 mu+mu-
 03051313 4233   00   2     8.38904885E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44309746E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44278324E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.20894187E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50528358E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.34442471E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50528358E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.34442471E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50528358E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.34442611E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825687E-07   # C7
     0305 4422   00   2     2.11077585E-06   # C7
     0305 4322   00   2     1.51306573E-07   # C7'
     0305 6421   00   0     3.30485217E-07   # C8
     0305 6421   00   2    -1.80085081E-07   # C8
     0305 6321   00   2     3.19152385E-08   # C8'
 03051111 4133   00   2     4.11021485E-07   # C9 e+e-
 03051111 4233   00   2     1.69406605E-07   # C9' e+e-
 03051111 4137   00   2     3.71666902E-07   # C10 e+e-
 03051111 4237   00   2    -1.25521775E-06   # C10' e+e-
 03051313 4133   00   2     4.11021412E-07   # C9 mu+mu-
 03051313 4233   00   2     1.69406604E-07   # C9' mu+mu-
 03051313 4137   00   2     3.71666976E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.25521775E-06   # C10' mu+mu-
 03051212 4137   00   2    -5.37943187E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.71794730E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.37943153E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.71794730E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.37933555E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.71794729E-07   # C11' nu_3 nu_3
