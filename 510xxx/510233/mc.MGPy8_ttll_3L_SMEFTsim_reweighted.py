selected_operators = ['cQl111','cQl122','cQl311','cQl322','cte11','cte22','cQe11','cQe22','ctl11','ctl22','cleQt1Re11','cleQt1Re22','cleQt3Re11','cleQt3Re22']
process_definition = 'generate p p > t t~ l+ l- QCD=2 QED=4 NP=1, (t > w+ b NP=0,  w+ > wdec wdec NP=0), (t~ > w- b~ NP=0, w- > wdec wdec NP=0) @0 NPprop=0 SMHLOOP=0'
fixed_scale = 436.0 # ~ 2*m(top)+m(Z)
gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+ll, top model, inclusive, reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob = 1000
runArgs.maxEvents = int(evgenConfig.nEventsPerJob / 0.40)

include("SMEFTsim_topmW_topX_reweighted.py")
runArgs.maxEvents = evgenConfig.nEventsPerJob
include("GeneratorFilters/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 7000
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 3 
