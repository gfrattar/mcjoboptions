gKKtR=5.711782707
gKKtL=5.711782707
MKK=3000
WGKK=3000*0.5

decay_str='decay t > w+ b, w+ >  fall fall\ndecay t~ > w- b~, w- > fall fall\n'
evgenConfig.nEventsPerJob = 10000

include ( "MadGraphControl_KKgluon_tt.py" )
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Semi-leptonic decay filter
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #no-allhad
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
