evgenConfig.description = "QCD Instanton production with a minimum mass of 100 GeV and 5 active flavors"
evgenConfig.keywords = ["QCD","SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch"]
evgenConfig.tune = "none"

from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]
testSeq.TestHepMC.MaxVtxDisp=10000.
testSeq.TestHepMC.MaxTransVtxDisp=10000.
testSeq.TestHepMC.UnstableNoVtxTest=False
testSeq.TestHepMC.NoDecayVertexStatuses=[1,2,3,4,5,6]
testSeq.TestHepMC.VtxDisplacedTest=False
