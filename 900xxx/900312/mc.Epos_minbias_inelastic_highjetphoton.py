evgenConfig.description = "High-pT EPOS inelastic minimum bias events for pileup, using high-pT jet and photon filters."
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch", "jan.kretzschmar@cern.ch" ]
evgenConfig.nEventsPerJob = 200
evgenConfig.tune = "EPOS LHC"

evgenConfig.saveJets = True
evgenConfig.savePileupTruthParticles = True

include("Epos_i/Epos_Base_Fragment.py")

include ("GeneratorFilters/AddPileupTruthParticles.py")
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
CreateJets(prefiltSeq, 0.6)

AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
filtSeq.QCDTruthJetFilter.MinPt = 35.*GeV

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 8000. ]
filtSeq.DirectPhotonFilter.Etacut = 4.5

filtSeq.Expression = 'DirectPhotonFilter or QCDTruthJetFilter'
