# Metadata
evgenConfig.description = "MadGraph5+Pythia8 for LRSM WR and NR (Keung-Senjanovic process)"
evgenConfig.keywords = ["exotic","Wprime","BSM"]
evgenConfig.contact = ["Xanthe Hoad <xanthe.hoad@cern.ch>"]
evgenConfig.process = "pp > WR > l NR, NR > l WR, WR > j j"


evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 10000

# Shower options
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
