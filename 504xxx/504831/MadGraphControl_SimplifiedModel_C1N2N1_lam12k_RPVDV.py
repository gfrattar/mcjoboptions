from MadGraphControl.MadGraphUtils import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

domerge = True

#Read filename of jobOptions to obtain: realgentype, decaytype, neutralino mass, lifetime.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')
gentype = "C1N2N1"      #for param card
decaytype = "GGMHinoZ"  #for param card
realgentype = str(tokens[2])
realdecaytype = str(tokens[3])
neutralinoMass = float(tokens[4])
lifetime = str(tokens[5])
neutralino_ctau = lifetime.replace("mm","").replace(".py","")
decayWidth = 3.*1e11*6.581e-25/float(neutralino_ctau)
decayWidthStr = '%e' % decayWidth

headerN1 = 'DECAY   1000022  ' + decayWidthStr
headerC1 = 'DECAY   1000024  3.24412243E+00' #taken from /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/MadGraph/param_card.SM.C1N2N1.GGMHinoZ.dat
headerN2 = 'DECAY   1000023  3.24412243E+00' #taken from /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/MadGraph/param_card.SM.C1N2N1.GGMHinoZ.dat

evgenLog.info('lifetime of 1000022 is set to %s mm'% neutralino_ctau)
evgenConfig.nEventsPerJob = 2000

if realdecaytype == "lam12k":
        branchingRatiosN1 = ''' # neutralino decay
        ##           BR         NDA      ID1       ID2       ID3
        8.333333E-02    3      12       -13        11   # BR(~chi_10 -> nue mubar e)
        8.333333E-02    3     -12        13       -11   # BR(~chi_10 -> nuebar mu ebar)
        8.333333E-02    3      14       -13        11   # BR(~chi_10 -> numu mubar e)
        8.333333E-02    3     -14        13       -11   # BR(~chi_10 -> numubar mu ebar)
        1.666667E-01    3      14       -11        11   # BR(~chi_10 -> numu ebar e)
        1.666667E-01    3     -14        11       -11   # BR(~chi_10 -> numubar e ebar)
        1.666667E-01    3      12       -13        13   # BR(~chi_10 -> nue mubar mu)
        1.666667E-01    3     -12        13       -13   # BR(~chi_10 -> nuebar mu mubar)
        '''
        branchingRatiosC1 = ''' # C1 decays, taken from param_card.SM.C1N2N1.GGMHinoZ.dat 
        ##          BR         NDA          ID1       ID2       ID3       ID4
        3.64604242E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
        3.64604242E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
        1.21534824E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
        1.21534824E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
        2.77218666E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
        '''
        branchingRatiosN2 = ''' #N2 decays, taken from param_card.SM.C1N2N1.GGMHinoZ.dat
        ##          BR         NDA          ID1       ID2       ID3       ID4
        1.37284605E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
        1.84624124E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
        1.37284605E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
        1.84624124E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
        3.98007010E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
        3.98007010E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
        3.98835450E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
        7.88991980E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
        7.88991980E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
        7.88991980E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
        '''

else:
    raise RunTimeError("ERROR: realdecaytype is not lam12k")

decays['1000022'] = headerN1 + branchingRatiosN1
decays['1000024'] = headerC1 + branchingRatiosC1
decays['1000023'] = headerN2 + branchingRatiosN2

njets = 2
evt_multiplier = 1.3
if domerge:
    evt_multiplier*= 1.1
else:
    njets = 0
    xqcut = 0

if (int(neutralinoMass)==50):
    evt_multiplier*=1.8
if (int(neutralinoMass)==100):
    evt_multiplier*=1.6
if (int(neutralinoMass)==300):
    evt_multiplier*=1.3

mass_split = 1
masses['1000022'] = float(neutralinoMass) #N1
masses['1000023'] = -1.0 * float(neutralinoMass + mass_split) #N2
masses['1000024'] = float(neutralinoMass + mass_split) #C1
# __And now production. Taking cues from higgsinoRPV.py generator__
# Format taken from https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/500xxx/500195/SUSY_SimplifiedModel_VBFewkino_N2ZC1W.py#L232-266

# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 0.00E+00'   # N_11 bino 
param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1  3']=' 7.07E-01'   # N_13
param_blocks['NMIX']['1  4']='-7.07E-01'   # N_14 
param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21 
param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2  3']='-7.07E-01'   # N_23 higgsino
param_blocks['NMIX']['2  4']='-7.07E-01'   # N_24 higgsino 
param_blocks['NMIX']['3  1']=' 1.00E+00'   # N_31 
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32 
param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 higgsino
param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 higgsino
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']='-1.00E+00'   # N_42 wino
param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44

run_settings['bwcutoff']=10000

mgprocstring=""
if realgentype == 'C1N2N1' : #production modes cover all of C1N1, N2N1, C1+C1-, and C1N2
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    define c1 = x1+ x1-
    define X = n1 c1
    define Y = n2 c1
    generate    p p > X Y     RPV=0 QED<=2 / susystrong @1
    add process p p > X Y j   RPV=0 QED<=2 / susystrong @2
    add process p p > X Y j j RPV=0 QED<=2 / susystrong @3
    '''

#elif 'C1C1' in realgentype or 'CpCm' in realgentype or 'CmCp' in realgentype:
#    mgprocstring="x1+ x1-"
#    hardproc="pp>{~chi_1+,1000024}{~chi_1-,-1000024}"
#elif 'N2N1' in realgentype or 'N1N2' in realgentype:
#    mgprocstring="n2 n1"
#    hardproc="pp>{~chi_20,1000023}{~chi_10,1000022}"
#elif 'N2C1m' in realgentype or 'C1mN2' in realgentype:
#    mgprocstring="n2 x1-"
#    hardproc="pp>{~chi_1-,-1000024}{~chi_20,1000023}"
#elif 'N2C1p' in realgentype or 'C1pN2' in realgentype:
#    mgprocstring="n2 x1+"
#    hardproc="pp>{~chi_1+,1000024}{~chi_20,1000023}"
#elif 'N1C1m' in realgentype or 'C1mN1' in realgentype:
#    mgprocstring="n1 x1-"
#    hardproc="pp>{~chi_1-,-1000024}{~chi_10,1000022}"
#elif 'N1C1p' in realgentype or 'C1pN1' in realgentype:
#    mgprocstring="n1 x1+"
#    hardproc="pp>{~chi_1+,1000024}{~chi_10,1000022}"
else:
    raise RuntimeError("Unknown process %s, aborting." % realgentype)

#process = '''
#    import model RPVMSSM_UFO
#    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
#    '''

#if domerge:
#    process += '''
#    generate    p p > %s      / susystrong
#    add process p p > %s j    / susystrong
#    add process p p > %s j j  / susystrong
#    ''' % (mgprocstring, mgprocstring, mgprocstring)
#else:
#    process += '''
#    generate    p p > %s j / susystrong
#    ''' % (mgprocstring)

print "Final process card:"
print process

evgenConfig.contact = ["zachary.pollock@cern.ch"]
evgenConfig.keywords +=['SUSY', 'RPV', 'neutralino']
if realgentype == 'C1N2N1' :
    evgenConfig.description = 'C1N1, N2N1, C1C1 and C1N2 higgsino'
elif realgentype == 'C1N1' :
    evgenConfig.description = 'chargino-neutralino'
elif realgentype == 'N1N2' :
    evgenConfig.description = 'neutralino1-neutralino2'
elif realgentype == 'C1C1' :
    evgenConfig.description = 'chargino+-chargino-'
elif realgentype == 'N1C1p' or realgentype == 'C1pN1':
    evgenConfig.description = 'chargino+-neutralino1'
elif realgentype == 'N1C1m' or realgentype == 'C1mN1':
    evgenConfig.description = 'chargino--neutralino1'
elif realgentype == 'N2C1p' or realgentype == 'C2pN2':
    evgenConfig.description = 'chargino+-neutralino2'
elif realgentype == 'N2C1m' or realgentype == 'C2mN2':
    evgenConfig.description = 'chargino--neutralino2'

evgenConfig.description += ' production and decays via RPV coupling (%s), m_N1 = %s GeV, m_C1 = %s GeV, m_N2 = %s GeV'%(realdecaytype, masses['1000022'],masses['1000024'],masses['1000023'])

extras = {'time_of_flight':'0'} #in mm

add_lifetimes_lhe = True

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess","1000024:spinType = 1","1000023:spinType = 1","1000022:spinType = 1"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]


#if domerge:
#    mergeproc="Merging:Process = %s" % hardproc
#    #if guessproc:
#    #    mergeproc="Merging:Process = guess"
#    if njets > 0:
#        genSeq.Pythia8.Commands += [ "%s" % mergeproc,
#                                     "1000024:spinType = 1",
#                                     "1000023:spinType = 1",
#                                     "1000022:spinType = 1" ]
#
#        if "guess" in mergeproc:
#            genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']

testSeq.TestHepMC.MaxVtxDisp = 1e8 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1e8 #in mm


